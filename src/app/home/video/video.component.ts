import { Component, OnInit, AfterViewInit } from '@angular/core';
import {HomeService}from '../home.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit, AfterViewInit {
  showMobileView = false;
  homepageData : any;
  constructor(private homeservice :HomeService) { }

  ngOnInit() {
    this.getAllHomepageVideo();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  getAllHomepageVideo() {                                        
    this.homeservice.gethompagePublishedVideo().subscribe(data => {
      this.homepageData =data;
      
     
     
    }, error => {
      console.log(error);
    });
  }
}
