import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HomeService } from '../home.service';
import { AppSetting } from '../../config/appSetting';
@Component({
  selector: 'app-strip',
  templateUrl: './strip.component.html',
  styleUrls: ['./strip.component.css']
})
export class StripComponent implements OnInit {
  public names = 'flowers';
  public redClass = 'text-danger';
  public greenClass = 'text-success';

  title: any;
  description: any;
  productModel: any;
  productImageUrl: string;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  forthFrame: any;

  constructor(private router: Router, private homeService: HomeService) {
    this.productImageUrl = AppSetting.productImageUrl;
  }

  ngOnInit() {
    this.getSecondRowProduct();
    this.getAllProductTag();
  }
  submit() {
    this.router.navigate(['/home/subscribe']);
  }
  getSecondRowProduct() {
    this.homeService.getSecondRow().subscribe(data => {
      this.title = data[0].title;
      this.description = data[0].description;
      this.productModel = data[0].productDetails;
      this.firstFrame = data[0].productDetails[0];
      this.secondFrame = data[0].productDetails[1];
      this.thirdFrame = data[0].productDetails[2];
      this.forthFrame = data[0].productDetails[3];
      console.log('strip', this.productModel);
    }, error => {
      console.log(error);
    });
  }
  getAllProductTag() {
    this.homeService.getAllProductTag().subscribe(data => {
    }, error => {
      console.log(error);
    });
  }/*
  getProduct(id) {
    this.homeService.getProductPosition(id).subscribe(data => {
      this.productModel = data;
    }, error => {
      console.log(error);
    });
  } */
}
