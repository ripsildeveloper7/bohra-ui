import { Component, OnInit, AfterViewInit } from '@angular/core';
import {HomeService} from '../home.service';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit, AfterViewInit {
  public name = 'krishnan';
  public newClass = 'balaji2';
  public oldClass = 'balaji3';
  public errorTime = true;
  public noerrorTime = false;
  public balaji = 'paraSin';
  public bala = 'hSix';
  showMobileView = false;
  instaImages: any;
  track = [{image: '../../../assets/images/Icons/1.png' , msg:'Genuine Products'},
{image: '../../../assets/images/Icons/free-delivery.png' , msg:'Free Delivery in India'},
{image: '../../../assets/images/Icons/free.png', msg:'Free Fall & Pico Services'},
{image: '../../../assets/images/Icons/logistics-delivery-truck-in-movement.png', msg:'Ready to Ship'}];

  final = [
    {
      finals: '../../../assets/img/Categories/5.jpg',
      kurtiName: 'BAGS',
      prize: '6 Products'
    },
    {
      finals: '../../../assets/img/Categories/16.jpg',
      kurtiName: 'BOOKING',
      prize: '6 Products'
    },
    {
      finals: '../../../assets/img/Categories/3.jpg',
      kurtiName: 'CLOTHING',
      prize: '12 Products'
    },
    {
      finals: '../../../assets/img/Categories/18.jpg',
      kurtiName: 'MEN',
      prize: '9 Products'
    },
    {
      finals: '../../../assets/img/Categories/19.jpg',
      kurtiName: 'NEW ONE',
      prize: '8 Products'
    },
    {
      finals: '../../../assets/img/Categories/6.jpeg',
      kurtiName: 'FASHION',
      prize: '8 Products'
    },
    {
      finals: '../../../assets/img/Categories/7.jpg',
      kurtiName: 'T.SHIRTS',
      prize: '12 Products'
    }

  ];

  constructor(public homeService: HomeService) { }

  ngOnInit() {
    this.getInstaPics();
  }
  getInstaPics() {
    this.homeService.getInstagramPics().subscribe(data => {
    this.instaImages = data;
    }, err => {
    console.log(err);
    });
  }
  ngAfterViewInit(){
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
}
