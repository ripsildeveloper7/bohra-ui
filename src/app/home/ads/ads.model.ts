export class ADSModel {
    position: number;
    adsImageName: string;
    adsDescription: string;
    adsTitle: string;
    link: string;
    publish: boolean;
}
