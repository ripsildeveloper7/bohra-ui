import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-product-review',
  templateUrl: './product-review.component.html',
  styleUrls: ['./product-review.component.css']
})
export class ProductReviewComponent implements OnInit {
  holder: any;

  constructor(private homeService: HomeService) {
    this.getPublishedReview();
   }

  ngOnInit() {
  }
  getPublishedReview() {
    this.homeService.getPublishedReview().subscribe(data => {
      this.holder = data;
      console.log('product review', data);
    }, error => {
      console.log(error);
    });
  }
}
