import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SuperCategory } from './category.model';
import {HomeService} from '../home.service';
import { AppSetting } from '../../config/appSetting';

@Component({
  selector: 'app-category-content',
  templateUrl: './category-content.component.html',
  styleUrls: ['./category-content.component.css']
})
export class CategoryContentComponent implements OnInit, AfterViewInit {
  showFiller = false;
  productImageUrl: string = AppSetting.categoryBannerImageUrl;
  categoryModel: SuperCategory[];
  ProductHolder: any;
  firstFrame: any;
  secondFrame: any;
  thirdFrame: any;
  showMobileView = false;
constructor(private router: Router, private homeService: HomeService) {
  this.getCategoryBanner();
 }
getimage(id) {
this.router.navigate([]);
}
  ngOnInit()  {
    // this.getCategoryDetails();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  // getCategoryDetails() {
  //  this.homeService.getCategory().subscribe(data => {
  //    console.log(data);
  //    this.ProductHolder = data.find(e => e.subCategory);
  //    this.firstFrame = data[0];
  //    console.log(this.firstFrame);
  //    this.secondFrame = data[1];
  //    this.thirdFrame = data[2];
  //    console.log(this.ProductHolder);
  //  }, error => {
  //    console.log(error);
  //  });
  // }
  getCategoryBanner() {
    this.homeService.getAllCategoryBanner().subscribe(data => {
      this.ProductHolder = data;
      console.log('category banner', data);
    }, error => {
      console.log(error);
    });
  }
}

