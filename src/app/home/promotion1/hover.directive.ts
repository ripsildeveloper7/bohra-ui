import { Directive, ElementRef, Input, AfterViewInit, Renderer2 } from '@angular/core';
import { HostListener } from '@angular/core';
import { HostBinding } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective {

  @HostBinding('class.productBoxHover') private ishovering: boolean;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    // renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'gray');
  }

  @HostListener('mouseover') onMouseOverLeft() {
    const part1 = this.el.nativeElement.querySelector('.productActionLeft');
    const part2 = this.el.nativeElement.querySelector('.productActionRight');
    this.renderer.setStyle(part1, 'display', 'block');
    this.renderer.setStyle(part2, 'display', 'block');
    this.ishovering = true;
  }
  /* @HostListener('mouseover') onMouseOverRight() {
    const part = this.el.nativeElement.querySelector('.productActionRight');
    this.renderer.setStyle(part, 'display', 'block');
    this.ishovering = true;
  } */

  @HostListener('mouseout') onMouseOutLeft() {
    const part1 = this.el.nativeElement.querySelector('.productActionLeft');
    const part2 = this.el.nativeElement.querySelector('.productActionRight');
    this.renderer.setStyle(part1, 'display', 'none');
    this.renderer.setStyle(part2, 'display', 'none');
    this.ishovering = false;
  }
  /* @HostListener('mouseout') onMouseOutRight() {
    const part = this.el.nativeElement.querySelector('.productActionRight');
    this.renderer.setStyle(part, 'display', 'none');
    this.ishovering = false;
  } */
}
