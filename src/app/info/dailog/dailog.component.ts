import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from "@angular/material";

@Component({
  selector: 'app-dailog',
  templateUrl: './dailog.component.html',
  styleUrls: ['./dailog.component.css']
})
export class DailogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DailogComponent>) { }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close("Thanks for using me!");
  }


}
