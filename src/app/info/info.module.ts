import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfoRoutingModule } from './info-routing.module';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { MatFormFieldModule, MatInputModule, MatDialogModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms'
import { InfoService } from './info.service';
import { AboutUsComponent } from './about-us/about-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { FaqComponent } from './faq/faq.component';
import { DailogComponent } from './dailog/dailog.component';
import { PaymentPolicyComponent } from './payment-policy/payment-policy.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { ReturnRefundComponent } from './return-refund/return-refund.component';
import { ShippingPolicyComponent } from './shipping-policy/shipping-policy.component';

@NgModule({
  declarations: [ContactUsComponent, AboutUsComponent, TermsConditionsComponent, PrivacyPolicyComponent, FaqComponent, DailogComponent, PaymentPolicyComponent, DisclaimerComponent, ReturnRefundComponent, ShippingPolicyComponent],
  providers:[InfoService],
  imports: [
    CommonModule,
    InfoRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    ReactiveFormsModule
  ],
  entryComponents: [DailogComponent]
})
export class InfoModule { }
