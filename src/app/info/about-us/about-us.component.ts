import { Component, OnInit } from '@angular/core';
import { InfoService } from '../info.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  aboutUs : any;
  constructor( private infoservice: InfoService) {
    this.infoservice.getAboutUs()
    .subscribe( data => {
      this.aboutUs = data;
      console.log(data);
    })
   }
  ngOnInit() {
  }

}
