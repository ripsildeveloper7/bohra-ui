import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { contactUs } from '../shared/model/contactus.model';
import { AppSetting } from '../config/appSetting';

@Injectable({
  providedIn: 'root'
})
export class InfoService {
  serviceUrl = AppSetting.contentServiceUrl;
  constructor(private httpClient: HttpClient) { }

  contactSubmit(contactForm) {
    const urlway = this.serviceUrl + 'createcontactus';
    return this.httpClient.post<contactUs>(urlway, contactForm);
  }

  getTerms(){
    const termsUrl = this.serviceUrl + 'getterms';
    return this.httpClient.get(termsUrl);
  }

  getPrivacyPolicy(){
    const policyUrl = this.serviceUrl + 'getprivacypolicy';
    return this.httpClient.get(policyUrl);
  }

  getAboutUs(){
    const aboutUrl = this.serviceUrl + 'getfooterdetail';
    return this.httpClient.get(aboutUrl);
  }

  getFaq(){
    const faqUrl = this.serviceUrl + 'getfaq';
    return this.httpClient.get(faqUrl);
  }
}
