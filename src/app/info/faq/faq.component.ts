import { Component, OnInit } from '@angular/core';
import { InfoService } from '../info.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

 
  faq: any;
  constructor( private infoservice : InfoService) { 
    this.infoservice.getFaq()
    .subscribe( data => {
      this.faq = data;
      console.log(data);
    })
   }

  ngOnInit() {
  }

}
