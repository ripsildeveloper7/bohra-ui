import { Component, OnInit, Inject, Optional, Input  } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from './../account.service';
import { Review } from './review.mode';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegModel } from './../registration/registration.model';
import { AppSetting } from '../../config/appSetting';

@Component({
  selector: 'app-review-product',
  templateUrl: './review-product.component.html',
  styleUrls: ['./review-product.component.css']
})
export class ReviewProductComponent implements OnInit {
  reviewForm: FormGroup;
  userId: any;
  productData: any;
  productImageUrl: string = AppSetting.productImageUrl;
  customerModel: RegModel;
  holder: Review;
  itemId: any;
  rating: any;
  ratingClicked: number;
  itemIdRatingClicked: string;

  inputName: string;
  customerMail: string;
  reviewHolder: any;
  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<ReviewProductComponent>, private accountService: AccountService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
    this.getProduct();
    this.userId = sessionStorage.getItem('userId');
    this.getCustomer();
    this.inputName = this.itemId + '_rating';
  }
  createForm() {
    this.reviewForm = this.fb.group({
      customerName: [''],
      customerId: [''],
      productId: [''],
      review: [''],
      country: ['']
    });
  }
  getProduct() {
    this.accountService.getSingleProductsWithBrand(this.data.productId).subscribe(data => {
      this.productData = data;
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  getCustomer() {
    this.accountService.getCustomerDetails(this.userId).subscribe(data => {
      this.customerModel = data;
      this.customerMail = data.emailId;
      console.log(data.emailId);
      }, error => {
        console.log(error);
      });
  }
  onSubmit() {
    this.holder = new Review();
    this.holder.customerName = this.customerModel.firstName;
    this.holder.customerId = this.userId;
    this.holder.productId = this.data.productId;
    this.holder.cartId = this.data.cartId;
    this.holder.review = this.reviewForm.controls.review.value;
    this.holder.rating = this.rating !== undefined ? this.rating : 0;
    this.holder.customerEmailId = this.customerMail;
    this.holder.country = this.reviewForm.controls.country.value;
    this.holder.orderId = this.data.orderId;
    this.accountService.createReview(this.holder).subscribe(value => {
      console.log(value);
      this.dialogRef.close(true);
    }, error => {
      this.dialogRef.close(false);
      console.log(error);
    });
  }
  onClick(rating: number): void {
    this.rating = rating;
  }
 
}
