
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { ReviewProductComponent } from './review-product.component';
import { Review } from './review.mode';

@Injectable({
  providedIn: 'root'
})
export class ReviewProductService {
  dialogRef: MatDialogRef<ReviewProductComponent>;
  constructor(private dialog: MatDialog) { }

  productReview(product?: any): Observable<boolean> {
    this.dialogRef = this.dialog.open(ReviewProductComponent,
      {
        disableClose: true, backdropClass: 'light-backdrop',
        width: '720px',
        data: product,
      });
    return this.dialogRef.afterClosed();
  }
  closeProductReview() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
  /* editAddress(address: AddressModel): Observable<any> {
    this.dialogRef = this.dialog.open(ReviewProductComponent,
      {
        disableClose: true, backdropClass: 'light-backdrop',
        width: '720px',
        data: address,
      });
    return this.dialogRef.afterClosed();
  } */
}
