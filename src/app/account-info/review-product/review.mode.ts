export class Review {
    customerName: string;
    customerId: string;
    cartId: string;
    productId: string;
    review: string;
    createdDate: string;
    rating: number;
    customerEmailId: string;
    country: string;
    orderId: string;
}
