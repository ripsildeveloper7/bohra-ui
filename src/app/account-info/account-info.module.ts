import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration/registration.component';
import { AccountInfoRoutingModule } from './account-info-routing.module';
import { AccountService } from './account.service';
import { AddressService } from './address/address.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {OrderComponent} from './order/order.component';

import {
  MatSidenavModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule,
  MatSliderModule
} from '@angular/material';
import { CardDetailsComponent } from './card-details/card-details.component';
import { AddressComponent } from './address/address.component';
import { ProfileComponent } from './profile/profile.component';
import { SigninComponent } from './signin/signin.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { ListAddressComponent } from './list-address/list-address.component';
import { ListCardComponent } from './list-card/list-card.component';
import { CardDetailsService } from './card-details/card-details.service';
import { AccountComponent } from './account/account.component';
import { SharedModule } from './../shared/shared.module';
import { ReviewProductComponent } from './review-product/review-product.component';
import { ReviewProductService } from './review-product/review-product.service';
import { PayPalComponent } from './pay-pal/pay-pal.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SetPasswordComponent } from './set-password/set-password.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { ViewMeasurementComponent } from './view-measurement/view-measurement.component';
import { ViewSingleMeasurementComponent } from './view-single-measurement/view-single-measurement.component';
import { TailoringDetailService } from './view-tailoring-detail/tailoring-detail.service';
import { ViewTailoringDetailComponent } from './view-tailoring-detail/view-tailoring-detail.component';

@NgModule({
  declarations: [RegistrationComponent,
    CardDetailsComponent, AddressComponent, ProfileComponent,
     SigninComponent, AccountDetailsComponent, ListAddressComponent, OrderComponent,
      ListCardComponent,  ResetPasswordComponent, SetPasswordComponent,
      AccountComponent,
      ReviewProductComponent,
      PayPalComponent,
      TrackOrderComponent,
      ViewMeasurementComponent,
      ViewSingleMeasurementComponent, ViewTailoringDetailComponent],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule,
    AccountInfoRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatTooltipModule,
    MatOptionModule,
    MatSelectModule,
    MatMenuModule,
    MatSnackBarModule,
    MatGridListModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatRadioModule,
    MatCheckboxModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatRippleModule,
    MatDialogModule,
    MatChipsModule,
    MatInputModule,
    MatFormFieldModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatSliderModule,
    SharedModule
  ],
  providers: [AccountService, AddressService, CardDetailsService, ReviewProductService, TailoringDetailService],
  entryComponents: [AccountDetailsComponent,  CardDetailsComponent, ReviewProductComponent, ViewTailoringDetailComponent]
})
export class AccountInfoModule { }
