import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { SignIn } from '../signin/signIn.model';
import { AccountService } from '../account.service';
import { Cart } from './../../shared/model/cart.model';
import { Product } from './../../shared/model/product.model';
import { RegModel } from '../registration/registration.model';
import { MustMatch } from '../reset-password/must-match.validator';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent implements OnInit {
  passwordForm: FormGroup;
  accountDetail;
  userId;
  holder;
  text = 'registredCustomer';
  constructor(private fb: FormBuilder, private router: Router, private accountService: AccountService) { }

  ngOnInit() {
    this.userId = sessionStorage.getItem('userId');
    this.createForm();
  }
  createForm() {
    this.passwordForm = this.fb.group({
      newPwd: ['', Validators.required],
      confirmPwd: ['', Validators.required]
    },
    {
      validator: MustMatch('newPwd', 'confirmPwd')
    });
  }
  setPassword() {
    this.holder = new RegModel();
    this.holder.password = this.passwordForm.controls.newPwd.value;
    this.holder.createdBy = this.text;
    this.accountService.setPassword(this.holder, this.userId).subscribe(data => {
      this.router.navigate(['account/profile']);
    }, error => {
      console.log(error);
    });
  }
  cancel() {
    this.router.navigate(['account/profile']);
  }
}
