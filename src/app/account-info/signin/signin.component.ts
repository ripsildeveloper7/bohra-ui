import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location} from '@angular/common';
import {SignIn} from './signIn.model';
import {AccountService} from '../account.service';
import { Cart } from './../../shared/model/cart.model';
import { Product } from './../../shared/model/product.model';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit, AfterViewInit {
  tabItems = [{item: 'Login'}, {item: 'Registration'}];
  selectedItemTab = this.tabItems[0].item;
  signInForm: FormGroup;
  shopModel: any;
  signInModel: SignIn;
  pwdError: boolean;
  submitted = false;
  showPassword = false;
  cart: Cart;
  productModel: Product;
  userId: string;
  cartLocal: any;
  showMobileView = false;
  constructor(private fb: FormBuilder, private router: Router, private accountService: AccountService, private location: Location ) { }

  ngOnInit() {
    this.createForm();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  createForm() {
    this.userId = sessionStorage.getItem('userId');
    this.signInForm = this.fb.group({
      emailId: ['', Validators.required],
      password: ['', Validators.required],
      mobileNumber: ['']
    });
  }

onSubmit() {
  this.submitted = true;
  this.signInModel = new SignIn();
  this.signInModel.emailId = this.signInForm.controls.emailId.value;
  this.signInModel.password = this.signInForm.controls.password.value;
  this.accountService.signIn(this.signInModel).subscribe(data => {
    console.log(data);
    if (data.length === 0) {
     this.pwdError = true;
     sessionStorage.setItem('login', 'false');
     sessionStorage.removeItem('userId');
    } else {
      /* this.setCookie(data[0]._id); */
      console.log(data);
      sessionStorage.setItem('login', 'true');
      sessionStorage.setItem('userId', data[0].userId);
      sessionStorage.setItem('emailId', data[0].emailId);
      this.router.navigate(['account']);
      this.logInUserData();
    }
  });
}

checkPassword() {
  this.showPassword = !this.showPassword;
}
logInUserData()   {
  this.userId = sessionStorage.getItem('userId');
  this.cartLocal = JSON.parse(sessionStorage.getItem('cart'));
  if (!this.cartLocal)     {
    /* this.router.navigate(['home/welcome']); */
    this.shoppingCartUser(this.userId);
    this.location.back();
  } else {
    const localItem = this.cartLocal.map(item => item.items);
    this.addToCartServer(this.userId, localItem);
 }
}

addToCartServer(userId, items) {
  this.cart = new Cart();
  this.cart.userId = userId;
  this.cart.items = items;
  this.accountService.addToCart(this.cart).subscribe(data => {
  this.shopModel = data;
  sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
  sessionStorage.removeItem('cart');
  /* this.router.navigate(['home/welcome']); */
  this.location.back();
  }, error => {
    console.log(error);
  });
}

shoppingCartUser(userId) {
  this.accountService.shoppingUser(userId).subscribe(data => {
    this.shopModel = data;
    sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
  }, err => {
    console.log(err);
  });
}

}
