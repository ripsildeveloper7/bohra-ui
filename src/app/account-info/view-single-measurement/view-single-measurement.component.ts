import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from './../account.service';
import {  RegModel } from './../registration/registration.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-view-single-measurement',
  templateUrl: './view-single-measurement.component.html',
  styleUrls: ['./view-single-measurement.component.css']
})
export class ViewSingleMeasurementComponent implements OnInit {
  id: string;
  holder: any;
  showNoData: boolean;

  constructor(private accountService: AccountService, private router: Router,
              private activatedRoute: ActivatedRoute) {
                this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
                  this.id = params.get('id');
                });
                this.getMeasurment();
               }

  ngOnInit() {
  }
  getMeasurment() {
    this.accountService.getSelctedMeasurementByUser(this.id).subscribe(data => {
      console.log(data);
      this.holder = data;
      if(data.length === 0) {
        this.showNoData = true;
      } else {
        this.showNoData = false;
      }
    }, error => {
      console.log(error);
    });
  }
}
