import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Order } from './../../shared/model/order.model';
import { AppSetting } from '../../config/appSetting';
import { Router } from '@angular/router';
import { ReviewProductService } from '../review-product/review-product.service';
import { TailoringDetailService } from '../view-tailoring-detail/tailoring-detail.service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  userId: string;
  orderModel;
  productImageUrl: string = AppSetting.productImageUrl;
  checkStatus: any;
  productId: any;
  userDetail: any;
  reviewHolder: any;
  constructor(private accountService: AccountService, private router: Router,
              private  reviewProductService: ReviewProductService, private tailorService: TailoringDetailService ) { }

  ngOnInit() {
    this.getOrders();
  }

  getOrders() {
    this.userId = sessionStorage.getItem('userId');
    this.accountService.getCustomerOrderDetails(this.userId).subscribe(data => {
      this.orderModel = data;
      this.getCustomer(this.orderModel, this.userId);
      if (this.orderModel.length !== 0) {
        this.getAllReviewProduct();
      }
      console.log('order', data);
      this.checkStatus = data[0];
    }, error => {
      console.log(error);
    });
  }
  getCustomer(order, userId) {
    this.accountService.getMeasurementByUser(userId).subscribe(data => {
      this.userDetail = data;
      this.tailoringService(order, this.userDetail);
    }, error => {
      console.log(error);
    });
  }
  tailoringService(order, user) {
    
  }
  viewProduct(id) {
    this.router.navigate(['product/viewsingle/', id]);
  }
  createReview(id, orderid, cartid) {
    const holder = {
      productId: id,
      orderId: orderid,
      cartId: cartid
    };
    this.reviewProductService.productReview(holder).subscribe(
      res => {
        if (res)  {
          this.getOrders();
        }
      }
    );

  }
  trackTheOrder(awbNo, orderId) {
    this.router.navigate(['account/trackorders/', awbNo, 'order', orderId]);
    /* this.accountService.trackAWB(awbNo).subscribe(data => {
      console.log(data);
    }, err => {
      console.log('err', err);
    }); */
  }
  getMeasurment(value) {
    console.log(value);
    this.tailorService.open(value);
   /*  this.salesService.getSelctedMeasurementByUser(serviceId).subscribe(data => {
      const holder = data;
      this.tailoringService.open(holder);
    }, error => {
      console.log(error);
    }); */
  }
  getAllReviewProduct() {
    this.accountService.getReviewForVerification().subscribe(data => {
      this.reviewHolder = data;
      this.checkOneTimeReview();
    }, error => {
      console.log(error);
    });
  }
  checkOneTimeReview() {
    const field = 'orderId';
    if (this.reviewHolder.length === 0) {
      for (const order of this.orderModel) {
        order.isReview = false;
      }
    } else {
      for (const reivew of this.reviewHolder) {
    for (const order of this.orderModel) {
      for (const cart of order.cart) {
      
        if (order[field] === reivew[field]) {
          if (cart._id === reivew.cartId) {
            cart.isReview = true;
          }
        } else {
          cart.isReview = false;
        }
      }
    }
    }
  }
  }
}
