import { Directive, ElementRef, Input, AfterViewInit, Renderer2 } from '@angular/core';
import { HostListener } from '@angular/core';
import { HostBinding } from '@angular/core';

@Directive({
  selector: '[appHoverItem]'
})
export class HoverItemDirective {

  @HostBinding('class.productBoxHover') private ishovering: boolean;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    // renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'gray');
  }

  @HostListener('mouseover') onMouseOver() {
    const part1 = this.el.nativeElement.querySelector('.productActionLeft');
    const part2 = this.el.nativeElement.querySelector('.productActionRight');
    /* const part3 = this.el.nativeElement.querySelector('.productAction'); */
    this.renderer.setStyle(part1, 'display', 'block');
    this.renderer.setStyle(part2, 'display', 'block');
    /* this.renderer.setStyle(part3, 'display', 'block'); */
    this.ishovering = true;
  }

  @HostListener('mouseout') onMouseOut() {
    const part1 = this.el.nativeElement.querySelector('.productActionLeft');
    const part2 = this.el.nativeElement.querySelector('.productActionRight');
    /* const part3 = this.el.nativeElement.querySelector('.productAction'); */
    this.renderer.setStyle(part1, 'display', 'none');
    this.renderer.setStyle(part2, 'display', 'none');
    /* this.renderer.setStyle(part3, 'display', 'none'); */
    this.ishovering = false;
  }
}
