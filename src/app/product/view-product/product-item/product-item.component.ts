import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, ViewChild } from '@angular/core';
import { AppSetting } from './../../../config/appSetting';
import { ProductService } from './../../../product/product.service';
import { Product } from './../../../shared/model/product.model';
import { WishList } from './../../../shared/model/wishList.model';
import { MainCategory } from './../../../shared/model/mainCategory.model';
import { BrandModel } from './../../../shared/model/brand.model';
import { ProductSettings } from './../../../shared/model/product-setting.model';
import { ScrollService } from './scroll.service';
import { MatSnackBar, MatPaginator } from '@angular/material';
import { Filter } from './../all-product/filter.model';
import { Size } from './../../../shared/model/size.model';
import { Cart } from './../../../shared/model/cart.model';
import { FieldAttribute } from './../side-bar/filterAtribute.model';
import { ActivatedRoute, Router, NavigationEnd, PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data, ActivatedRouteSnapshot } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { filter, map, } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { price } from 'src/app/app.component';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit, AfterViewInit {
  /* @Input() productModel: Product[];
  @Input() productImageUrl: string; */
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;
  backgroundUrl = '/../../../../assets/images/746888_denim_1.jpg';
  productImageUrl: string = AppSetting.productImageUrl;
  shopModel: any;
  paginatorData:{
    pageNo:number;
    perPage :number;
    supercatid:string;
  };
  paginatorPages :any;
  product$: Observable<Product>;
  productModel: any;
  wishList: WishList[];
  wishListLength;
  wish: WishList;
  product: Product;
  productCommonModel: Product[];
  fieldAttribute: FieldAttribute;
  fieldAttributePush: Array<any> = [];
  mainCategory: MainCategory;
  brandModel: BrandModel;
  productSettings: ProductSettings;
  color: any;
  filter: Filter;
  public pageSize = 30;
  itemCount = 1;
  cartModel: Cart;
  public currentPage = 0;
  public totalSize = 0;
  public array: any;
  catId: string;
  mainCatId: string;
  subCatId: string;
  brandId: string;
  breadcrumbs;
  getFilterColor: any;
  getFilterSize: any;
  getFilterMaxPrice: any;
  getFilterMinPrice: any;
  getFilterAttribute: any;
  filterColor: string;
  discountStore: any;
  userId: string;
  message: string;
  count = 1;
  action: string;
  isCart = false;
  isLocalCart = false;
  showMobileView = false;
  addedCart;
  queryItems: any;
  totalPageCount:any;
  noOfPages:any;
  queryParams:{
    pageNo:number,
    
  }
  imageLoader = true;
  filterAttributepush: Array<any> = [];
  attributeId: any;
  attributefieldId: any;
  filterPages: any;
  noOfFilterPages:any;
  pageNum: number;
  pager: any = {};

  // paged items
  pagedItems: any[];
  page: string = "1";
  constructor(private productService: ProductService,
    private snackBar: MatSnackBar,
    private scrollService: ScrollService, private activatedRoute: ActivatedRoute, private router: Router,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
      // this.pageNum = route.queryParamMap.get('pageNo');
      // console.log(this.pageNum);
    iconRegistry.addSvgIcon(
      'hearts',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/hearts.svg'));
      this.activatedRoute.parent.paramMap.subscribe((params: ParamMap) => {
        this.catId = params.get('catid');
      });
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.subCatId = params.get('subid');
      this.mainCatId = params.get('maincatid');
      this.attributeId = params.get('attributeId');
      this.attributefieldId = params.get('attributefieldId');
      // console.log(params.get('pageNo'),"pp")
      
    });

    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
    
  }
  /* routerLink="/product/viewsingle/{{product._id}}"  */
  ngOnInit() {
    

    this.activatedRoute.queryParamMap.subscribe((queryparams: ParamMap): void => {
      /* this.getFilterColor = queryparams.get('Color');
      this.getFilterSize = queryparams.get('Size'); */
      const data: any = Object.values({...queryparams.keys});
      const localTest = data.filter(el => el);
      console.log(localTest,"local tt")
      /* localTest.forEach(test => {
        this.getFilterAttribute = queryparams.get(test);
        this.filterAttributepush.push({fieldName: test, fieldValue: this.getFilterAttribute});
      }) */
      this.filterAttributepush = new Array();
      if (localTest.length > 0) {
        localTest.forEach(el => {
         
          this.fieldAttribute = new FieldAttribute();
          this.fieldAttribute.fieldName =  el;
          if (queryparams.get("pageNo")) {
            this.pageNum = parseInt(queryparams.get("pageNo"));
            this.page = queryparams.get("pageNo");
          } else {
            this.pageNum = 1;
            this.page = '1';
          }
          
          
          this.fieldAttribute.fieldValue =  queryparams.get(el).split(',');
          this.filterAttributepush.push(this.fieldAttribute);
         });
       } else {
        if (queryparams.get("pageNo")) {
          this.pageNum = parseInt(queryparams.get("pageNo"));
          this.page = queryparams.get("pageNo");
        } else {
          this.pageNum = 1;
          this.page = '1';
        }
       }
      /*  console.log('filterattributepush',this.filterAttributepush); */
      
      /*  const obj = {};
       for (const item of this.filterAttributepush) {
        if (!obj[item.fieldName]) {
          const element = item.fieldName;
          obj[element] = item.fieldValue;
        }
      }
       console.log(obj); */
       /* !this.getFilterColor && !this.getFilterSize && !this.getFilterMaxPrice && !this.getFilterMinPrice &&  */
       /* console.log(this.filterAttributepush.length < 0); */
      //  this.router.navigate([], {
      //   relativeTo: this.activatedRoute, queryParams:
      //   this.displayAttribute[0]
      // });
      if (this.filterAttributepush.length === 0) {
        this.activatedRoute.data.subscribe((data: Data) => {
          console.log('product item',data);
          // this.productModel = data.product;
         
          this.productModel = data.product[0].data;
          // this.totalPageCount = data.product[0].metadata[0].total;
          // console.log('total pages',this.totalPageCount);
          // var totalData =  Math.ceil(this.totalPageCount / 30);
          // this.noOfPages =[];
          // console.log('product model',this.productModel);
          // for(let i = 1; i <= totalData; i++){
          //   this.noOfPages.push(i);  
          // }  
         
          /* console.log(this.productModel.forEach(element => {
            if(element){
              console.log(element.child);
            }
          })) */
         
          // console.log('product demo', this.productModel);
          this.productCommonModel = data.product[0].data;
          // this.productCommonModel = data.product;
          this.array = this.productModel;
          /* this.ilterPlainArray(this.productModel, this.fieldAttributePush) */
          this.totalSize = this.array.length;
          
          
          this.checkLogin();
          this.iterator();
          this.getDiscount();
          this.setPage(this.pageNum);
          // this.setPage(1);
          // this.fetchMoreItems();
        });
      } else {
        this.viewCategoryFitler();
      }
    });
  }
  /* 
  getValue = value => (typeof value === 'string' ? value.toUpperCase() : value);
  ilterPlainArray(array, filters) {
    const filterKeys = Object.keys(filters);
    array.filter(item => {
      // validates all filter criteria
      return filterKeys.every(key => {
        // ignores an empty filter
        if (!filters[key].length) return true;
        return filters[key].find(filter => this.getValue(filter) === this.getValue(item[key]));
      });
    });
    console.log(array)
  } */
  getDiscount() {
    for (let i = 0; i <= this.productModel.length - 1; i++) {
      const sp = this.productModel[i].sp * 110 / 100;
      this.productModel[i].sp = sp;
      this.productModel[i].price = sp;
      const discount = 100 - this.productModel[i].discount;
      const totalPrice = this.productModel[i].sp * (100 / discount);
      const savePrice = totalPrice - this.productModel[i].sp;
      this.productModel[i].savePrice = savePrice;
      this.productModel[i].totalPrice = totalPrice;
    }
    this.checkQty();
  }
  checkQty() {
    this.productModel.forEach(element => {
      element.quantityCheck = 0;
      this.assingQtyCheck(element);
    });
    console.log(this.productModel);
  }
  assingQtyCheck(product) {
    product.child.forEach(element => {
      product.quantityCheck += element.quantity;
    });
  }
  getStartPage(item){
    console.log('pagenumber',item);
    this.queryParams={
      pageNo:1
    }
  }
  getPage(item){
   
    console.log('item',item);
    // this.router.navigate(["product/supercategory/",this.catId],{
    //   queryParams:{
    //     "pageNo":item,
    //     "perPage":30,
    //   }
    // })
    this.queryParams={
      pageNo:item,
     
    }
    if(item!=1){
    this.router.navigate([], {
      relativeTo: this.activatedRoute, queryParams:
      this.queryParams,
      queryParamsHandling:'merge'
    });
  }else{
    var snapshot = this.activatedRoute.snapshot;
    const params= {...snapshot.queryParams};
    delete params.pageNo
    this.router.navigate([],{ relativeTo: this.activatedRoute,queryParams:params})
    // this.router.navigate([], {
    //   relativeTo: this.activatedRoute
    // })
    // console.log('activate route',this.activatedRoute);
  }
  // console.log('activate route',this.activatedRoute);
  // this.setPage(item);
}
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  onLoad(){
    this.imageLoader = false;
  }
  
  clickToView(id) {
    this.router.navigate(['/product/viewsingle/', id]);
  }

  private fetchMoreItems() {
    this.scrollService.onScrolledDown$
      .subscribe((test) => {
        if (this.pageSize < this.array.length) {
          this.pageSize += 5;
          this.iterator();
        }
      });
  }
  getProduct(product) {
    if (this.catId && this.mainCatId && this.subCatId) {
      this.router.navigate(['/product/viewsingle/', this.subCatId, product._id]);
    } else if (this.catId && this.mainCatId) {
      this.router.navigate(['/product/viewsingle/', this.mainCatId, product._id]);
    } else if (this.catId) {
      this.router.navigate(['/product/viewsingle/', this.catId, product._id]);
    }
  }
  viewCategoryFitler() {
    if (this.catId && this.mainCatId && this.subCatId) {
      this.viewSubCategoryFilter();
    } else if (this.catId && this.attributeId && this.attributefieldId ) {
      this.viewSupAttributeCategoryFilter();
    } else if (this.catId && this.mainCatId) {
      this.viewMainCategoryFilter();
    } else if (this.catId) {
      this.viewSuperCategoryFilter();
    }
  }
  viewSupAttributeCategoryFilter(){
    this.product = new Product();
    this.product.superCategoryId = this.catId;
    this.product.attribute = this.filterAttributepush;
    this.productService.getAllSuperCategoryAttributeFilter(this.product).subscribe(data => {
    this.productModel = data[0].data;
      console.log('filtervalue',data);
    this.productCommonModel = data[0].data;
       this.totalPageCount = data[0].metadata[0].total;
      var totalData =  Math.ceil(this.totalPageCount / 30)
      this.noOfPages =[];
      console.log('product model',this.productModel);
      for(let i = 1; i <= totalData; i++){
        this.noOfPages.push(i);  
      }
      
      this.array = this.productModel;
      this.totalSize = this.productModel.length;
      this.getDiscount();
      this.checkLogin();
      this.iterator();
      this.setPage(this.pageNum);
    }, err => {
      console.log(err);
    });
  }

  setPage(page: number){
     this.pager = this.getPager(this.totalPageCount, page);
     /* this.pagedItems = this.noOfPages.slice(this.pager.startIndex, this.pager.endIndex + 1); */
     this.getPage(page);
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 30) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    // ensure current page isn't out of range
    if (currentPage < 1) { 
        currentPage = 1; 
    } else if (currentPage > totalPages) { 
        currentPage = totalPages; 
    }
    
    let startPage: number, endPage: number;
    if (totalPages <= 10) {
        // less than 10 total pages so show all
        startPage = 1;
        endPage = totalPages;
    } else {
        // more than 10 total pages so calculate start and end pages
        if (currentPage <= 6) {
            startPage = 1;
            endPage = 10;
        } else if (currentPage + 4 >= totalPages) {
            startPage = totalPages - 9;
            endPage = totalPages;
        } else {
            startPage = currentPage - 5;
            endPage = currentPage + 4;
        }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    // return object with all pager properties required by the view
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
    };
}
  
  viewSuperCategoryFilter() {
    this.product = new Product();
    this.product.superCategoryId = this.catId;
    this.product.attribute = this.filterAttributepush;
    
    this.productService.getAllSuperCategoryFilter(this.product).subscribe(data => {
      this.productModel = data[0].data;
      console.log('filtervalue',data);
      this.productCommonModel = data[0].data;
      this.totalPageCount = data[0].metadata[0].total;
      var totalData =  Math.ceil(this.totalPageCount / 30);
      this.noOfPages =[];
      console.log('product model',this.productModel);
      for(let i = 1; i <= totalData; i++){
        this.noOfPages.push(i);  
      }
      
      this.array = this.productModel;
      this.totalSize = this.productModel.length;
      this.getDiscount();
      this.checkLogin();
      this.iterator();
      this.setPage(this.pageNum);
    }, err => {
      console.log(err);
    });
  }
  viewMainCategoryFilter() {
    this.product = new Product();
    this.product.mainCategoryId = this.mainCatId;
  /*   this.product.color = this.getFilterColor === null ? [] : this.getFilterColor.split(',');
    this.product.size = this.getFilterSize === null ? [] : this.getFilterSize.split(',');
    this.product.maxPrice = this.getFilterMaxPrice === null ? null : this.getFilterMaxPrice;
    this.product.minPrice = this.getFilterMinPrice === null ? null : this.getFilterMinPrice;
    this.product.attribute = this.getFilterAttribute === null ? [] : this.filterAttributepush; */
    this.productService.getAllMainCategoryFilter(this.product).subscribe(data => {
      this.productModel = data;
      this.productCommonModel = data;
      this.array = this.productModel;
      this.totalSize = this.productModel.length;
      this.getDiscount();
      this.checkLogin();
      this.iterator();
    }, err => {
      console.log(err);
    });
  }
  viewSubCategoryFilter() {
    
 /*    this.product.subCategoryId = this.subCatId;
    this.product.color = this.getFilterColor === null ? [] : this.getFilterColor.split(',');
    this.product.size = this.getFilterSize === null ? [] : this.getFilterSize.split(',');
    this.product.maxPrice = this.getFilterMaxPrice === null ? null : this.getFilterMaxPrice;
    this.product.minPrice = this.getFilterMinPrice === null ? null : this.getFilterMinPrice;
    this.product.attribute = this.getFilterAttribute === null ? [] : this.filterAttributepush; */
    this.product = new Product();
    this.product.subCategoryId = this.subCatId;
    /* this.product.color = this.getFilterColor === null ? [] : this.getFilterColor.split(',');
    this.product.size = this.getFilterSize === null ? [] : this.getFilterSize.split(',');
    this.product.maxPrice = this.getFilterMaxPrice === null ? null : this.getFilterMaxPrice; */
    this.product.attribute = this.filterAttributepush;
    this.productService.getAllSubCategoryFilter(this.product).subscribe(data => {
      this.productModel =data[0].data;
      this.totalPageCount = data[0].metadata[0].total;
      var totalData =  Math.ceil(this.totalPageCount / 30);
      this.noOfPages =[];
      console.log('product model',this.productModel);
      for (let i = 1; i <= totalData; i++) {
        this.noOfPages.push(i);
      }
      this.productCommonModel = data[0].data;
      this.array = this.productModel;
      this.totalSize = this.productModel.length;
      this.getDiscount();
      this.checkLogin();
      this.iterator();
      this.setPage(this.pageNum);
    }, err => {
      console.log(err);
    });
  }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.productModel = part;
  }
  previous(e) {
    this.currentPage = e.pageIndex;

  }
  // getPaginator(){
  //   this.paginatorData = {
  //     pageNo : 2,
  //     perPage : 30,
  //     supercatid:"5d84655912a3870008d8d899",
  //   }
  //   this.productService.getPagination(this.paginatorData).subscribe(data => {
  //     this.paginatorPages = data;
   
  //   }, error => {
  //     console.log(error);
  //   });
  // }
  
  getAllDiscount() {
    this.productService.getAllDiscount().subscribe(data => {
      this.discountStore = data;
      this.combainDiscount();
    }, error => {
      console.log(error);
    });
  }
  combainDiscount() {
    for (let l = 0; l <= this.productModel.length - 1; l++) {
      this.productModel[l].discountStyle = 'discountNone';        // set all discountStyle field 'discountNone'
      for (let i = 0; i <= this.discountStore.length - 1; i++) {
        for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
          for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {

            if (this.discountStore[i].conditions[j].field === 'Product Name') {     // check discount field type
              if (this.productModel[l]._id === this.discountStore[i].conditions[j].value[k]) {

                if (this.productModel[l].discountStyle === 'discountStyle') {     // check offer already applied or not
                  if (this.discountStore[i].amountType === 'Flat') {              // check amount type
                    const temp = this.productModel[l].oldPrice - this.discountStore[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel[l].price > temp) {      // new price is lesserthan previous price
                      this.productModel[l].price = this.productModel[l].oldPrice - this.discountStore[i].typeValue;
                      this.productModel[l].discount = 'Flat ' + this.discountStore[i].typeValue;
                      this.productModel[l].discountStyle = 'discountStyle';
                    } else {        // new price is smallerthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {                  // amount type percentage
                    const temp = this.productModel[l].price - Math.round((this.productModel[l].price / 100 * this.discountStore[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel[l].price > temp) {          // new price is lesserthan previous price
                      this.productModel[l].price = this.productModel[l].oldPrice - Math.round((this.productModel[l].oldPrice / 100 * this.discountStore[i].typeValue));
                      this.productModel[l].discount = this.discountStore[i].typeValue + '%';
                      this.productModel[l].discountStyle = 'discountStyle';
                    } else {                // new price is smallerthan previous price
                      continue;
                    }
                  }

                  /* check amount greaterthan or lessthan ----- end ---------*/

                  /* previous offer not applied product-------------- start ------------- */

                } else {
                  this.productModel[l].oldPrice = this.productModel[l].price;
                  if (this.discountStore[i].amountType === 'Flat') {
                    this.productModel[l].price = this.productModel[l].oldPrice - this.discountStore[i].typeValue;
                    this.productModel[l].discount = 'Flat ' + this.discountStore[i].typeValue;
                    this.productModel[l].discountStyle = 'discountStyle';
                  } else {
                    this.productModel[l].price = this.productModel[l].oldPrice - (this.productModel[l].oldPrice / 100 * this.discountStore[i].typeValue);
                    this.productModel[l].discount = this.discountStore[i].typeValue + '%';
                    this.productModel[l].discountStyle = 'discountStyle';
                  }
                }

                /* previous offer not applied product-------------- end ------------- */

              }
            } else if (this.discountStore[i].conditions[j].field === 'Product Category') {     // check discount field type
              if (this.productModel[l].superCategoryId === this.discountStore[i].conditions[j].value[k]) {
                if (this.productModel[l].discountStyle === 'discountStyle') {  // check offer already applied or not
                  if (this.discountStore[i].amountType === 'Flat') {     // check amount type
                    const temp = this.productModel[l].oldPrice - this.discountStore[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel[l].price > temp) {      // new price is lesserthan previous price
                      this.productModel[l].price = this.productModel[l].oldPrice - this.discountStore[i].typeValue;
                      this.productModel[l].discount = 'Flat ' + this.discountStore[i].typeValue;
                      this.productModel[l].discountStyle = 'discountStyle';
                    } else {        // new price is lesserthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {       // amount type percentage
                    const temp = this.productModel[l].price - (this.productModel[l].price / 100 * this.discountStore[i].typeValue);

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (this.productModel[l].price > temp) {          // new price is lesserthan previous price
                      this.productModel[l].price = this.productModel[l].oldPrice - (this.productModel[l].oldPrice / 100 * this.discountStore[i].typeValue);
                      this.productModel[l].discount = this.discountStore[i].typeValue + '%';
                      this.productModel[l].discountStyle = 'discountStyle';
                    } else {  // new price is smallerthan previous price
                      continue;
                    }
                  }
                  /* previous offer not applied product-------------- start ------------- */

                } else {
                  this.productModel[l].oldPrice = this.productModel[l].price;
                  if (this.discountStore[i].amountType === 'Flat') {
                    this.productModel[l].price = this.productModel[l].oldPrice - this.discountStore[i].typeValue;
                    this.productModel[l].discount = 'Flat ' + this.discountStore[i].typeValue;
                    this.productModel[l].discountStyle = 'discountStyle';
                  } else {
                    this.productModel[l].price = this.productModel[l].oldPrice - (this.productModel[l].oldPrice / 100 * this.discountStore[i].typeValue);
                    this.productModel[l].discount = this.discountStore[i].typeValue + '%';
                    this.productModel[l].discountStyle = 'discountStyle';
                  }
                }
                /* previous offer not applied product-------------- end ------------- */
              }
            }
          }
        }
      }
    }
  }
  checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.addToWishList(product);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  }
  addToWishList(product) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = product._id;
    this.productService.addToWishList(this.wish).subscribe(data => {
      this.wishList = data;
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
    } else {

    }
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.productService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      console.log('countWishList', data);
      this.wishListLength = data;
      sessionStorage.setItem('wishqty', this.wishListLength.length);
      this.checkWishListEnable();
    }, err => {
      console.log(err);
    });
  }
  addToBag(product) {
    this.productModel.forEach(element => {
      if (element._id === product) {
        element.display = true;
      } else {
        element.display = false;
      }
    });
  }

  skuProductAddToCart(productId, skuItem) {
    const userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.getUserCart(userId, productId, skuItem);
    } else {
      this.addToCartLocal(productId, skuItem);
      this.closeToBag(productId._id);
    }
  }
  getUserCart(userId, productId, skuItem) {
    this.productService.shoppingUser(userId).subscribe(data => {
      this.isCart = false;
      this.addedCart = data;
      this.addedCart.forEach(element => {
        if (element.items.sku === skuItem.sku) {
          this.isCart = true;
        }
      });
      if (this.isCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
        this.addToCartServer(userId, productId, skuItem);
      }
      console.log(this.addedCart);
    }, error => {
      console.log(error);
    });

  }
  addToCartLocal(product, skuItem) {
    const cartLocal = JSON.parse(sessionStorage.getItem('cart')) || [];
    if (cartLocal.length === 0) {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      console.log('checkCArtLength', currentProduct);
      const item = {
        productId: product._id,
        sku: skuItem.sku,
        qty: this.count
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      console.log('chel2', cart);
      totalItem.push(cart);
      this.message = 'Product Added To Cart';
      console.log(totalItem);
      sessionStorage.setItem('cart', JSON.stringify(totalItem));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    } else {
      const totalItem: any = [];
      const currentProduct: any = [];
      currentProduct.push(product);
      const item = {
        productId: product._id,
        sku: skuItem.sku,
        qty: this.count
      };
      const cart = {
        items: item,
        cart_product: currentProduct
      };
      totalItem.push(cart);
      this.isLocalCart = false;
      totalItem.map(element => {
        if (cartLocal.find(s => s.items.sku === element.items.sku)) {
          /* const localSame = cartLocal.find(s => s.items.sku === element.items.sku);
          localSame.items.qty += element.items.qty; */
          this.isLocalCart = true;
        }
        if (this.isLocalCart !== true) {
          cartLocal.push(element);
        }
      });
      if (this.isLocalCart) {
        this.message = 'Already Added To Your Cart';
        this.snackBar.open(this.message, this.action, {
          duration: 500,
        });
      } else {
      this.message = 'Product Added To Cart';
      sessionStorage.setItem('cart', JSON.stringify(cartLocal));
      this.snackBar.open(this.message, this.action, {
        duration: 500,
      });
    }
    }
  }
  addToCartServer(userId, product, skuItem) {
    const totalItem: any = [];
    const cart = {
      productId: product._id,
      sku: skuItem.sku,
      qty: 1
    };
    totalItem.push(cart);
    this.cartModel = new Cart();
    this.cartModel.userId = userId;
    this.cartModel.items = totalItem;
    this.productService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', this.shopModel.length);
      this.message = 'Product Added To Cart';
      this.snackBar.open(this.message, this.action, {
        duration: 3000,
      });
      this.closeToBag(product._id);
    }, error => {
      console.log(error);
    });
  }
  closeToBag(product) {
    this.productModel.forEach(element => {
      if (element._id === product) {
        element.display = false;
      }
    });
  }
  checkWishListEnable() {
    const wishlist: any = this.wishList.map(a => a.productIds);
    sessionStorage.setItem('wislistLength', wishlist.length);
    const obj = {};
    for (const item of wishlist) {
      if (!obj[item.proId]) {
        const element = item.proId;
        obj[element] = true;
      }
    }
    for (const prod of this.productModel) {
      if (obj[prod._id]) {
        prod.wishList = true;
      } else {
        prod.wishList = false;
      }
    }
  }

}
