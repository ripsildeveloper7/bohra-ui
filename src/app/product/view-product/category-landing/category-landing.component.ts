import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, NavigationEnd, Event} from '@angular/router';
import { ProductService } from '../../product.service';
import { AppSetting } from '../../../config/appSetting';

@Component({
  selector: 'app-category-landing',
  templateUrl: './category-landing.component.html',
  styleUrls: ['./category-landing.component.css']
})
export class CategoryLandingComponent implements OnInit {
  id: string;
  holder: any;
  imageUrl: string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private productService: ProductService) {
                    this.activatedRoute.paramMap.subscribe((param: ParamMap) => {
                      this.id = param.get('id');
                      this.getCategory();
                    });
                    this.imageUrl = AppSetting.subCategoryImageUrl;
                   
               }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get('id');
      this.getCategory();
    });
    
  }
  getCategory() {
    this.productService.getSingleCategory(this.id).subscribe(data => {
      this.holder = data;
      console.log('landing ', this.holder);
    }, error => {
      console.log(error);
    });
  }
}
