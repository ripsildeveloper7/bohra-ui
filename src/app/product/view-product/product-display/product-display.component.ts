import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { AppSetting } from './../../../config/appSetting';
import { ProductService } from './../../../product/product.service';
import { Product } from './../../../shared/model/product.model';
import { WishList } from './../../../shared/model/wishList.model';
import { MainCategory } from './../../../shared/model/mainCategory.model';
import { BrandModel } from './../../../shared/model/brand.model';
import { ProductSettings } from './../../../shared/model/product-setting.model';
import { MatSnackBar } from '@angular/material';
import { Filter } from './../all-product/filter.model';
import { Size } from './../../../shared/model/size.model';
import { Cart } from './../../../shared/model/cart.model';
import { FieldAttribute } from './../side-bar/filterAtribute.model';
import { ActivatedRoute, Router, NavigationEnd, PRIMARY_OUTLET, RoutesRecognized, Params, ParamMap, Data } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { filter, map, } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-product-display',
  templateUrl: './product-display.component.html',
  styleUrls: ['./product-display.component.css']
})
export class ProductDisplayComponent implements OnInit {
listProduct:any;
imageLoader = true;
catId: string;
mainCatId: string;
subCatId: string;
  constructor(private activatedRoute: ActivatedRoute, private router: Router, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'hearts',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/hearts.svg'));
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.catId = params.get('catid');
      this.subCatId = params.get('subid');
      this.mainCatId = params.get('maincatid');
    });
   }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: Data) => {
      console.log('new Arrival', data.product);
      this.listProduct=data.product;
    });
    this.getDiscount();
  }
  getDiscount() {
    for (let i = 0; i <= this.listProduct.length - 1; i++) {
      
      this.listProduct[i].savePrice = this.listProduct[i].sp * (this.listProduct[i].discount / 100);
      this.listProduct[i].totalPrice = this.listProduct[i].sp + this.listProduct[i].savePrice;
    }
  }
  onLoad(){
    this.imageLoader = false;
  }
  getProduct(product) {
    console.log(product);
    
      this.router.navigate(['/product/viewsingle/',product.superCategoryId, product._id]);
  
    // } else if (this.catId && this.mainCatId) {
    //   this.router.navigate(['/product/viewsingle/', this.mainCatId, product._id]);
    // } else if (this.catId) {
    //   this.router.navigate(['/product/viewsingle/', this.catId, product._id]);
    // }
    
  }
}
