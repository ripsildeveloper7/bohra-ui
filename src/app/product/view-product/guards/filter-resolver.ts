import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from '../../product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';


@Injectable()
export class FilterResolver implements Resolve<any> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    
    return this.productService.getFilterOptions(); /* .pipe(
      catchError(_ => {
        this.router.navigate(['']);
        return of(new Product());
      })
    ); */
  }
}

