import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from './../../../product/product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouteReuseStrategy,
  RouterStateSnapshot, NavigationEnd,
  Router, ParamMap,
  ActivatedRoute, Data, Event
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';
import { SearchModel } from '../../../shared/model/search.model';
import { Subscription } from 'rxjs';
import { ConcatSource } from 'webpack-sources';

@Injectable()
export class SearchResolver implements Resolve<Product> {
    childParamSubscription: Subscription;
    searchValue;
    paginatorPages:{
      pageNo:number;
      perPage:number;
      searchId: String,
    };
  constructor(
    private productService: ProductService,
    private router: Router, private activatedRoute: ActivatedRoute
  ) 
{}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
    const value = route.paramMap.get('searchID');
    console.log(state.root.firstChild.paramMap.get('searchID'));
    const store = new SearchModel();
    store.searchID = value;
    if(route.queryParamMap.get('pageNo')){
    
      const pageNumber = parseInt(route.queryParamMap.get('pageNo'));
      // const perPages = parseInt(route.queryParamMap.get('perPage'));
      this.paginatorPages = {
        pageNo:pageNumber,
        perPage:30,
        searchId: store.searchID,
      }
    }else{
        this.paginatorPages = {
          pageNo : 1,
          perPage :30,
          searchId: store.searchID,
        }
    }
  
    return this.productService.searchProduct(this.paginatorPages);

  }
  
  // {
  //  router.events.subscribe((event: Event) => {
  //       if (event instanceof NavigationEnd) {
  //         this.childParamSubscription = this.activatedRoute.firstChild.queryParams.subscribe(
  //           (params: ParamMap): void => {
  //             this.searchValue = params.get('search');
  //           });
  //       }
  //     });
  //     this.router.routeReuseStrategy.shouldReuseRoute = function () {
  //         return false;
  //     }; 
  //      this.activatedRoute.queryParamMap.subscribe(queryParam => {
  //         this.searchValue = queryParam['search'];
  //         console.log('search items',this.searchValue);
  //     }) 
  // }

//   resolve(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot
//   ): Observable<Product> {
//     this.router.events.subscribe((event: Event) => {
//         if (event instanceof NavigationEnd) {
//           this.childParamSubscription = this.activatedRoute.firstChild.queryParams.subscribe(
//             (params: ParamMap): void => {
//               this.searchValue = params.get('search');
//               console.log('searched items',this.searchValue);
//             });
//         }
//       }) 
//       route.firstChild.queryParamMap

//     const value = route.queryParamMap.get('search');
//     console.log(state.root.firstChild.queryParamMap.get('search')); 
//     console.log(route.firstChild.queryParamMap); 
//     console.log(state);
//     console.log(value);
//     const store = new SearchModel();
//     store.search = value;
//     return this.productService.searchProduct(store);
//   }

 
 }

