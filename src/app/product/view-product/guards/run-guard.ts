import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, RunGuardsAndResolvers } from '@angular/router';
import { Observable } from 'rxjs';

export const predicate: RunGuardsAndResolvers = (
    from: ActivatedRouteSnapshot,
    to: ActivatedRouteSnapshot,
  ) => from['_urlSegment'].segments.map(e => e.path).join('/') !== to['_urlSegment'].segments.map(e => e.path).join('/')/* {    */
  /*   console.log(from['_urlSegment'].segments.map(e => e.path).join('/'));
    console.log(to['_urlSegment'].segments.map(e => e.path).join('/')); */
    /* if(from['_urlSegment'].segments.map(e => e.path).join('/') !== to['_urlSegment'].segments.map(e => e.path).join('/')) {
      return true;
    }
    else {
     return  false;
  };
  }  */