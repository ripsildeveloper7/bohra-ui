import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ProductService } from '../../product.service';
import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Product } from '../../../shared/model/product.model';

@Injectable()
export class RecentProductResolver implements Resolve<Product> {
  constructor(
    private productService: ProductService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Product> {
    const userId = route.paramMap.get('recentid');
    return this.productService.viewRecentProduct(userId);
  }
}

