
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mobile-filter',
  templateUrl: './mobile-filter.component.html',
  styleUrls: ['./mobile-filter.component.css']
})
export class MobileFilterComponent implements OnInit {
  attributeModel;
  fieldAttributePush;
  constructor(public dialogRef: MatDialogRef<MobileFilterComponent>, private location: Location,
              private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close(true);
  }
  filterAttributeMultiAll(e) {

  }
}
