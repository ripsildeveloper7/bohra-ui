import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSearchProductsComponent } from './view-search-products.component';

describe('ViewSearchProductsComponent', () => {
  let component: ViewSearchProductsComponent;
  let fixture: ComponentFixture<ViewSearchProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSearchProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSearchProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
