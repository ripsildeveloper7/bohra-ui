import { Component, OnInit, Input } from '@angular/core';
import { Product } from './../../../shared/model/product.model';

@Component({
  selector: 'app-product-header',
  templateUrl: './product-header.component.html',
  styleUrls: ['./product-header.component.css']
})
export class ProductHeaderComponent implements OnInit {
  @Input() productModel: Product[];
  sortOrderTrue = false;
  selectedSortVal: 'Default';
  sortBy = [
    {
      type: 'default',
      value: 'Default'
    }, {
    type: 'lowtohigh',
    value: 'Low < High'
  }, {
    type: 'hightolow',
    value: 'High > Low'
  },
  {
    type: 'AtoZ',
    value: 'A To Z'
  },
  {
    type: 'ZtoA',
    value: 'Z To A'
  }];
  constructor() { }

  ngOnInit() {
    this.selectedSortVal = 'Default';
  }
  sortType(val, productModel) {
    this.selectedSortVal = val.value;
    localStorage.setItem('productSortType', val);
    if (val.type === 'lowtohigh') {
        this.lowToHigh(productModel);
    } else if (val.type === 'hightolow') {
        this.highToLow(productModel);
    } else if (val.type === 'AtoZ') {
      this.AtoZSort(productModel);
    } else if (val.type === 'ZtoA') {
      this.ZtoASort(productModel);
    }     else if (val.type === 'default') {
        this.sortOrderTrue = !this.sortOrderTrue;
        productModel = productModel;
    }
  }
  sortOrderType() {
    this.sortOrderTrue = !this.sortOrderTrue;
  }
  highToLow(productModel) {
    this.sortOrderTrue = !this.sortOrderTrue;
    productModel.sort((a, b) => {
      return b.price - a.price;
    });
  }
  lowToHigh(productModel) {
    this.sortOrderTrue = !this.sortOrderTrue;
    productModel.sort((a, b) => {
      return a.price - b.price;
    });
  }
  AtoZSort(productModel) {
    this.sortOrderTrue = !this.sortOrderTrue;
    productModel.sort((a, b) => {
      return a.productName.localeCompare(b.productName);
    });
  }
  ZtoASort(productModel) {
    this.sortOrderTrue = !this.sortOrderTrue;
    productModel.sort((a, b) => {
      return b.productName.localeCompare(a.productName);
    });
  }
}
