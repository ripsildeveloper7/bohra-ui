import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppSetting } from '../config/appSetting';
import { ProductOption } from './../shared/model/product-option.model';
import { Product } from './../shared/model/product.model';
import { ProductOptionValue } from './../shared/model/product-option-value.model';
import { Cart } from './../shared/model/cart.model';
import { AddressModel } from '../account-info/address/address.model';
import { ProductSettings } from '../shared/model/product-setting.model';
import { Router, ActivatedRoute, NavigationEnd, ActivatedRouteSnapshot, Params, ParamMap } from '@angular/router';
import { SuperCategory } from '../home/category-content/category.model';
import { MainCategory } from '../shared/model/mainCategory.model';
import { Review } from '../account-info/review-product/review.mode';
import { WishList } from '../shared/model/wishList.model';
import {Inquiry} from '../shared/model/inquiry.model';


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  contentServiceUrl = AppSetting.contentServiceUrl;
  customerServiceUrl = AppSetting.customerServiceUrl;
  productServiceUrl: string = AppSetting.productServiceUrl;
  marketingServiceUrl: string = AppSetting.marketingServiceUrl;
  commerceOrderServiceUrl = AppSetting.commerceOrderServiceUrl;
  private country_url: string = '../../assets/data/countries.json';

  constructor(private httpClient: HttpClient) { }

  getCountry(){
    return this.httpClient.get(this.country_url);
  }

  getSuperCategoryAttributeFilter(supercatId): Observable<any> {
    const addUrl = 'attributesuperfield/';
    const url: string = this.productServiceUrl + addUrl + supercatId;
    return this.httpClient.get<any>(url);
 }

  getAllSuperCategoryFilter(data): Observable<any> {
    const addUrl = 'supercatfilter';
    const url: string = this.productServiceUrl + addUrl;
    return this.httpClient.post<any>(url, data);
 }
 getAllSubCategoryFilter(data): Observable<any> {
  const addUrl = 'subcatfilter';
  const url: string = this.productServiceUrl + addUrl;
  return this.httpClient.post<any>(url, data);
}
 getAllMainCategoryFilter(data): Observable<any> {
  const addUrl = 'maincatfilter';
  const url: string = this.productServiceUrl + addUrl;
  return this.httpClient.post<any>(url, data);
}

  uploadBannerImage(data): Observable<any> {
    const addUrl = 'banner/';
    const url: string = this.productServiceUrl + addUrl;
    return this.httpClient.put<any>(url, data);
 }
  // get category Product
  getSingleCategory(id): Observable<any> {
    const categoryUrl = 'supercategory/' + id;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Product>(url);
  }
  getAllSuperCategoryProduct(superid,data): Observable<any> {
    const categoryUrl = 'allsupercategory/' + superid;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.put<Product>(url,data);
  }
  getAllMainCategoryProducts(id): Observable<any> {
    const categoryUrl = 'allmaincategory/' + id;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Product>(url);
  }
  getAllSubCategoryProducts(id,data): Observable<any> {
    const categoryUrl = 'allsubcategory/' + id;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.put<Product>(url,data);
  }
  // getAllSubCategoryProducts(id): Observable<any> {
  //   const categoryUrl = 'allsubcategory/' + id;
  //   const url: string = this.productServiceUrl + categoryUrl;
  //   return this.httpClient.get<Product>(url);
  // }
  getAllSubCategory(id): Observable<any> {
    const categoryUrl = 'allsinglesubcategory/' + id;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<SuperCategory>(url);
  }
  getAllMainCategory(id, maincatId): Observable<any> {
    const categoryUrl = 'category/';
    const maincategoryUrl = '/mainCategoryfind/';
    const url: string = this.productServiceUrl + categoryUrl + id + maincategoryUrl + maincatId;
    return this.httpClient.get<MainCategory>(url);
  }
  getFilterOptions() : Observable<any> {
    const filterUrl = 'findfilteroptions';
    const url: string = this.productServiceUrl + filterUrl;
    return this.httpClient.get<any>(url);
  }
  // get products
  getAllProducts(): Observable<any> {
    const categoryUrl = 'product';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Product>(url);
  }
  getAllMainCategoryProduct(maincatId,data): Observable<any> {
    const categoryUrl = 'allmaincategory/' + maincatId;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.put<Product>(url,data);
  }
  getViewCategory(id): Observable<any> {
    const categoryUrl = 'categoryDetails/';
    const url: string = this.productServiceUrl + categoryUrl + id;
    return this.httpClient.get<Product>(url);
  }
  getSingleMatchCategory(supId): Observable<any> {
    const categoryUrl = 'getsinglecategory/';
    const url: string = this.productServiceUrl + categoryUrl + supId;
    return this.httpClient.get<Product>(url);
  }
  
  getAllBrand(): Observable<any> {
    const brandUrl = 'getbrand/';
    const url: string = this.productServiceUrl + brandUrl;
    return this.httpClient.get<Product>(url);
  }

  getProductFilter(): Observable<any> {
    const categoryUrl = 'viewproducts';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Product>(url);
  }

  // filter data

  getFilterData(): Observable<any> {
    const filterURL = 'productSettings/';
    const url: string = this.productServiceUrl + filterURL;
    return this.httpClient.get<Product>(url);
  }
  getSingleProductFilterColors(colors): Observable<any> {
    const filterURL = 'getcolorssingleproduct';
    const url: string = this.productServiceUrl + filterURL;
    return this.httpClient.post<Product>(url, colors);
  }
  getFilterColors(): Observable<any> {
    const filterURL = 'getcolorsetting';
    const url: string = this.productServiceUrl + filterURL;
    return this.httpClient.get<Product>(url);
  }
  // filter brand

  getFilterBrand(): Observable<any> {
    const filterURL = 'getbrand';
    const url: string = this.productServiceUrl + filterURL;
    return this.httpClient.get<Product>(url);
  }

  // filter category

  getAllCategory(): Observable<any> {
    const filterURL = 'viewCategory';
    const url: string = this.productServiceUrl + filterURL;
    return this.httpClient.get<Product>(url);
  }

  // filter method
  filterByColor(id, data): Observable<any> {
    const categoryUrl = 'categoryColor/';
    const url: string = this.productServiceUrl + categoryUrl + id;
    return this.httpClient.put<Product>(url, data);
  }

  // view single product

  getSingleProducts(id): Observable<any> {
    const categoryUrl = 'productsingle/';
    const url: string = this.productServiceUrl + categoryUrl + id;
    return this.httpClient.get<Product>(url);
  }
  getSingleProductsWithBrand(id): Observable<any> {
    const categoryUrl = 'singleproductwithbrand/';
    const url: string = this.productServiceUrl + categoryUrl + id;
    return this.httpClient.get<Product>(url);
  }

  getFilterbyMultipleData(data): Observable<any> {
    const categoryUrl = 'findmultipledata';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Product>(url, data);
  }

  getRelatedProducts(data): Observable<any> {
    const productUrl = 'relatedproducts/';
    const productUrl1 = '/product/';
    const url: string = this.productServiceUrl + productUrl + data.styleCode + productUrl1 + data._id;
    return this.httpClient.get<Product>(url);
  }

  addToCart(cart): Observable<Cart> {
    const cartUrl = 'cart';
    const url: string = this.productServiceUrl + cartUrl;
    return this.httpClient.post<Cart>(url, cart);
  }

  addToCartDecrement(cart): Observable<Cart> {
    const cartUrl = 'findcartproduct/';
    const url: string = this.productServiceUrl + cartUrl;
    return this.httpClient.post<Cart>(url, cart);
  }

  addToCartTest(prod): Observable<any>  {
    const categoryUrl = 'cart';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.post<Product>(url, prod);
  }
  deleteToCart(userid, proId): Observable<any>  {
    const cartUrl = 'deletecart/';
    const productUrl = '/itemId/';
    const url: string = this.productServiceUrl + cartUrl + userid + productUrl + proId;
    return this.httpClient.delete<Cart>(url);
  }
  shoppingUser(userId): Observable<any>  {
    const shoppingUrl = 'findcart/';
    const url: string = this.productServiceUrl + shoppingUrl + userId;
    return this.httpClient.get<Cart>(url);
  }
  shoppingCart(): Observable<any>  {
    const shoppingUrl = 'shopping/';
    const url: string = this.productServiceUrl + shoppingUrl;
    return this.httpClient.get<Product>(url);
  }

  addToCartMinus(cart ): Observable<any> {
    const cartUrl = 'cart/';
    const productUrl = '/decproduct/';
    const url: string = this.productServiceUrl + cartUrl + cart.userId + productUrl + cart.product.productId;
    return this.httpClient.put<Product>(url, cart);
  }
  allProductOption(): Observable<any> {
    const productUrl = 'getproductoption';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<ProductOption>(url);
  }
  /* placeOrder(data: SingleProductOrder): Observable<any> {
    const categoryUrl = 'orderproduct/';
    const url: string = this.serviceUrl + categoryUrl ;
    return this.httpClient.post<SingleProductOrder>(url, data);
  } */

  // customer Details
  getCustomerDetails(id): Observable<any> {
    const filterURL = 'customerDetail/' + id;
    const url: string = this.productServiceUrl + filterURL;
    return this.httpClient.get<Product>(url);
  }

  // add new addres details
  getaddressDetails(addressHolder, id): Observable<AddressModel> {
    const urladdress = this.productServiceUrl + 'addressupdate/' + id;
    return this.httpClient.put<AddressModel>(urladdress, addressHolder);
  }
  getProductSettings(): Observable<any> {
    const categoryUrl = 'productSettings';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<ProductSettings>(url);
  }

  getSingleProductReview(id): Observable<any> {
    const pathUrl = 'getselectedproductreview/';
    const url: string = this.productServiceUrl + pathUrl + id;
    return this.httpClient.get<Review>(url);
  }

  //get attribute
  getSuperCategoryAttribute(supcatId): Observable<any> {
    const pathUrl = 'attributesuperfields/';
    const url: string = this.productServiceUrl + pathUrl + supcatId;
    return this.httpClient.get<any>(url);
  }
  getSubCategoryAttribute(subcatId): Observable<any> {
    const pathUrl = 'attributesubfields/';
    const url: string = this.productServiceUrl + pathUrl + subcatId;
    return this.httpClient.get<any>(url);
  }
  // Marketing

  getAllDiscount(): Observable<any> {
    const pathUrl = 'getdiscount';
    const url: string = this.marketingServiceUrl + pathUrl;
    return this.httpClient.get<any>(url);
  }
  /* wishlist */
  getWishList(wish): Observable<WishList[]> {
    const pathUrl = 'getwishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.get<WishList[]>(url);
  }
  addToWishList(wish): Observable<WishList[]> {
    const pathUrl = 'wishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.put<WishList[]>(url, wish);
  }
  // Recent Product
  getRecentProductID(userId): Observable<any> {
    const pathUrl = 'getrecentproduct/';
    const url: string = this.productServiceUrl + pathUrl + userId;
    return this.httpClient.get<any>(url);
  }
  addRecentProductID(recentProduct): Observable<any> {
    const pathUrl = 'addrecentproduct';
    const url: string = this.productServiceUrl + pathUrl;
    return this.httpClient.post<any>(url, recentProduct);
  }
  viewRecentProduct(userId): Observable<any> {
    const pathUrl = 'getrecentproductbyuser/';
    const url: string = this.productServiceUrl + pathUrl + userId;
    return this.httpClient.get<any>(url);
  }
  getRecentProductByLocalStorage(recentProduct): Observable<any> {
    const pathUrl = 'getproductforlocalrecent';
    const url: string = this.productServiceUrl + pathUrl;
    return this.httpClient.post<any>(url, recentProduct);
  }
  searchProduct(data): Observable<any> {
    const categoryUrl = 'searchproduct' ;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.post<any>(url,data);
  }
  
  getSearchProductFilter(data): Observable<any> {
    const addUrl = 'searchproductfilter';
    const url: string = this.productServiceUrl + addUrl;
    return this.httpClient.post<any>(url, data);
 }
  getAllBrandProduct(brandId): Observable<any> {
    const categoryUrl = 'allproductbybrand/' + brandId;
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Product>(url);
  }
  getPromotionProduct(productID): Observable<any> {
    const categoryUrl = 'getpromotionproduct';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.post<Product>(url, productID);
  }
  getProductCollection(superID, tagID): Observable<any> {
    const categoryUrl = 'getcategoryforproduct/';
    const tagUrl = '/gettagforproduct/';
    const url: string = this.productServiceUrl + categoryUrl + superID + tagUrl + tagID;
    return this.httpClient.get<any>(url);
  }
  // find attribue product
  getAllSuperCategoryAttributeFilter(data): Observable<any> {
  const categoryUrl = 'supattributeproduct';
  /* const attributeUrl = '/attribute/';
  const attributeFieldUrl = '/attributefield/'; */
  const url: string = this.productServiceUrl + categoryUrl;
  return this.httpClient.post<any>(url, data);
}
getPagination(data): Observable<any> {
  const categoryUrl = 'pagination';
  const url: string = this.productServiceUrl + categoryUrl;
  return this.httpClient.put<any>(url, data);
}
 // find attribue product
 getProductAttribute(catId, attributeId, attributefieldId): Observable<any> {
    const categoryUrl = 'supercat/';
    const attributeUrl = '/attribute/';
    const attributeFieldUrl = '/attributefield/';
    const url: string = this.productServiceUrl + categoryUrl + catId + attributeUrl + attributeId + attributeFieldUrl + attributefieldId;
    return this.httpClient.get<any>(url);
  }
  
  // save the inquiry
  saveInquiry(inquiry): Observable<any> {
    const categoryUrl = 'inquiry' ;
    const url: string = this.customerServiceUrl + categoryUrl;
    return this.httpClient.post<any>(url, inquiry);
  }

  // you may also like

  getAlsoLike(content): Observable<any> {
    const productUrl = 'getyoumayalsolike';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.post<any>(url, content);
  }

  getAllMeasurement(): Observable<any> {
    const productUrl = 'getallmeasurement';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  addMeasuremenByUser(data): Observable<any> {
    const productUrl = 'addmeasurementbycustomer';
    const url: string = this.customerServiceUrl + productUrl;
    return this.httpClient.post<any>(url, data);
  }
  getSelectedSuperCategory(id): Observable<any> {
    const productUrl = 'getselectedsupercategorymeasurement/';
    const url: string = this.productServiceUrl + productUrl + id;
    return this.httpClient.get<any>(url);
  }
  getSelectedSuperCategoryForKameez(id): Observable<any> {
    const productUrl = 'getselectedsupercategorykameezmeasurement/';
    const url: string = this.productServiceUrl + productUrl + id;
    return this.httpClient.get<any>(url);
  }
  getSelctedMeasurementByUser(id): Observable<any> {
    const productUrl = 'getselectedmeasurement/';
    const url: string = this.customerServiceUrl + productUrl + id;
    return this.httpClient.get<any>(url);
  }
  getAllShipmentSetting(): Observable<any> {                           //  get All shipment setting
    const categoryUrl = 'getallshipmentsetting';
    const url: string = this.commerceOrderServiceUrl + categoryUrl;
    return this.httpClient.get<any>(url);
  }
  getAllKameezMeasurement(): Observable<any> {
    const productUrl = 'getallkameezmeasurement';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getMesurementByUser(id, mesId): Observable<any> {                           //  get All shipment setting
    const categoryUrl = 'getmeasurementforuser/';
    const url: string = this.customerServiceUrl + categoryUrl + id + '/' + mesId;
    return this.httpClient.get<any>(url);
  }
  getIncrementRate(): Observable<any> {
    const productUrl = 'getincrementrate';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getColors(): Observable<any> {
    const colorUrl = 'getcolorsetting';
    const url: string = this.productServiceUrl + colorUrl;
    return this.httpClient.get<any>(url);
  }
  getLehengaMeasurement(id): Observable<any> {
    const colorUrl = 'getlehengabycategory/';
    const url: string = this.productServiceUrl + colorUrl + id;
    return this.httpClient.get<any>(url);
  }
  getAllLehengaMeasurement(): Observable<any> {
    const productUrl = 'getlehengameasurement';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getCatNote(): Observable<any> {
    const productUrl = 'getnote';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }

  getCategoryWithFilter(id): Observable<any> {
    const productUrl = 'getsupercategoryandfilter/';
    const url: string = this.productServiceUrl + productUrl + id;
    return this.httpClient.get<any>(url);
  }
  getSubCategoryWithFilter(id): Observable<any> {
    const productUrl = 'getsubcategoryandfilter/';
    const url: string = this.productServiceUrl + productUrl + id;
    return this.httpClient.get<any>(url);
  }
  getAllBodyHeight(): Observable<any> {
    const productUrl = 'getallbodyheight';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
}
