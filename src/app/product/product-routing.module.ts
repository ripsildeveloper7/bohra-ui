import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AllProductComponent } from './view-product/all-product/all-product.component';
import { SingleProductComponent } from './view-single-product/single-product/single-product.component';
import { ProductItemComponent } from './view-product/product-item/product-item.component';
import { ProductResolver } from './view-product/guards/product-resolver';
import { AllProductResolver } from './view-product/guards/allproduct-resolver';
import { FilterResolver } from './view-product/guards/filter-resolver';
import { MainCategoryProductResolver } from './view-product/guards/maincategory-product-resolver';
import { SubCategoryProductResolver } from './view-product/guards/subcategory-product-resolver';
import { ProductItemResolver } from './view-single-product/guard/product-item.resolver';
import { ReadyToShipResolver } from './view-single-product/guard/ready-to-ship.resolver';
import { CategoryProductResolver } from './view-single-product/guard/category-product.resolver';
import { CategoryResolver } from './view-product/guards/category-resolver';
import { ProductZoomComponent } from './view-single-product/product-zoom/product-zoom.component';
import { BrandResolver } from '../product/view-product/guards/brand-resolver';
import { CollectionResolver } from './view-product/guards/collection-resolver';
import { PromotionResolver } from './view-product/guards/promotion-resolver';
import { RecentProductResolver } from './view-product/guards/recentProduct-resolver';
import { SearchResolver } from './view-product/guards/search-resolver';
import { AllAttributeResolver } from './view-product/guards/all-attribute-resolver';
import { SingleCategoryResolver } from './view-product/guards/single-category-resolver';
import { AttributeProductResolver } from './view-product/guards/attribute-product-resolver';
import { MeasurementPageComponent } from './view-single-product/measurement-page/measurement-page.component';
import { NewArriavalResolver } from './view-product/guards/newArrivals-resolver';
import { ProductDisplayComponent } from './view-product/product-display/product-display.component';
import { KameezMeasurementPageComponent } from './view-single-product/kameez-measurement-page/kameez-measurement-page.component';
import { LehengaMeasurementPageComponent } from './view-single-product/lehenga-measurement-page/lehenga-measurement-page.component';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router, RunGuardsAndResolvers
} from '@angular/router';
import { TestGuardsGuard } from './view-product/guards/test-guards.guard';
import { predicate } from './view-product/guards/run-guard';
import { CategoryLandingComponent } from './view-product/category-landing/category-landing.component';
import { CatAttributeResolver } from './view-product/guards/cat-attribute-resolver';
import { ViewSearchProductsComponent } from './view-product/view-search-products/view-search-products.component';
export function callGuardResolver(from: ActivatedRouteSnapshot,  to: ActivatedRouteSnapshot) {
  if (from['_urlSegment'].segments.map(e => e.path).join('/') !== to['_urlSegment'].segments.map(e => e.path).join('/')) {
    return true;
  } else {
   return  false;
}
}

const routes: Routes = [
  {
    path: 'test',
    component: ProductZoomComponent
  },
  {
    path: 'supercategory/:catid',
    component: AllProductComponent,
    canActivate: [TestGuardsGuard],
    resolve: {
      common: CatAttributeResolver
     /*  category: CategoryResolver, filterOption: FilterResolver, attribute: 
      AllAttributeResolver, common: SingleCategoryResolver */
    },
    runGuardsAndResolvers: callGuardResolver,
    data: { breadcrumb: 'Home' },
    children: [
/*       { path: '', component: ProductItemComponent, resolve: {
        product: AllProductResolver
      }}, */
    { path: '', component: ProductItemComponent, canActivateChild: [TestGuardsGuard],  resolve: {
      product: ProductResolver
    }},
    { path: 'maincategory/:maincatid', component: ProductItemComponent, resolve: {
      product: MainCategoryProductResolver
    }},
    { path: 'maincategory/:maincatid/subcategory/:subid', component: ProductItemComponent, 
     resolve: {
      product: SubCategoryProductResolver
    }},
    {
         path: 'attribute/:attributeId/attributefiled/:attributefieldId', component: ProductItemComponent, resolve: {
          product: AttributeProductResolver
      }
    },
    {
      path: 'brandproduct/:brandid', component: ProductItemComponent, resolve: {
        product: BrandResolver
      }
    },
    {
      path: 'collectionTag/:collectionid', component: ProductItemComponent, resolve: {
        product: CollectionResolver
      }
    },
    {
      path: 'promotion/:promotionid', component: ProductItemComponent, resolve: {
        product: PromotionResolver
      }
    },
    {
      path: 'recentlyviewed/:recentid', component: ProductItemComponent, resolve: {
        product: RecentProductResolver
      }
    },
  
  ]
  },
  {
    path: 'searchProduct/:searchID', component: ViewSearchProductsComponent,
    canActivate: [TestGuardsGuard],
    runGuardsAndResolvers: callGuardResolver,
    resolve: {
    product: SearchResolver,
    }
  },
  {
    path: 'viewsingle/:selectcatid/:id',
    component: SingleProductComponent, resolve: {
      category: CategoryProductResolver, product: ProductItemResolver, readyToShip: ReadyToShipResolver
    }
  },
  {
    path: 'viewProduct',
    component: ProductDisplayComponent
  },
  {
    path: 'newarrival/:num', component: ProductDisplayComponent, resolve: {
      product: NewArriavalResolver
    }
  },
  {
    path: 'measurement/:id',
    component: MeasurementPageComponent
  },
  {
    path: 'kameezmeasurement/:id',
    component: KameezMeasurementPageComponent
  },
  {
    path: 'lehengameasurement/:id',
    component: LehengaMeasurementPageComponent
  },
  {
    path: 'categorylandingpage/:id',
    component: CategoryLandingComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
  RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})

export class ProductRoutingModule { }
