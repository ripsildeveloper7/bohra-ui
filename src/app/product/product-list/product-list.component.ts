import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatPaginatorIntl } from '@angular/material';
import { ParamMap, ActivatedRoute, NavigationEnd, Router, Params } from '@angular/router';
import { ProductService } from './../product.service';
import { Product } from '../../shared/model/product.model';
import { Observable, } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Filter } from './filter.model';
import { Cart } from '../../shared/model/cart.model';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  /* @ViewChild(MatPaginator) paginator: MatPaginator; */
  message;
  action;
  productModel: any;
 
  productModel$: Observable<string>;
  cat: { catId: string };
  cart: Cart;
  cartModel: any;
  public pageSize = 10;
  public currentPage = 0;
  public totalSize = 0;
  filterPrice;
  filterColor;
  filterMaterial;
  sortBy = [{
    type: 'lowtohigh',
    value: 'Price - Low To High'
  }, {
    type: 'hightolow',
    value: 'Price -  High To Low'
  }];
  selectedCheckBox;
  selectedMaterialCheckBox;
  selectedPriceCheckBox;
  selectedSortVal;
  public array: any;
  public displayedColumns = ['', '', '', '', ''];
  public dataSource: any;
  filterModel: Filter;
  resultdata: any;
  userId: string;
  constructor(public productService: ProductService, private route: ActivatedRoute, private router: Router, private snackBar: MatSnackBar) {

  }
  ngOnInit() {
    localStorage.removeItem('filterMaterial');
    localStorage.removeItem('minimumPriceFilter');
    localStorage.removeItem('maximumPriceFilter');
    localStorage.removeItem('filterColor');
    /*  this.productModel$ = this.route.paramMap.pipe(
       switchMap(params => {
         this.catid = params.get('catId');
         this.viewCategory();
                return this.catid;
       })
     ); */
    this.cat = {
      catId: this.route.snapshot.params.catid
    };
    this.route.params.subscribe((params: Params) => {
      this.cat.catId = params.catId;
      this.viewCategory();
    });
    this.userId = sessionStorage.getItem('userId');

    /* this.getProducts(); */

  }
  getProducts() {
    this.productService.getAllProducts().subscribe(data => {
      this.productModel = data;
    }, err => {
      console.log(err);
    });
  }

  sortType(val) {
    this.selectedSortVal = val;
    localStorage.setItem('productSortType', val);
    if (val === 'lowtohigh') {
      this.lowToHigh();
    } else if (val === 'hightolow') {
      this.highToLow();
    }
  }
  highToLow() {
    this.productModel.sort((a, b) => {
      return b.price - a.price;
    });
  }
  viewCategory() {
    this.productService.getViewCategory(this.cat.catId).subscribe(data => {
      const val = localStorage.getItem('productSortType');
      this.selectedSortVal = val;
      if (val === 'lowtohigh') {
        data.sort((a, b) => {
          return a.price - b.price;
        });
        this.productModel = data;
        console.log('sorted product low to high', this.productModel);
      } else if (val === 'hightolow') {
        data.sort((a, b) => {
          return b.price - a.price;
        });
        this.productModel = data;
      } else if (val === undefined || val === null) {
        this.productModel = data;
      }
      /* this.productModel.paginator = this.paginator; */
      this.productModel = data;
      this.array = data;
      this.totalSize = this.array.length;
      this.iterator();
    }, error => {
      console.log(error);
    });
  }
  lowToHigh() {

    this.productModel.sort((a, b) => {
      return a.price - b.price;
    });
  }
  public handlePage(e: any) {
    console.log('paginator', e);
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }
  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.array.slice(start, end);
    this.productModel = part;
  }
  viewSingleProduct(id) {
    this.router.navigate(['/product/productview', id]);
  }
}
