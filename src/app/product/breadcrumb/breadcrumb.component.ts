import { Component, OnInit, Input } from '@angular/core';
import { Data, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  @Input() superCatName;
  @Input() mainCatName;
  @Input() mainCatId;
  @Input() subCatName;
  @Input() productName;
  @Input() supCatId;
  details;
  showMobileView1 = false;
  constructor(private activatedRoute: ActivatedRoute,private router: Router) {
    if (window.screen.width > 1024) {
      this.showMobileView1 = false;
    } else {
      this.showMobileView1 = true;
    }
   }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data: Data) => {
      this.details = data;
      console.log(data, 'single product');
      console.log(data.product.subCategoryId)
    });
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 1024) {
      this.showMobileView1 = false;
    } else {
      this.showMobileView1 = true;
    }
  }

  supCatLink(){
    this.router.navigate(['/product/supercategory/',this.supCatId])
  }


}
