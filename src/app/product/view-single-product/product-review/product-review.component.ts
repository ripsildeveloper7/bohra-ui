import { Component, OnInit, Input } from '@angular/core';
import { Review } from '../../../account-info/review-product/review.mode';

@Component({
  selector: 'app-product-review',
  templateUrl: './product-review.component.html',
  styleUrls: ['./product-review.component.css']
})
export class ProductReviewComponent implements OnInit {
  @Input() reviewData: Review;
  @Input() totalRating: any;
    constructor() { }

  ngOnInit() {}

}
