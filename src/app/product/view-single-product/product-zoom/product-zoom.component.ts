import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Zoom } from './zoom.model';
import { AppSetting } from './../../../config/appSetting';
import { Product } from './../../../shared/model/product.model';
import { WindowScrollingService } from './window-scrolling.service';

@Component({
  selector: 'app-product-zoom',
  templateUrl: './product-zoom.component.html',
  styleUrls: ['./product-zoom.component.css']
})
export class ProductZoomComponent implements OnInit {
  @Input()  zoom: Zoom;
  @Input() productModel: Product;
  productImageUrl: string = AppSetting.productImageUrl;
  @Input() slideIndex = 0;
  constructor(private windowScrolling: WindowScrollingService) { 
    this.windowScrolling.disable();
  }

  ngOnInit() {
    this.windowScrolling.enable();
  }
  clickRight(array, slideIndex) {
    if (slideIndex >= array.length - 1) {
      this.slideIndex = 0;
    } else {
      this.slideIndex = slideIndex + 1;
    }
    array.forEach((element, index) => {
      if (index === this.slideIndex) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }
  clickCenter(array, indexValue) {
    array.forEach((element, index) => {
      if (index === indexValue) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
    this.slideIndex  = indexValue;
  }
  clickLeft(array, indexValue) {
    if (indexValue <= 0) {
      this.slideIndex = array.length - 1 ;
    } else {
      this.slideIndex = indexValue - 1;
    }
    array.forEach((element, index) => {
      if (index === this.slideIndex) {
        element.display = 'displayBlock';
      } else {
        element.display = 'displayNone';
      }
    });
  }
  closeZoom() {
    this.zoom.displayClass = 'displayNone';
    this.windowScrolling.enable();
  }
}
