import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { HowToMeasureComponent } from './how-to-measure.component';
import { HowToMeasure } from '../../../shared/model/howToMeasure.model'

@Injectable({
  providedIn: 'root'
})
export class HowToMeasureService {
  dialogRef: MatDialogRef<HowToMeasureComponent>;
  constructor(private dialog: MatDialog) { }

  openPopUp(val?: any): Observable<boolean> {
    this.dialogRef = this.dialog.open(HowToMeasureComponent,
      {
        disableClose: true, backdropClass: 'light-backdrop',
       /*  width: '1020px',
        height: '420px', */
        width: '550px',
        height:'auto',
        data: val,
      });
      console.log(val);
    return this.dialogRef.afterClosed();
  }
  closePop() {
    if (this.dialogRef) {
      this.dialogRef.close();
    }
  }
}
