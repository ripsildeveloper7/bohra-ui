import { TestBed } from '@angular/core/testing';

import { HowToMeasureService } from './how-to-measure.service';

describe('HowToMeasureService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HowToMeasureService = TestBed.get(HowToMeasureService);
    expect(service).toBeTruthy();
  });
});
