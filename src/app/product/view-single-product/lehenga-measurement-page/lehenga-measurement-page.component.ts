import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AppSetting } from '../../../config/appSetting';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LehengaMeasurement } from '../../../shared/model/lehengaMeasurement.model';

@Component({
  selector: 'app-lehenga-measurement-page',
  templateUrl: './lehenga-measurement-page.component.html',
  styleUrls: ['./lehenga-measurement-page.component.css']
})
export class LehengaMeasurementPageComponent implements OnInit {
  measurementForm: FormGroup;
  prodId: string;
  measurmentUrl: string;
  userId: string;
  productModel: any;
  productImage: {
    imageName: string,
    title: string
  };
  measurementData: any;
  catId: any;
  show = false;
  show1 = false;
  show2 = false;
  show3 = false;
  show4 = false;
  show5 = false;
  show6 = false;
  show7 = false;
  show8 = false;
  show9 = false;
  show10 = false;
  show11 = false;
  show12 = false;
  show13 = false;
  show14 = false;
  frontNeckData: any;
  backNeckData: any;
  sleeveNeckData: any;
  bottomStyleData: any;
  typeData: any;
  ratioCheckType: any;
  ratioCheckEvent3: any;
  ratioCheckEvent2: any;
  ratioCheckEvent1: any;
  ratioCheckEvent: any;
  aroundAbWaist: any;
  aroundWaistIn: boolean;
  aroundHipLeng: any;
  blouseIn: boolean;
  kameezLeng: any;
  kameezIn: boolean;
  frontNeckDep: any;
  fnIn: boolean;
  backNeckDep: any;
  bnIn: boolean;
  aroundB: any;
  aroundBIn: boolean;
  aroundAr: any;
  armIn: boolean;
  aroundW: any;
  aroundWIn: boolean;
  aroundT: any;
  aroundTIn: boolean;
  sleevLen: any;
  sleeveIn: boolean;
  aroundK: any;
  aroundKIn: boolean;
  aroundC: any;
  aroundCIn: boolean;
  aroundBot: any;
  aroundBotIn: boolean;
  bottomL: any;
  bottomLIn: boolean;
  ale: boolean;
  temp: LehengaMeasurement;
  showReview: boolean;
  holder: {
    productId: string,
    measurementId: string,
    userId: string
  };
  container: any;
  measurementModel: any;
  isSaved = false;

  constructor(private productService: ProductService, private router: Router,
              private activatedRoute: ActivatedRoute, private fb: FormBuilder) {
                this.activatedRoute.paramMap.subscribe((param: ParamMap) => {
                  this.prodId = param.get('id');
                });
                this.getSingleProduct();
                this.measurmentUrl = AppSetting.measurementImageUrl;
               
                this.userId = sessionStorage.getItem('userId');
               /*  this.getMeasurmentByUser(); */
               }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.measurementForm = this.fb.group({
      typeName: ['', Validators.required],
      aroundBust: ['', Validators.required],
      aroundBustInput: ['', Validators.max(40)],
      aroundAboveWaist: ['', Validators.required],
      choliLength: ['', Validators.required],
      frontNeckStyle: [''],
      backNeckStyle: [''],
      sleeveStyle: [''],
      choliClosingSide: [''],
      choliClosingWith: [''],
      lining: [''],
      aroundWaist: ['', Validators.required],
      aroundHip: ['', Validators.required],
      lehengaClosingSide: [''],
      lehengaClosingWith: [''],
      lehengaLength: ['', Validators.required],
      specialInstruction: [''],
    });
  }
  getSingleProduct() {
    this.productService.getSingleProducts(this.prodId).subscribe(data => {
      this.productModel = data;
      console.log(this.productModel);
      this.productModel.child.forEach(child => {
        if (child.headChild === true) {
          this.productImage = {
            imageName: child.productImage[0].productImageName,
            title: 'As Shown'
          };
        }
      });
      if (this.productModel.subCategoryId) {

      } else if (this.productModel.mainCategoryId) {

      } else {
        this.catId = this.productModel.superCategoryId;
      }
      this.getAllMeasurement();
    }, error => {
      console.log(error);
    });
  }
  getMeasurmentByUser() {
    this.productService.getMesurementByUser(this.userId, this.measurementData._id).subscribe(data => {
      this.container = data;
      console.log('check', this.container);
    }, error => {
      console.log(error);
    });
  }
  measurmentSelected(e) {
    console.log(e.value);
    if (e.value === 'createNew') {
      this.isSaved = false;
      this.measurementForm.reset();
    } else {
      this.isSaved = true;
      this.measurementModel = e.value;
    }
  }
  getAllMeasurement() {
    this.productService.getAllLehengaMeasurement().subscribe(data => {
      this.measurementData = data[0];
      this.measurementData.choliMeasurement[0].frontNeckStyle = this.setImage(this.measurementData._id, this.measurementData.choliMeasurement[0].frontNeckStyle);
      this.measurementData.choliMeasurement[0].backNeckStyle = this.setImage(this.measurementData._id, this.measurementData.choliMeasurement[0].backNeckStyle);
      this.measurementData.choliMeasurement[0].sleeveStyle = this.setImage(this.measurementData._id, this.measurementData.choliMeasurement[0].sleeveStyle);
      const front =  [this.productImage].concat(this.measurementData.choliMeasurement[0].frontNeckStyle);
      const back = [this.productImage].concat(this.measurementData.choliMeasurement[0].backNeckStyle);
      const sleeve = [this.productImage].concat(this.measurementData.choliMeasurement[0].sleeveStyle);
      this.measurementData.choliMeasurement[0].frontNeckStyle = front;
      this.measurementData.choliMeasurement[0].backNeckStyle = back;
      this.measurementData.choliMeasurement[0].sleeveStyle = sleeve;
      console.log('measurement', this.measurementData);
      this.getMeasurmentByUser();
    }, error => {
      console.log(error);
    });
  }
  setImage(id, array) {
    for (const img of array) {
      img.imageName = this.measurmentUrl + id + '/' + img.imageName;
    }
    return array;
  }
  view() {
    this.show = !this.show;
  }
  view1() {
    this.show1 = !this.show1;
  }
  view2() {
    this.show2 = !this.show2;
  }
  view3() {
    this.show3 = !this.show3;
  }
  view4() {
    this.show4 = !this.show4;
  }
  view5() {
    this.show5 = !this.show5;
  }
  view6() {
    this.show6 = !this.show6;
  }
  view7() {
    this.show7 = !this.show7;
  }
  view8() {
    this.show8 = !this.show8;
  }
  view9() {
    this.show9 = !this.show9;
  }
  view10() {
    this.show10 = !this.show10;
  }
  view11() {
    this.show11 = !this.show11;
  }
  view12() {
    this.show12 = !this.show12;
  }
  view13() {
    this.show13 = !this.show13;
  }
  view14() {
    this.show14 = !this.show14;
  }
  changeEvent(event, full) {
    console.log(full);
    this.measurementForm.controls.frontNeckStyle.reset();
    this.frontNeckData = full;
    this.ratioCheckEvent = event.value;
  }

  changeEvent1(event, full) {
    this.measurementForm.controls.backNeckStyle.reset();
    this.backNeckData = full;
    this.ratioCheckEvent1 = event.value;
  }

  changeEvent2(event, full) {
    this.measurementForm.controls.sleeveStyle.reset();
    this.sleeveNeckData = full;
    this.ratioCheckEvent2 = event.value;
  }
  changeEvent3(event, full) {
    this.measurementForm.controls.bottomStyle.reset();
    this.bottomStyleData = full;
    this.ratioCheckEvent3 = event.value;
  }
  changeEventType(event, full, array) {
    /*   this.measurementForm.controls.typeName.reset(); */
      array.forEach(e => e.checked = false);
      this.typeData = full;
      /* this.ratioCheckType = event.value; */
      event.value.checked = true;
    }
  inputW(event, max, min) {
    this.aroundAbWaist = event.target.value;
    if ( this.aroundAbWaist < min  && this.aroundAbWaist > 0) {
      this.aroundWaistIn = true;
    } else if (this.aroundAbWaist > max) {
      this.aroundWaistIn = true;
    } else {
      this.aroundWaistIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputB(event, max, min) {
    this.aroundHipLeng = event.target.value;
    if ( this.aroundHipLeng < min  && this.aroundHipLeng > 0) {
      this.blouseIn = true;
    } else if (this.aroundHipLeng > max) {
      this.blouseIn = true;
    } else {
      this.blouseIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputK(event, max, min) {
    this.kameezLeng = event.target.value;
    if ( this.kameezLeng < min  && this.kameezLeng > 0) {
      this.kameezIn = true;
    } else if (this.kameezLeng > max) {
      this.kameezIn = true;
    } else {
      this.kameezIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputFN(event, max, min) {
    this.frontNeckDep = event.target.value;
    if ( this.frontNeckDep < min  && this.frontNeckDep > 0) {
      this.fnIn = true;
    } else if (this.frontNeckDep > max) {
      this.fnIn = true;
    } else {
      this.fnIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputBN(event, max, min) {
    this.backNeckDep = event.target.value;
    if ( this.backNeckDep < min  && this.backNeckDep > 0) {
      this.bnIn = true;
    } else if (this.backNeckDep > max) {
      this.bnIn = true;
    } else {
      this.bnIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputBF(event, max, min) {
    this.aroundB = event.target.value;
    if ( this.aroundB < min  && this.aroundB > 0) {
      this.aroundBIn = true;
    } else if (this.aroundB > max) {
      this.aroundBIn = true;
    } else {
      this.aroundBIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputArm(event, max, min) {
    this.aroundAr = event.target.value;
    if ( this.aroundAr < min && this.aroundAr > 0) {
      this.armIn = true;
    } else if (this.aroundAr > max) {
      this.armIn = true;
    } else {
      this.armIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputWaist(event, max, min) {
    this.aroundW = event.target.value;
    if ( this.aroundW < min && this.aroundW > 0) {
      this.aroundWIn = true;
    } else if (this.aroundW > max) {
      this.aroundWIn = true;
    } else {
      this.aroundWIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputThigh(event, max, min) {
    this.aroundT = event.target.value;
    if ( this.aroundT < min && this.aroundT > 0) {
      this.aroundTIn = true;
    } else if (this.aroundT > max) {
      this.aroundTIn = true;
    } else {
      this.aroundTIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputSleeve(event, max, min) {
    this.sleevLen = event.target.value;
    if ( this.sleevLen < min && this.sleevLen > 0) {
      this.sleeveIn = true;
    } else if (this.sleevLen > max) {
      this.sleeveIn = true;
    } else {
      this.sleeveIn = false;
    }
    // console.log(this.aroundMax);
  }

  inputKnee(event, max, min) {
    this.aroundK = event.target.value;
    if ( this.aroundK < min && this.aroundK > 0) {
      this.aroundKIn = true;
    } else if (this.aroundK > max) {
      this.aroundKIn = true;
    } else {
      this.aroundKIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputCalf(event, max, min) {
    this.aroundC = event.target.value;
    if ( this.aroundC < min && this.aroundC > 0) {
      this.aroundCIn = true;
    } else if (this.aroundC > max) {
      this.aroundCIn = true;
    } else {
      this.aroundCIn = false;
    }
    // console.log(this.aroundMax);
  }

  inputBottom(event, max, min) {
    this.aroundBot = event.target.value;
    if ( this.aroundBot < min && this.aroundBot > 0) {
      this.aroundBotIn = true;
    } else if (this.aroundBot > max) {
      this.aroundBotIn = true;
    } else {
      this.aroundBotIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputABottom(event, max, min) {
    this.bottomL = event.target.value;
    if ( this.bottomL < min && this.bottomL > 0) {
      this.bottomLIn = true;
    } else if (this.bottomL > max) {
      this.bottomLIn = true;
    } else {
      this.bottomLIn = false;
    }
    // console.log(this.aroundMax);
  }
  getReview() {
     if(this.measurementForm.invalid || this.typeData === undefined || 
      this.backNeckData === undefined || this.frontNeckData === undefined || 
      this.sleeveNeckData === undefined){
      this.ale = true;
    } else { 
    this.temp = new LehengaMeasurement();
    this.temp.aroundAboveWaist = this.measurementForm.controls.aroundAboveWaist.value;
    this.temp.aroundBust = this.measurementForm.controls.aroundBust.value;
    this.temp.choliLength = this.measurementForm.controls.choliLength.value;
    this.temp.aroundHip = this.measurementForm.controls.aroundHip.value;
    this.temp.aroundWaist = this.measurementForm.controls.aroundWaist.value;
    this.temp.backNeckStyle = this.backNeckData;
    this.temp.lehengaLength = this.measurementForm.controls.lehengaLength.value;
    this.temp.choliClosingSide = this.measurementForm.controls.choliClosingSide.value;
    this.temp.choliClosingWith = this.measurementForm.controls.choliClosingWith.value;
    this.temp.lining = this.measurementForm.controls.lining.value;
    this.temp.frontNeckStyle = this.frontNeckData;
    this.temp.sleeveStyle = this.sleeveNeckData;
    this.temp.lehengaClosingSide = this.measurementForm.controls.lehengaClosingSide.value;
    this.temp.lehengaClosingWith = this.measurementForm.controls.lehengaClosingWith.value;
    this.temp.specialInstruction = this.measurementForm.controls.specialInstruction.value;
    this.temp.typeName = this.typeData;
    console.log(this.temp);
    this.showReview = true;
     } 
  }
  onSave(name) {
    this.temp.measurementName = name;
    this.temp.userId = this.userId;
   /*  this.temp.productId = this.prodId; */
    this.temp.price = this.measurementData.price;
    this.temp.discount = this.measurementData.discount;
    this.temp.typeName = this.typeData.typeName;
    this.temp.typeDescription = this.typeData.typeDescription;
    this.temp.serviceId = this.measurementData._id;
    this.temp.frontNeckStyle = this.frontNeckData.title;
    this.temp.backNeckStyle = this.backNeckData.title;
    this.temp.sleeveStyle = this.sleeveNeckData.title;
    this.temp.measurementId = this.measurementData._id;
    /* this.temp.bottomStyle = this.bottomStyleData.title; */
    console.log(this.temp);
    this.productService.addMeasuremenByUser(this.temp).subscribe(data => {
      console.log(data);
      this.holder = {
        productId: this.prodId,
        measurementId: data._id,
        userId: this.userId
      };
      sessionStorage.setItem('measurment', JSON.stringify(this.holder));
      this.router.navigate(['product/viewsingle/', this.catId, this.prodId]);
    });
    this.showReview = false;
  }
  getReviewBySaved() {
    this.measurementData.measurementType.forEach(a => {
      if (a.typeName === this.measurementForm.value.typeName) {
        this.typeData = a;
      }
    });
    this.measurementData.frontNeckStyle.forEach(a => {
      if (a.title === this.measurementForm.value.frontNeckStyle) {
        this.frontNeckData = a;
      }
    });
    this.measurementData.backNeckStyle.forEach(a => {
      if (a.title === this.measurementForm.value.backNeckStyle) {
        this.backNeckData = a;
      }
    });
    this.measurementData.sleeveStyle.forEach(a => {
      if (a.title === this.measurementForm.value.sleeveStyle) {
        this.sleeveNeckData = a;
      }
    });
    console.log(this.measurementForm.value);
    this.temp = new LehengaMeasurement();
    this.temp.aroundAboveWaist = this.measurementForm.controls.aroundAboveWaist.value;
    this.temp.aroundBust = this.measurementForm.controls.aroundBust.value;
    this.temp.choliLength = this.measurementForm.controls.choliLength.value;
    this.temp.aroundHip = this.measurementForm.controls.aroundHip.value;
    this.temp.aroundWaist = this.measurementForm.controls.aroundWaist.value;
    this.temp.backNeckStyle = this.backNeckData;
    this.temp.lehengaLength = this.measurementForm.controls.lehengaLength.value;
    this.temp.choliClosingSide = this.measurementForm.controls.choliClosingSide.value;
    this.temp.choliClosingWith = this.measurementForm.controls.choliClosingWith.value;
    this.temp.lining = this.measurementForm.controls.lining.value;
    this.temp.frontNeckStyle = this.frontNeckData;
    this.temp.sleeveStyle = this.sleeveNeckData;
    this.temp.lehengaClosingSide = this.measurementForm.controls.lehengaClosingSide.value;
    this.temp.lehengaClosingWith = this.measurementForm.controls.lehengaClosingWith.value;
    this.temp.specialInstruction = this.measurementForm.controls.specialInstruction.value;
    this.temp.typeName = this.typeData;
    console.log(this.temp);
    this.showReview = true;
  }
}
