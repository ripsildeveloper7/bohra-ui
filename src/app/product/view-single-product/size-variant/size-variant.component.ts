import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Child } from '../../../shared/model/child.model';
import { PopUpSizeGuideService } from '../pop-up-size-guide/pop-up-size-guide.service';
import { HowToMeasureService } from '../how-to-measure/how-to-measure.service';

@Component({
  selector: 'app-size-variant',
  templateUrl: './size-variant.component.html',
  styleUrls: ['./size-variant.component.css']
})
export class SizeVariantComponent implements OnInit, AfterViewInit {
  @Input() productModel: any;
  @Input() selectedSize: any;
  @Input() sizeData: any;
  @Input() isSizeGuide: boolean;
  @Input() howToMeasureData: any;
  @Output() selectedVariant = new EventEmitter<any>();
  @Input() isBodyHeight: boolean;
  @Input() availableBodyHeight: any;
  @Output() bodyHeight = new EventEmitter<any>();
  selectedItem: Child;
  selected: any;
  selectedHeight: any;
  @ViewChild("foc",{static:true}) focus: ElementRef;
  constructor(private popService: PopUpSizeGuideService, private howToMeasureService: HowToMeasureService ) { }

  ngOnInit() {
  }

  ngAfterViewInit(){

  }

  protected ngOnChanges(){
    if(this.selectedSize == true){
      this.focus.nativeElement.focus();
      this.focus.nativeElement.scrollIntoView({behavior:"smooth",block: "center"});
    }
  }
  sizeSelect(itemselect) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
   
  }
  getSizeChart(sizeChart) {
    this.popService.openPopUp(sizeChart);
  }

  getHow(howChart){
    this.howToMeasureService.openPopUp(howChart);
    console.log(howChart);
  }
  heightSelect(height) {
    this.bodyHeight.emit(height);
  }
}
