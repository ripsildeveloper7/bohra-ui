
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Child } from '../../../shared/model/child.model';
import { Color } from '../../../shared/model/colorSetting.model';

@Component({
  selector: 'app-size-color-variant',
  templateUrl: './size-color-variant.component.html',
  styleUrls: ['./size-color-variant.component.css']
})
export class SizeColorVariantComponent implements OnInit {
  @Input() productModel: any;
  @Input() selectedSize: any;
  @Input() color: any;
  @Input() selectedColorMain: any;
  @Output() selectedVariantSizeColor = new EventEmitter<any>();
  /* @Output() selectedImage = new EventEmitter<any>(); */
  selectedItem: Child;
  constructor() { }

  ngOnInit() {
  }
  sizeSelect(itemselect) {
    this.selectedItem = itemselect;
    this.selectedVariantSizeColor.emit(this.selectedItem);
  }
  selectedColor(colorId, product) {
    product.selectedColorMain = colorId;
    product.child.forEach(element => {
      if (element.colorId === colorId) {
        element.headChild = true;
      } else {
        element.headChild = false;
      }
    });
  /*   product.productImage = product.child.find(el => el.headChild === true);
    this.selectedImage.emit(product.productImage);
    this.selectedItem = null; 
  }
  */
}
}