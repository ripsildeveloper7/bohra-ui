import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AppSetting } from '../../../config/appSetting';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MeasurementModel } from './measurement.model';
@Component({
  selector: 'app-measurement-page',
  templateUrl: './measurement-page.component.html',
  styleUrls: ['./measurement-page.component.css']
})
export class MeasurementPageComponent implements OnInit {
  measurementData: any;
  measurementForm: FormGroup;
  prodId: string;
  productModel: any;
  measurmentUrl;
  showReview = false;
  temp;
  userId: string;
  holder: {
    productId: string,
    measurementId: string,
    userId: string
  };
  catId: any;

  show = false;
  show1 = false;
  show2 = false;
  show3 = false;
  show4 = false;
  show5 = false;
  show6 = false;
  show7 = false;
  ratioCheckEvent: any;
  ratioCheckEvent1: any;
  ratioCheckEvent2: any;
  val: any;
  aroundMax: any;
  frontNeckData: any;
  backNeckData: any;
  sleeveNeckData: any;
  ratioCheckType: any;
  aroundMaxIn = false;
  aroundAbWaist: any;
  aroundWaistIn: boolean;
  aroundMaxV: any;
  blouseLeng: any;
  blouseIn: boolean;
  frontNeckDep: any;
  fnIn: boolean;
  bnIn: boolean;
  backNeckDep: any;
  sleeveIn: boolean;
  aroundAr: any;
  sleevLen: any;
  armIn: boolean;
  typeData: any;
  container: any;
  measurementModel: any;
  isSaved = false;
  ale: boolean;
  productImage: {
    imageName: string,
    title: string
  };
  constructor(private productService: ProductService, private router: Router,
              private activatedRoute: ActivatedRoute, private fb: FormBuilder) {
    this.activatedRoute.paramMap.subscribe((param: ParamMap) => {
      this.prodId = param.get('id');
    });
    this.measurmentUrl = AppSetting.measurementImageUrl;
    this.userId = sessionStorage.getItem('userId');
   }

  ngOnInit() {
    this.createForm();
    this.getSingleProduct();
  }
  getMeasurmentByUser() {
    this.productService.getMesurementByUser(this.userId, this.measurementData._id).subscribe(data => {
      this.container = data;
      console.log('check', this.container);
    }, error => {
      console.log(error);
    });
  }
  measurmentSelected(e) {
    console.log(e.value);
    if (e.value === 'createNew') {
      this.isSaved = false;
      this.measurementForm.reset();
    } else {
      this.isSaved = true;
      this.measurementModel = e.value;
    }
  }

  createForm() {
    this.measurementForm = this.fb.group({
      typeName: ['' ,Validators.required],
      aroundBust: [''],
      aroundBustInput: ['', Validators.max(40)],
      aroundAboveWaist: [''],
      blouseLength: [''],
      frontNeckStyle: [''],
      frontNeckDepth: [''],
      backNeckStyle: [''],
      backNeckDepth: [''],
      sleeveStyle: [''],
      sleeveLenth: [''],
      aroundArm: [''],
      closingSide: ['', Validators.required],
      closingWith: ['', Validators.required],
      lining: ['', Validators.required],
      specialInstruction: [''],
    });
  }

  view() {
    this.show = !this.show;
  }
  view1() {
    this.show1 = !this.show1;
  }
  view2() {
    this.show2 = !this.show2;
  }
  view3() {
    this.show3 = !this.show3;
  }
  view4() {
    this.show4 = !this.show4;
  }
  view5() {
    this.show5 = !this.show5;
  }
  view6() {
    this.show6 = !this.show6;
  }
  view7() {
    this.show7 = !this.show7;
  }
  getSingleProduct() {
    this.productService.getSingleProducts(this.prodId).subscribe(data => {
      this.productModel = data;
      console.log(this.productModel);
      this.productModel.child.forEach(child => {
        if (child.headChild === true) {
          this.productImage = {
            imageName: child.productImage[0].productImageName,
            title: 'As Shown'
          };
        }
      });
      if (this.productModel.subCategoryId) {

      } else if (this.productModel.mainCategoryId) {

      } else {
        this.catId = this.productModel.superCategoryId;
      }
      this.getAllMeasurement();
    }, error => {
      console.log(error);
    });
  }
  getAllMeasurement() {
    this.productService.getAllMeasurement().subscribe(data => {
      this.measurementData = data[0];
      this.measurementData.frontNeckStyle = this.setImage(this.measurementData._id, this.measurementData.frontNeckStyle);
      this.measurementData.backNeckStyle = this.setImage(this.measurementData._id, this.measurementData.backNeckStyle);
      this.measurementData.sleeveStyle = this.setImage(this.measurementData._id, this.measurementData.sleeveStyle);
      const front =  [this.productImage].concat(this.measurementData.frontNeckStyle);
      const back = [this.productImage].concat(this.measurementData.backNeckStyle);
      const sleeve = [this.productImage].concat(this.measurementData.sleeveStyle);
      this.measurementData.frontNeckStyle = front;
      this.measurementData.backNeckStyle = back;
      this.measurementData.sleeveStyle = sleeve;
   /*    this.measurementData.measurementType[0].checked = true; */
      console.log('measurement', this.measurementData);
      this.getMeasurmentByUser();
    }, error => {
      console.log(error);
    });
  }
  setImage(id, array) {
    for (const img of array) {
      img.imageName = this.measurmentUrl + id + '/' + img.imageName;
    }
    return array;
  }
  getReview() {
    if(this.measurementForm.invalid || this.typeData === undefined || this.backNeckData === undefined || this.frontNeckData === undefined || this.sleeveNeckData === undefined){
      this.ale = true;
    } else {
    this.temp = new MeasurementModel();
    this.temp.aroundAboveWaist = this.measurementForm.controls.aroundAboveWaist.value;
    this.temp.aroundBust = this.measurementForm.controls.aroundBust.value;
    this.temp.aroundArm = this.measurementForm.controls.aroundArm.value;
    this.temp.backNeckDepth = this.measurementForm.controls.backNeckDepth.value;
    this.temp.backNeckStyle = this.backNeckData;
    this.temp.blouseLength = this.measurementForm.controls.blouseLength.value;
    this.temp.closingSide = this.measurementForm.controls.closingSide.value;
    this.temp.closingWith = this.measurementForm.controls.closingWith.value;
    this.temp.frontNeckDepth = this.measurementForm.controls.frontNeckDepth.value;
    this.temp.frontNeckStyle = this.frontNeckData;
    this.temp.lining = this.measurementForm.controls.lining.value;
    this.temp.sleeveLength = this.measurementForm.controls.sleeveLenth.value;
    this.temp.sleeveStyle = this.sleeveNeckData;
    this.temp.specialInstruction = this.measurementForm.controls.specialInstruction.value;
    this.temp.typeName = this.typeData;
    console.log(this.temp);
    this.showReview = true;
    }
  }
  onSave(name) {
    this.temp.measurementName = name;
    this.temp.userId = this.userId;
    this.temp.productId = this.prodId;
    this.temp.price = this.measurementData.price;
    this.temp.discount = this.measurementData.discount;
    this.temp.typeName = this.typeData.typeName;
    this.temp.typeDescription = this.typeData.typeDescription;
    this.temp.serviceId = this.measurementData._id;
    this.temp.frontNeckStyle = this.frontNeckData.title;
    this.temp.backNeckStyle = this.backNeckData.title;
    this.temp.sleeveStyle = this.sleeveNeckData.title;
    this.temp.measurementId = this.measurementData._id;
    console.log(this.temp);
    this.productService.addMeasuremenByUser(this.temp).subscribe(data => {
      console.log(data);
      this.holder = {
        productId: this.prodId,
        measurementId: data._id,
        userId: this.userId
      };
      sessionStorage.setItem('measurment', JSON.stringify(this.holder));
      this.router.navigate(['product/viewsingle/', this.catId, this.prodId]);
    });
    this.showReview = false;
  }
  goBack() {
    this.router.navigate(['home/welcome']);
  }
  changeEvent(event, full) {
    console.log(full);
    this.measurementForm.controls.frontNeckStyle.reset();
    this.frontNeckData = full;
    this.ratioCheckEvent = event.value;
  }

  changeEvent1(event, full) {
    this.measurementForm.controls.backNeckStyle.reset();
    this.backNeckData = full;
    this.ratioCheckEvent1 = event.value;
  }

  changeEvent2(event, full) {
    this.measurementForm.controls.sleeveStyle.reset();
    this.sleeveNeckData = full;
    this.ratioCheckEvent2 = event.value;
  }
  changeEventType(event, full, array) {
  /*   this.measurementForm.controls.typeName.reset(); */
    array.forEach(e => e.checked = false);
    this.typeData = full;
    /* this.ratioCheckType = event.value; */
    event.value.checked = true;
  }
  inputV(event, max, min) {
    this.aroundMax = event.target.value;
    console.log('amax', this.aroundMaxIn);
    if ( this.aroundMax < min && this.aroundMax > 0 ) {
      this.aroundMaxIn = true;
    } else if (this.aroundMax > max) {
      this.aroundMaxIn = true;
      // this.aroundMaxV = event.target.value;
    } else{
      this.aroundMaxIn = false;
    }
    console.log(this.aroundMax);
  }
  inputW(event, max, min) {
    this.aroundAbWaist = event.target.value;
    if ( this.aroundAbWaist < min  && this.aroundAbWaist > 0) {
      this.aroundWaistIn = true;
    } else if (this.aroundAbWaist > max) {
      this.aroundWaistIn = true;
    } else {
      this.aroundWaistIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputB(event, max, min) {
    this.blouseLeng = event.target.value;
    if ( this.blouseLeng < min  && this.blouseLeng > 0) {
      this.blouseIn = true;
    } else if (this.blouseLeng > max) {
      this.blouseIn = true;
    } else {
      this.blouseIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputFN(event, max, min) {
    this.frontNeckDep = event.target.value;
    console.log(this.frontNeckDep)
    if ( this.frontNeckDep < min  && this.frontNeckDep > 0) {
      this.fnIn = true;
    } else if (this.frontNeckDep > max) {
      this.fnIn = true;
    } else {
      this.fnIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputBN(event, max, min) {
    this.backNeckDep = event.target.value;
    if ( this.backNeckDep < min  && this.backNeckDep > 0) {
      this.bnIn = true;
    } else if (this.backNeckDep > max) {
      this.bnIn = true;
    } else {
      this.bnIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputArm(event, max, min) {
    this.aroundAr = event.target.value;
    if ( this.aroundAr < min && this.aroundAr > 0) {
      this.armIn = true;
    } else if (this.aroundAr > max) {
      this.armIn = true;
    } else {
      this.armIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputSleeve(event, max, min) {
    this.sleevLen = event.target.value;
    if ( this.sleevLen < min && this.sleevLen > 0) {
      this.sleeveIn = true;
    } else if (this.sleevLen > max) {
      this.sleeveIn = true;
    } else {
      this.sleeveIn = false;
    }
    // console.log(this.aroundMax);
  }
  getReviewBySaved() {
    this.measurementData.measurementType.forEach(a => {
      if (a.typeName === this.measurementForm.value.typeName) {
        this.typeData = a;
      }
    });
    this.measurementData.frontNeckStyle.forEach(a => {
      if (a.title === this.measurementForm.value.frontNeckStyle) {
        this.frontNeckData = a;
      }
    });
    this.measurementData.backNeckStyle.forEach(a => {
      if (a.title === this.measurementForm.value.backNeckStyle) {
        this.backNeckData = a;
      }
    });
    this.measurementData.sleeveStyle.forEach(a => {
      if (a.title === this.measurementForm.value.sleeveStyle) {
        this.sleeveNeckData = a;
      }
    });
    console.log(this.measurementForm.value);
    this.temp = new MeasurementModel();
    this.temp.aroundAboveWaist = this.measurementForm.value.aroundAboveWaist;
    this.temp.aroundBust = this.measurementForm.value.aroundBust;
    this.temp.aroundArm = this.measurementForm.value.aroundArm;
    this.temp.backNeckDepth = this.measurementForm.value.backNeckDepth;
    this.temp.backNeckStyle = this.backNeckData;
    this.temp.blouseLength = this.measurementForm.value.blouseLength;
    this.temp.closingSide = this.measurementForm.value.closingSide;
    this.temp.closingWith = this.measurementForm.value.closingWith;
    this.temp.frontNeckDepth = this.measurementForm.value.frontNeckDepth;
    this.temp.frontNeckStyle = this.frontNeckData;
    this.temp.lining = this.measurementForm.value.lining;
    this.temp.sleeveLength = this.measurementForm.value.sleeveLenth;
    this.temp.sleeveStyle = this.sleeveNeckData;
    this.temp.specialInstruction = this.measurementForm.value.specialInstruction;
    this.temp.typeName = this.typeData;
    console.log(this.temp);
    this.showReview = true;
  }
  onChange() {
    this.showReview = false;
  }
}
