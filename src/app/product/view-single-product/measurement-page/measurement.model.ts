export class MeasurementModel {
    typeName: string;
    aroundBust: number;
    aroundAboveWaist: number;
    blouseLength: number;
    frontNeckStyle: [{ imageName: string, title: string}];
    frontNeckDepth: number;
    backNeckStyle: [{ imageName: string, title: string}];
    backNeckDepth: number;
    sleeveStyle: [{ imageName: string, title: string}];
    sleeveLength: number;
    aroundArm: number;
    closingSide: string;
    closingWith: string;
    lining: string;
    specialInstruction: string;
    measurementName: string;
    userId: string;
    serviceId: string;
    typeDescription: string;
    price: number;
    discount: number;
    productId: string;
    measurementId: string;
}
