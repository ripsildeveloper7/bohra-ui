import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-you-may-also-like-page',
  templateUrl: './you-may-also-like-page.component.html',
  styleUrls: ['./you-may-also-like-page.component.css']
})
export class YouMayAlsoLikePageComponent implements OnInit {

  @Input() alsoModel: any;
  @Input() productImageUrl: any;
  showMobileView = false;
  constructor(private router:Router) {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
   }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  getView(id, supId, subId) {
    if(subId){
      this.router.navigate(['product/viewsingle/', subId, id ]);
    } else {
      this.router.navigate(['product/viewsingle/', supId, id ]);
    }
  }
}
