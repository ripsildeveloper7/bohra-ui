import { Component, OnInit } from '@angular/core';
import { KameezMeasurement } from '../../../shared/model/kameezMeasurement.model';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ProductService } from '../../product.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppSetting } from '../../../config/appSetting';

@Component({
  selector: 'app-kameez-measurement-page',
  templateUrl: './kameez-measurement-page.component.html',
  styleUrls: ['./kameez-measurement-page.component.css']
})
export class KameezMeasurementPageComponent implements OnInit {
  prodId: string;
  measurementModel: any;
  measurementForm: FormGroup;
  showReview = false;
  measurementData: any;
  measurmentUrl: string;
  holder: {
    productId: string,
    measurementId: string,
    userId: string
  };
  show = false;
  show1 = false;
  show2 = false;
  show3 = false;
  show4 = false;
  show5 = false;
  show6 = false;
  show7 = false;
  temp: any;
  frontNeckData: any;
  ratioCheckEvent: any;
  backNeckData: any;
  ratioCheckEvent1: any;
  sleeveNeckData: any;
  ratioCheckEvent2: any;
  typeData: any;
  ratioCheckType: any;
  bottomStyleData: any;
  ratioCheckEvent3: any;
  show8 = false;
  show9 = false;
  show10 = false;
  show11 = false;
  show12 = false;
  show13 = false;
  show14 = false;
  userId: any;
  productModel: any;
  catId: any;
  sleeveIn: boolean;
  sleevLen: any;
  armIn: boolean;
  aroundAr: any;
  bnIn: boolean;
  backNeckDep: any;
  fnIn: boolean;
  frontNeckDep: any;
  blouseIn: boolean;
  blouseLeng: any;
  aroundWaistIn: boolean;
  aroundAbWaist: any;
  aroundMaxIn: boolean;
  kameezLeng: any;
  kameezIn: boolean;
  aroundB: any;
  aroundBIn: boolean;
  aroundW: any;
  aroundWIn: boolean;
  aroundT: any;
  aroundTIn: boolean;
  aroundK: any;
  aroundKIn: boolean;
  aroundCIn: boolean;
  aroundC: number;
  aroundBot: any;
  aroundBotIn: boolean;
  bottomL: any;
  bottomLIn: boolean;
  aroundHipLeng: any;
  ale: boolean;
  productImage: {
    imageName: string,
    title: string
  };
  container: any;
  isSaved = false;

  constructor(private productService: ProductService, private router: Router,
              private activatedRoute: ActivatedRoute, private fb: FormBuilder) {
                this.activatedRoute.paramMap.subscribe((param: ParamMap) => {
                  this.prodId = param.get('id');
                });
                this.userId = sessionStorage.getItem('userId');
                this.measurmentUrl = AppSetting.measurementImageUrl;
               
                this.getSingleProduct();
              }

  ngOnInit() {
    this.createForm();
  }
  getKameezMeasurement() {
    this.productService.getAllKameezMeasurement().subscribe(data => {
      this.measurementData = data[0];
      console.log(this.measurementData);
      this.measurementData.kameezMeasurement[0].frontNeckStyle = this.setImage(this.measurementData._id, this.measurementData.kameezMeasurement[0].frontNeckStyle);
      this.measurementData.kameezMeasurement[0].backNeckStyle = this.setImage(this.measurementData._id, this.measurementData.kameezMeasurement[0].backNeckStyle);
      this.measurementData.kameezMeasurement[0].sleeveStyle = this.setImage(this.measurementData._id, this.measurementData.kameezMeasurement[0].sleeveStyle);
      this.measurementData.bottomMeasurement[0].bottomStyle = this.setImage(this.measurementData._id, this.measurementData.bottomMeasurement[0].bottomStyle);
      const front =  [this.productImage].concat(this.measurementData.kameezMeasurement[0].frontNeckStyle);
      const back = [this.productImage].concat(this.measurementData.kameezMeasurement[0].backNeckStyle);
      const sleeve = [this.productImage].concat(this.measurementData.kameezMeasurement[0].sleeveStyle);
      const bottom = [this.productImage].concat(this.measurementData.bottomMeasurement[0].bottomStyle);
      this.measurementData.kameezMeasurement[0].frontNeckStyle = front;
      this.measurementData.kameezMeasurement[0].backNeckStyle = back;
      this.measurementData.kameezMeasurement[0].sleeveStyle = sleeve;
      this.measurementData.bottomMeasurement[0].bottomStyle = bottom;
      this.getMeasurmentByUser();
    }, error => {
      console.log(error);
    });
  }
  getMeasurmentByUser() {
    this.productService.getMesurementByUser(this.userId, this.measurementData._id).subscribe(data => {
      this.container = data;
      console.log('check', this.container);
    }, error => {
      console.log(error);
    });
  }
  measurmentSelected(e) {
    console.log(e.value);
    if (e.value === 'createNew') {
      this.isSaved = false;
      this.measurementForm.reset();
    } else {
      this.isSaved = true;
      this.measurementModel = e.value;
    }
  }
  getSingleProduct() {
    this.productService.getSingleProducts(this.prodId).subscribe(data => {
      this.productModel = data;
      this.productModel.child.forEach(child => {
        if (child.headChild === true) {
          this.productImage = {
            imageName: child.productImage[0].productImageName,
            title: 'As Shown'
          };
        }
      });
      if (this.productModel.subCategoryId) {

      } else if (this.productModel.mainCategoryId) {

      } else {
        this.catId = this.productModel.superCategoryId;
      }
      this.getKameezMeasurement();
    }, error => {
      console.log(error);
    });
  }
  setImage(id, array) {
    for (const img of array) {
      img.imageName = this.measurmentUrl + id + '/' + img.imageName;
    }
    return array;
  }
  createForm() {
    this.measurementForm = this.fb.group({
      typeName: [''],
      aroundBust: [''],
      aroundBustInput: [''],
      aroundAboveWaist: [''],
      aroundHip: [''],
      /* blouseLength: [''], */
      frontNeckStyle: [''],
      frontNeckDepth: [''],
      backNeckStyle: [''],
      backNeckDepth: [''],
      sleeveStyle: [''],
      sleeveLength: [''],
      aroundArm: [''],
      kameezLength: [''],
      kameezClosingSide: ['', Validators.required],
      kameezClosingWith: ['', Validators.required],
      bottomStyle: [''],
      fitOption: ['', Validators.required],
      aroundWaist: [''],
      aroundThigh: [''],
      aroundKnee: [''],
      aroundCalf: [''],
      aroundBottom: [''],
      bottomLength: [''],
      waistClosingSide: ['', Validators.required],
      waistClosingWith: ['', Validators.required],
      specialInstruction: [''],
    });
  }
  view() {
    this.show = !this.show;
  }
  view1() {
    this.show1 = !this.show1;
  }
  view2() {
    this.show2 = !this.show2;
  }
  view3() {
    this.show3 = !this.show3;
  }
  view4() {
    this.show4 = !this.show4;
  }
  view5() {
    this.show5 = !this.show5;
  }
  view6() {
    this.show6 = !this.show6;
  }
  view7() {
    this.show7 = !this.show7;
  }
  view8() {
    this.show8 = !this.show8;
  }
  view9() {
    this.show9 = !this.show9;
  }
  view10() {
    this.show10 = !this.show10;
  }
  view11() {
    this.show11 = !this.show11;
  }
  view12() {
    this.show12 = !this.show12;
  }
  view13() {
    this.show13 = !this.show13;
  }
  view14() {
    this.show14 = !this.show14;
  }
  changeEvent(event, full) {
    console.log(full);
    this.measurementForm.controls.frontNeckStyle.reset();
    this.frontNeckData = full;
    this.ratioCheckEvent = event.value;
  }

  changeEvent1(event, full) {
    this.measurementForm.controls.backNeckStyle.reset();
    this.backNeckData = full;
    this.ratioCheckEvent1 = event.value;
  }

  changeEvent2(event, full) {
    this.measurementForm.controls.sleeveStyle.reset();
    this.sleeveNeckData = full;
    this.ratioCheckEvent2 = event.value;
  }
  changeEvent3(event, full) {
    this.measurementForm.controls.bottomStyle.reset();
    this.bottomStyleData = full;
    this.ratioCheckEvent3 = event.value;
  }
  changeEventType(event, full, array) {
    /*   this.measurementForm.controls.typeName.reset(); */
      array.forEach(e => e.checked = false);
      this.typeData = full;
      /* this.ratioCheckType = event.value; */
      event.value.checked = true;
    }
  getReview() {
    if(this.measurementForm.invalid || this.typeData === undefined || 
      this.backNeckData === undefined || this.frontNeckData === undefined || 
      this.sleeveNeckData === undefined || this.bottomStyleData === undefined){
      this.ale = true;
    } else {
    this.temp = new KameezMeasurement();
    this.temp.aroundAboveWaist = this.measurementForm.controls.aroundAboveWaist.value;
    this.temp.aroundBust = this.measurementForm.controls.aroundBust.value;
    this.temp.aroundArm = this.measurementForm.controls.aroundArm.value;
    this.temp.aroundHip = this.measurementForm.controls.aroundHip.value;
    this.temp.backNeckDepth = this.measurementForm.controls.backNeckDepth.value;
    this.temp.backNeckStyle = this.backNeckData;
    this.temp.kameezLength = this.measurementForm.controls.kameezLength.value;
    this.temp.kameezClosingSide = this.measurementForm.controls.kameezClosingSide.value;
    this.temp.kameezClosingWith = this.measurementForm.controls.kameezClosingWith.value;
    this.temp.frontNeckDepth = this.measurementForm.controls.frontNeckDepth.value;
    this.temp.frontNeckStyle = this.frontNeckData;
    this.temp.bottomStyle = this.bottomStyleData;
    /* this.temp.lining = this.measurementForm.controls.lining.value; */
    this.temp.sleeveLength = this.measurementForm.controls.sleeveLength.value;
    this.temp.sleeveStyle = this.sleeveNeckData;
    this.temp.fitOption = this.measurementForm.controls.fitOption.value;
    this.temp.aroundWaist = this.measurementForm.controls.aroundWaist.value;
    this.temp.aroundThigh = this.measurementForm.controls.aroundThigh.value;
    this.temp.aroundKnee = this.measurementForm.controls.aroundKnee.value;
    this.temp.aroundCalf = this.measurementForm.controls.aroundCalf.value;
    this.temp.aroundBottom = this.measurementForm.controls.aroundBottom.value;
    this.temp.bottomLength = this.measurementForm.controls.bottomLength.value;
    this.temp.waistClosingSide = this.measurementForm.controls.waistClosingSide.value;
    this.temp.waistClosingWith = this.measurementForm.controls.waistClosingWith.value;
    this.temp.specialInstruction = this.measurementForm.controls.specialInstruction.value;
    this.temp.typeName = this.typeData;
    console.log(this.temp);
    this.showReview = true;
    }
  }
  onSave(name) {
    this.temp.measurementName = name;
    this.temp.userId = this.userId;
    this.temp.productId = this.prodId;
    this.temp.price = this.measurementData.price;
    this.temp.discount = this.measurementData.discount;
    this.temp.typeName = this.typeData.typeName;
    this.temp.typeDescription = this.typeData.typeDescription;
    this.temp.serviceId = this.measurementData._id;
    this.temp.frontNeckStyle = this.frontNeckData.title;
    this.temp.backNeckStyle = this.backNeckData.title;
    this.temp.sleeveStyle = this.sleeveNeckData.title;
    this.temp.bottomStyle = this.bottomStyleData.title;
    this.temp.measurementId = this.measurementData._id;
    console.log(this.temp);
    this.productService.addMeasuremenByUser(this.temp).subscribe(data => {
      console.log(data);
      this.holder = {
        productId: this.prodId,
        measurementId: data._id,
        userId: this.userId
      };
      sessionStorage.setItem('measurment', JSON.stringify(this.holder));
      this.router.navigate(['product/viewsingle/', this.catId, this.prodId]);
    });
    this.showReview = false;
  }


  // inputV(event, max, min) {
  //   this.aroundMax = event.target.value;
  //   console.log('amax', this.aroundMaxIn);
  //   if ( this.aroundMax < min && this.aroundMax > 0 ) {
  //     this.aroundMaxIn = true;
  //   } else if (this.aroundMax > max) {
  //     this.aroundMaxIn = true;
  //     // this.aroundMaxV = event.target.value;
  //   } else{
  //     this.aroundMaxIn = false;
  //   }
  //   console.log(this.aroundMax);
  // }
  // aroundMax(aroundMax: any) {
  //   throw new Error("Method not implemented.");
  // }
  inputW(event, max, min) {
    this.aroundAbWaist = event.target.value;
    if ( this.aroundAbWaist < min  && this.aroundAbWaist > 0) {
      this.aroundWaistIn = true;
    } else if (this.aroundAbWaist > max) {
      this.aroundWaistIn = true;
    } else {
      this.aroundWaistIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputB(event, max, min) {
    this.aroundHipLeng = event.target.value;
    if ( this.aroundHipLeng < min  && this.aroundHipLeng > 0) {
      this.blouseIn = true;
    } else if (this.aroundHipLeng > max) {
      this.blouseIn = true;
    } else {
      this.blouseIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputK(event, max, min) {
    this.kameezLeng = event.target.value;
    if ( this.kameezLeng < min  && this.kameezLeng > 0) {
      this.kameezIn = true;
    } else if (this.kameezLeng > max) {
      this.kameezIn = true;
    } else {
      this.kameezIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputFN(event, max, min) {
    this.frontNeckDep = event.target.value;
    if ( this.frontNeckDep < min  && this.frontNeckDep > 0) {
      this.fnIn = true;
    } else if (this.frontNeckDep > max) {
      this.fnIn = true;
    } else {
      this.fnIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputBN(event, max, min) {
    this.backNeckDep = event.target.value;
    if ( this.backNeckDep < min  && this.backNeckDep > 0) {
      this.bnIn = true;
    } else if (this.backNeckDep > max) {
      this.bnIn = true;
    } else {
      this.bnIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputBF(event, max, min) {
    this.aroundB = event.target.value;
    if ( this.aroundB < min  && this.aroundB > 0) {
      this.aroundBIn = true;
    } else if (this.aroundB > max) {
      this.aroundBIn = true;
    } else {
      this.aroundBIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputArm(event, max, min) {
    this.aroundAr = event.target.value;
    if ( this.aroundAr < min && this.aroundAr > 0) {
      this.armIn = true;
    } else if (this.aroundAr > max) {
      this.armIn = true;
    } else {
      this.armIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputWaist(event, max, min) {
    this.aroundW = event.target.value;
    if ( this.aroundW < min && this.aroundW > 0) {
      this.aroundWIn = true;
    } else if (this.aroundW > max) {
      this.aroundWIn = true;
    } else {
      this.aroundWIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputThigh(event, max, min) {
    this.aroundT = event.target.value;
    if ( this.aroundT < min && this.aroundT > 0) {
      this.aroundTIn = true;
    } else if (this.aroundT > max) {
      this.aroundTIn = true;
    } else {
      this.aroundTIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputSleeve(event, max, min) {
    this.sleevLen = event.target.value;
    if ( this.sleevLen < min && this.sleevLen > 0) {
      this.sleeveIn = true;
    } else if (this.sleevLen > max) {
      this.sleeveIn = true;
    } else {
      this.sleeveIn = false;
    }
    // console.log(this.aroundMax);
  }

  inputKnee(event, max, min) {
    this.aroundK = event.target.value;
    if ( this.aroundK < min && this.aroundK > 0) {
      this.aroundKIn = true;
    } else if (this.aroundK > max) {
      this.aroundKIn = true;
    } else {
      this.aroundKIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputCalf(event, max, min) {
    this.aroundC = event.target.value;
    if ( this.aroundC < min && this.aroundC > 0) {
      this.aroundCIn = true;
    } else if (this.aroundC > max) {
      this.aroundCIn = true;
    } else {
      this.aroundCIn = false;
    }
    // console.log(this.aroundMax);
  }

  inputBottom(event, max, min) {
    this.aroundBot = event.target.value;
    if ( this.aroundBot < min && this.aroundBot > 0) {
      this.aroundBotIn = true;
    } else if (this.aroundBot > max) {
      this.aroundBotIn = true;
    } else {
      this.aroundBotIn = false;
    }
    // console.log(this.aroundMax);
  }
  inputABottom(event, max, min) {
    this.bottomL = event.target.value;
    if ( this.bottomL < min && this.bottomL > 0) {
      this.bottomLIn = true;
    } else if (this.bottomL > max) {
      this.bottomLIn = true;
    } else {
      this.bottomLIn = false;
    }
    // console.log(this.aroundMax);
  }
  getReviewBySaved() {
    this.measurementData.measurementType.forEach(a => {
      if (a.typeName === this.measurementForm.value.typeName) {
        this.typeData = a;
      }
    });
    this.measurementData.frontNeckStyle.forEach(a => {
      if (a.title === this.measurementForm.value.frontNeckStyle) {
        this.frontNeckData = a;
      }
    });
    this.measurementData.backNeckStyle.forEach(a => {
      if (a.title === this.measurementForm.value.backNeckStyle) {
        this.backNeckData = a;
      }
    });
    this.measurementData.sleeveStyle.forEach(a => {
      if (a.title === this.measurementForm.value.sleeveStyle) {
        this.sleeveNeckData = a;
      }
    });
    this.measurementData.bottomStyle.forEach(a => {
      if (a.title === this.measurementForm.value.bottomStyle) {
        this.bottomStyleData = a;
      }
    });
    console.log(this.measurementForm.value);
    this.temp = new KameezMeasurement();
    this.temp.aroundAboveWaist = this.measurementForm.controls.aroundAboveWaist.value;
    this.temp.aroundBust = this.measurementForm.controls.aroundBust.value;
    this.temp.aroundArm = this.measurementForm.controls.aroundArm.value;
    this.temp.aroundHip = this.measurementForm.controls.aroundHip.value;
    this.temp.backNeckDepth = this.measurementForm.controls.backNeckDepth.value;
    this.temp.backNeckStyle = this.backNeckData;
    this.temp.kameezLength = this.measurementForm.controls.kameezLength.value;
    this.temp.kameezClosingSide = this.measurementForm.controls.kameezClosingSide.value;
    this.temp.kameezClosingWith = this.measurementForm.controls.kameezClosingWith.value;
    this.temp.frontNeckDepth = this.measurementForm.controls.frontNeckDepth.value;
    this.temp.frontNeckStyle = this.frontNeckData;
    this.temp.bottomStyle = this.bottomStyleData;
    /* this.temp.lining = this.measurementForm.controls.lining.value; */
    this.temp.sleeveLength = this.measurementForm.controls.sleeveLength.value;
    this.temp.sleeveStyle = this.sleeveNeckData;
    this.temp.fitOption = this.measurementForm.controls.fitOption.value;
    this.temp.aroundWaist = this.measurementForm.controls.aroundWaist.value;
    this.temp.aroundThigh = this.measurementForm.controls.aroundThigh.value;
    this.temp.aroundKnee = this.measurementForm.controls.aroundKnee.value;
    this.temp.aroundCalf = this.measurementForm.controls.aroundCalf.value;
    this.temp.aroundBottom = this.measurementForm.controls.aroundBottom.value;
    this.temp.bottomLength = this.measurementForm.controls.bottomLength.value;
    this.temp.waistClosingSide = this.measurementForm.controls.waistClosingSide.value;
    this.temp.waistClosingWith = this.measurementForm.controls.waistClosingWith.value;
    this.temp.specialInstruction = this.measurementForm.controls.specialInstruction.value;
    this.temp.typeName = this.typeData;
    console.log(this.temp);
    this.showReview = true;
  }
}
