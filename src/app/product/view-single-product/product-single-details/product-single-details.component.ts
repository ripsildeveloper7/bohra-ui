import { Component, OnInit, Input, Output, EventEmitter, INJECTOR } from '@angular/core';
import { Product } from './../../../shared/model/product.model';
import { Color } from './../../../shared/model/colorSetting.model';
import { Child } from './../../../shared/model/child.model';
import { SizeGuide } from '../../../shared/model/sizeGuide.model';
import { Attribute } from '../dynamic-attribute/attribute';
import { PopUpSizeGuideService } from '../pop-up-size-guide/pop-up-size-guide.service';
import { MatDialogConfig, MatDialog, MatSnackBar } from '@angular/material';
import { InquiryFormComponent } from '../inquiry-form/inquiry-form.component';
import { Router } from '@angular/router';
import { ProductService } from '../../product.service';
import { SigninDailogComponent } from 'src/app/shared/signin-dailog/signin-dailog.component';
import { HowToMeasureComponent } from '../how-to-measure/how-to-measure.component';
import { HowToMeasureService } from '../how-to-measure/how-to-measure.service';

declare const FB : any;
var twttr = twttr || {};
@Component({
  selector: 'app-product-single-details',
  templateUrl: './product-single-details.component.html',
  styleUrls: ['./product-single-details.component.css']
})
export class ProductSingleDetailsComponent implements OnInit {
  @Input() productModel: Product;
  @Input() color: Color;
  @Input() selectedSize: boolean;
  @Output() selectedVariant = new EventEmitter<any>();
  @Output() addTocart = new EventEmitter<any>();
  @Output() productImageSelected =  new EventEmitter<any>();
  @Input() productSizeGuide: SizeGuide;
  @Input() sizeData: any;
  @Input() selectedColor: string;
  @Input() attribute: Attribute;
  @Input() readyToWearholder: any;
  @Input() measurementData: any;
  @Input() isMeasurementApply: any;
  @Input() isBlouse: boolean;
  @Input() isKameez: boolean;
  @Input() isLehenga: boolean;
  @Output() selectedService = new EventEmitter<any>();
  selectedItem: Child;
  showDescription = false;
  content: {
    selectedSize: string,
    serviceName: string,
    serviceId: string
  };
  initialPrice = 0;
  char = 120;
  textData;
  file: any;
  url : any; 
  isReadyToWear = false;
  typeName: any;
  note:any;
  isMeaurement: boolean;
  message = 'Please Login';
  action: string;
  howToMeasureData: any;
  superCategoryId: any;
  subCategoryId: any;
  constructor(private router: Router, private dialog: MatDialog, private snackBar: MatSnackBar,
              private  popUpSizeGuideService: PopUpSizeGuideService, private productService: ProductService, 
              private howToMeasure: HowToMeasureService) {
    this.url = "www.ucchalfashion.com" + this.router.url;
    console.log(this.url);
    this.productService.getProductSettings().subscribe( data =>{
      this.note = data;
      console.log(this.note,'note');
    })
    //   this.productService.getHowToMeasure().subscribe(data =>{
    //     data.forEach(ele => {
    //       if(ele.superCategoryId == this.productModel.superCategoryId && ele.subCategoryId == this.productModel.subCategoryId){
    //       this.howToMeasureData = ele;
    //       } else if(ele.subCategoryId == this.productModel.superCategoryId){
    //         this.howToMeasureData = ele;
    //         console.log(this.howToMeasureData,"howw")
    //       } 
    //     });
        
    //         console.log(data);
    // })
   }

  ngOnInit() {
    console.log(this.productModel,"abc");
    (window as any).fbAsyncInit = function() {
   
      FB.init({
        appId: '464042414199162',
        cookie: false,  // enable cookies to allow the server to access
        // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.5
    });
  
    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  //   (function(d,s,id){
  //     var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
  //     if(!d.getElementById(id)){
  //         js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';
  //         fjs.parentNode.insertBefore(js,fjs);
  //     }
  // }(document, 'script', 'twitter-wjs'));
    }
  }

  


  fb() {
    console.log(this.router.url);
    FB.ui({
      method: 'feed',
      // href: urlHost+link,
      // method: 'feed',
      title: this.productModel.productName,
      link: "www.ucchalfashion.com/" + this.router.url,
      picture: this.productModel.productImage[0].productImageName,
      // caption: "Interbank",
      description:this.productModel.productName
    });
  }

  pinIt(){
    window.open("//www.pinterest.com/pin/create/button/"+
    "?url="+this.url+
    "&media="+this.productModel.productImage[0].productImageName+
    "&description="+this.productModel.productName,"_blank");
    return false;
  }

  
//   tweet(){
//     return {
//       link: function(scope, element, attr) {
//           setTimeout(function() {
//                   twttr.widgets.createShareButton(
//                       this.url,
//                       element[0],
//                       // function(el) {}, {
//                       //     count: 'none',
//                       //     text: productModel
//                       // }
//                   );
//           });
//       }
//   }
// }
  
  
  sizeSelect(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  /*   this.selectedItem.selectSize = itemselect.sizeName; */
  }
  addToCartItem(proId) {
    /* if(this.selectedColor){
      const prod = this.productModel.child.find(el => el.colorId === this.selectedColor);
      this.selectColorSize(prod);
    } */
    this.addTocart.emit(proId);
  }
  selectColorSize(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  }
  selectColor(itemselect: Child) {
    this.selectedItem = itemselect;
    this.selectedVariant.emit(this.selectedItem);
  }
  /* getDescription() {
    if (this.showDescription === false) {
      console.log(this.productModel);
      const file = this.productModel.productDescription;
      this.textData = file.substr(0, this.char);
      console.log(this.textData);
    } else {
      this.textData = this.productModel.productDescription;
    }
  } */
  /* checkLoginUser(product) {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.WishList.emit(product);
    } else {
      this.router.navigate(['/account/acc/signin']);
    }
  } */
  selectImageModel(event) {
    /* console.log(event); */
    this.productImageSelected.emit(event);
  }
  getPopUp() {
    this.popUpSizeGuideService.openPopUp(this.productSizeGuide);
    
  }
  readMore() {
    this.showDescription = true;
  }
  readLess() {
    this.showDescription = false;
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(InquiryFormComponent, {
      panelClass: 'c1',

    });
  }
  changeApplyOn(name) {
    /* this.typeName = name.value;
    console.log(name.value);
    this.isReadyToWear = true; */
    if (name.value === 'unStitchedFabric') {
      this.isMeaurement = false;
    } else {
      this.isMeaurement = true;
    }
  }
  getSizeChart(sizeChart) {
    this.popUpSizeGuideService.openPopUp(sizeChart);
  }
  selectedSizeValue(element, readyTo) {
    /* console.log(element.value, readyTo); */
    this.content = {
      selectedSize: element.value,
      serviceName: readyTo[0].serviceName,
      serviceId: readyTo[0]._id
    };
    this.selectedService.emit(this.content);
  }
  getClick() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      if (this.isBlouse) {
        this.router.navigate(['product/measurement/', this.productModel._id]);
      } else if (this.isKameez) {
        this.router.navigate(['product/kameezmeasurement/', this.productModel._id]);
      } else if (this.isLehenga) {
        this.router.navigate(['product/lehengameasurement/', this.productModel._id]);
      }
    } else {
      this.openDialogLogin();
      /* this.snackBar.open(this.message, this.action, {
        duration: 3000,
      }); */
    }
  }
  openDialogLogin() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      panelClass: 'c1',
    });
  }

  getHowToMeasure() {
    this.howToMeasure.openPopUp(this.howToMeasureData);
  }
}
