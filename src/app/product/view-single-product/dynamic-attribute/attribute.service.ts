import { Injectable } from '@angular/core';
import { ProductAttributeComponent } from './product-attribute/product-attribute.component';
import { Attribute } from './attribute';
@Injectable({
  providedIn: 'root'
})
export class AttributeService {

  getAds(data) {
    return [
      new Attribute(ProductAttributeComponent, data)
    ];
  }
}
