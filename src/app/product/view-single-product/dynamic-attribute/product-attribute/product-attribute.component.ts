import { Component, Input, OnInit } from '@angular/core';
import { InquiryFormComponent } from '../../inquiry-form/inquiry-form.component';
import { MatDialogConfig, MatDialog } from '@angular/material';


@Component({
  selector: 'app-product-attribute',
  templateUrl: './product-attribute.component.html',
  styleUrls: ['./product-attribute.component.css']
})
export class ProductAttributeComponent implements OnInit {
  @Input() data: any;

  constructor(private dialog: MatDialog) {}
  ngOnInit() {

  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(InquiryFormComponent, {
      panelClass: 'c1',

    });
  }

}
