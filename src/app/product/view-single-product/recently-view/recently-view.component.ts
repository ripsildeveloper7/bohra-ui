import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recently-view',
  templateUrl: './recently-view.component.html',
  styleUrls: ['./recently-view.component.css']
})
export class RecentlyViewComponent implements OnInit {
@Input() recentPrdouctData: any;
showMobileView = false;
  constructor(private router: Router) { }

  ngOnInit() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  getView(id, supId, subId) {
    if(subId){
      this.router.navigate(['product/viewsingle/', subId, id ]);
    } else {
      this.router.navigate(['product/viewsingle/', supId, id ]);
    }
  }
}
