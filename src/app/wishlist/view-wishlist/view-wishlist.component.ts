import { Component, OnInit } from '@angular/core';
import { WishList } from './../../shared/model/wishList.model';
import { WishlistService } from './../wishlist.service';
import { AppSetting } from './../../config/appSetting';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { SelectService } from './select.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-view-wishlist',
  templateUrl: './view-wishlist.component.html',
  styleUrls: ['./view-wishlist.component.css']
})
export class ViewWishlistComponent implements OnInit {
  wishForm: FormGroup;
  wishList: WishList[];
  wish: WishList;
  userId: string;
  discountStore;
  productImageUrl: string = AppSetting.productImageUrl;
  changeSize = 'changeSize';
  showMobileView = false;
  qty: any;
  constructor(private wishlistService: WishlistService, private fb: FormBuilder, private selectService: SelectService) { }

  ngOnInit() {
    
    this.wishForm = this.fb.group({
      seletedSku: ['']
    });
    this.getAllDiscount();
    this.checkLogin();
  }
  ngAfterViewInit() {
    this.getWindowSize();
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  checkLogin() {
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getwishList();
    } else {

    }
  }
  removeWishList(wish) {
    this.wishlistService.deleteWishlist(this.userId, wish.productIds._id).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
      }, err => {
        console.log(err);
      });
  }
  noneVariant(wish, pro) {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wish.productId = wish.productIds.proId;
    this.wish.INTsku = pro.INTsku;
    this.wishlistService.moveToAddtoCart(this.wish).subscribe(data => {
     this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
      }, err => {
        console.log(err);
      });
  }
  moveToBag(wish, pro) {
    switch (pro.variationType) {
      case 'None': {
        const noneVar = pro.child.find(el => el.headChild === true);
        this.noneVariant(wish, noneVar);
        break;
      }
      case 'Color': {
        this.selectService.openColorVariant(wish).subscribe(el => {
          this.wishList =  this.wishList.filter(text => text.productIds.proId !== pro._id);
          const wishlist: any = this.wishList.map(a => a.productIds);
          sessionStorage.setItem('wislistLength', wishlist.length);
          this.getCartCount(this.userId);
        });
        break;
      }
      case 'SizeColor': {
        this.selectService.openSizeColorVariant(wish).subscribe(el => {
          this.wishList =  this.wishList.filter(text => text.productIds.proId !== pro._id);
          const wishlist: any = this.wishList.map(a => a.productIds);
          sessionStorage.setItem('wislistLength', wishlist.length);
          this.getCartCount(this.userId);
        });
        
        /* console.log(this.sizeColorVariantColors); */


        /* this.product.child.map(size => size.sizeVariantId);
        console.log(this.product); */

        /* this.color.ids = this.product.child.map(color => color.colorId);
        this.color.ids = this.product.child.map(color => color.sizeVariantId); */
        /* this.viewAllColors(this.color) */
        break;
      }
      case 'Size': {
        this.selectService.openSizeVariant(wish).subscribe(el => {
          
          this.wishList = this.wishList.filter(text => text.productIds.proId !== pro._id);
          const wishlist: any = this.wishList.map(a => a.productIds);
          sessionStorage.setItem('wislistLength', wishlist.length);
          this.getCartCount(this.userId);
        });
        
        break;
      }
      default: {
        // statements;
        break;
      }
    }
  }
  selectedWishSize(data, wish) {
    this.wishList.forEach(mat => {
      if (mat.productIds.proId === wish.productIds.proId) {
          mat.productIds.INTsku = data;
      }
     });
  }
  getwishList() {
    this.wish = new WishList();
    this.wish.userId = this.userId;
    this.wishlistService.getWishList(this.wish).subscribe(data => {
      this.wishList = data;
      const wishlist: any = this.wishList.map(a => a.productIds);
      sessionStorage.setItem('wislistLength', wishlist.length);
      this.combainDiscount()
      
    }, err => {
      console.log(err);
    });
  }
  getCartCount(userId) {
    this.wishlistService.getCartCountUserId(userId).subscribe(data => {
     this.qty = sessionStorage.setItem('cartqty', data);
    }, err => {
      console.log(err);
    });
  }
  
  getAllDiscount() {
    this.wishlistService.getAllDiscount().subscribe(data => {
      this.discountStore = data;
      /* console.log('discount', this.discountStore); */
    }, error => {
      console.log(error);
    });
  }
  combainDiscount() {
    for (let l = 0; l <= this.wishList.length - 1; l++) {
      this.wishList[l].wishListProduct[0].discountStyle = 'discountNone';
      for (let i = 0; i <= this.discountStore.length - 1; i++) {
        for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
          for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {


            if (this.wishList[l].wishListProduct[0]._id === this.discountStore[i].conditions[j].value[k]) {
              this.wishList[l].wishListProduct[0].discountStyle = 'discountStyle';
              this.wishList[l].wishListProduct[0].oldPrice = this.wishList[l].wishListProduct[0].price;
              if (this.discountStore[i].amountType === 'Flat') {
                this.wishList[l].wishListProduct[0].price = this.wishList[l].wishListProduct[0].oldPrice - this.discountStore[i].typeValue;
                this.wishList[l].wishListProduct[0].discount = 'Flat' + this.discountStore[i].typeValue;
              } else {
                this.wishList[l].wishListProduct[0].price = this.wishList[l].wishListProduct[0].oldPrice - Math.round(this.wishList[l].wishListProduct[0].oldPrice / 100 * this.discountStore[i].typeValue);
                this.wishList[l].wishListProduct[0].discount = this.discountStore[i].typeValue + '%';
              }
            }
          }
        }
      }
    }
    console.log('updated', this.wishList);
  }
}
