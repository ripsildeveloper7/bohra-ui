import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { SelectWishlistComponent } from '../select-wishlist/select-wishlist.component';
import { ColorVariantComponent } from '../color-variant/color-variant.component';
import { SizeVariantComponent } from '../size-variant/size-variant.component';
import { SizeColorComponent } from '../size-color/size-color.component';
import { Product } from '../../shared/model/product.model';
@Injectable({
  providedIn: 'root'
})
export class SelectService {
  dialogRef1: MatDialogRef<SizeVariantComponent>;
  dialogRef2: MatDialogRef<ColorVariantComponent>;
  dialogRef3: MatDialogRef<SizeColorComponent>;
  constructor(private dialog: MatDialog) { }
  openSizeVariant(product?: Product): Observable<boolean> {
        const produData: any = product;
        this.dialogRef1 = this.dialog.open(SizeVariantComponent, {
        disableClose: true, backdropClass: 'light-backdrop',
        width: '720px',
        data: produData 
      });
    return this.dialogRef1.afterClosed();
  }
  openColorVariant(product?: Product): Observable<boolean> {
    this.dialogRef2 = this.dialog.open(ColorVariantComponent, {
        disableClose: true, backdropClass: 'light-backdrop',
        width: '720px',
        data: product
      });
    return this.dialogRef2.afterClosed();
  }
  
  openSizeColorVariant(product?: Product): Observable<boolean> {
    this.dialogRef3 = this.dialog.open(SizeColorComponent, {
        disableClose: true, backdropClass: 'light-backdrop',
        width: '720px',
        data: product
      });
    return this.dialogRef3.afterClosed();
  }
  closeOpenSizeVariant() {
    if (this.dialogRef1) {
      this.dialogRef1.close();
    }
  }
  closeColorVariant() {
    if (this.dialogRef2) {
      this.dialogRef2.close();
    }
  }
  closeSizeColorVariant() {
    if (this.dialogRef3) {
      this.dialogRef3.close();
    }
  }
}
