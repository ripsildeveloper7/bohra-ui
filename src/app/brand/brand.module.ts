import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CarouselItemComponent} from './brand-view/carousel-item/carousel-item.component';
import {CarouselItemDirective} from './brand-view/carousel-item/carousel-item.directive';
import { BrandRoutingModule } from './brand-routing.module';
import { BrandNavbarComponent } from './shared/brand-navbar/brand-navbar.component';
import { BrandBannerComponent } from './brand-view/brand-banner/brand-banner.component';
import { MultiBrandsComponent } from './brand-view/multi-brands/multi-brands.component';
import { BrandCategoryComponent } from './brand-view/brand-category/brand-category.component';
import { NewArrivalsComponent } from './brand-view/new-arrivals/new-arrivals.component';
import { TopSellersComponent } from './brand-view/top-sellers/top-sellers.component';
import { BrandViewsComponent } from './brand-view/brand-views/brand-views.component';

@NgModule({
  declarations: [
    BrandNavbarComponent,
    BrandBannerComponent,
    MultiBrandsComponent,
    BrandCategoryComponent,
    NewArrivalsComponent,
    TopSellersComponent,
    BrandViewsComponent,
    CarouselItemComponent,
    CarouselItemDirective
  ],
  imports: [
    CommonModule,
    BrandRoutingModule
  ]
})
export class BrandModule { }
