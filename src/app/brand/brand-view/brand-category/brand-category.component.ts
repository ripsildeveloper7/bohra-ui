import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-brand-category',
  templateUrl: './brand-category.component.html',
  styleUrls: ['./brand-category.component.css']
})
export class BrandCategoryComponent implements OnInit {


image = [
{images: '../../../../assets/images/brand/cropzs.jpg' , text: 'JACKETS' ,
para: 'Premium and rugged denim jackets'},
{images: '../../../../assets/images/brand/crop 2.jpg', text: 'JEANS',
para: 'From dark rinse washed jeans to acid washed grey jeans'},
{images: '../../../../assets/images/brand/crop.jpg' , text: 'OUR STORY'
, para: ' What is Scarecrow Jeanswear ? Jeans from jeans experts'}
];


  constructor() { }

  ngOnInit() {
  }

}
