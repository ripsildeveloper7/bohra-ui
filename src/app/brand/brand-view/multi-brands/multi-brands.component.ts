import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multi-brands',
  templateUrl: './multi-brands.component.html',
  styleUrls: ['./multi-brands.component.css']
})
export class MultiBrandsComponent implements OnInit {

image = [
{images: '../../../../assets/images/brand/4M6A8370 1.jpg', name: 'BILLY JEANS', range: '₹ 399'},
{images: '../../../../assets/images/brand/4M6A8370 1.jpg', name: 'LEGEND JEANS' , range: '₹ 399'},
{images: '../../../../assets/images/brand/4M6A8370 1.jpg', name: 'MARLON JEANS', range: '₹ 1109'}];
banner = [{banners: '../../../../assets/images/brand/ww.jpg'}];
  constructor() { }

  ngOnInit() {
  }

}
