
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppSetting } from '../config/appSetting';
import { Cart } from './../shared/model/cart.model';
import { WishList } from './../shared/model/wishList.model';
import { Product } from './../shared/model/product.model';
import { AddressModel } from './../shared/model/address.model';
import { RegModel } from './../shared/model/registration.model';
import {Order} from '../shared/model/order.model';
import {PaymentDetail} from '../shared/model/paymentDetail.model';
import { States } from './states';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  productServiceUrl: string = AppSetting.productServiceUrl;
  customerServiceUrl: string = AppSetting.customerServiceUrl;
  commerceServiceUrl: string = AppSetting.commerceOrderServiceUrl;
  marketingServiceUrl: string = AppSetting.marketingServiceUrl;
  private state_url: string = '../../assets/data/states-and-districts.json';

  constructor(private httpClient: HttpClient) { }

  getState(): Observable<States[]>{
    return this.httpClient.get<States[]>(this.state_url);
  }


  addToCart(cart): Observable<Cart> {
    const cartUrl = 'cart';
    const url: string = this.productServiceUrl + cartUrl;
    return this.httpClient.post<Cart>(url, cart);
  }

  // order
  createCCOrder(cart): Observable<Cart> {
    const cartUrl = 'cart';
    const url: string = this.productServiceUrl + cartUrl;
    return this.httpClient.post<Cart>(url, cart);
  }
  
  getCustomerDetails(userId): Observable<RegModel> {
    const urlprofile = this.customerServiceUrl + 'getcustomerprofile/' + userId;
    return this.httpClient.get<RegModel>(urlprofile);
  }
  deleteAllCart(carId) {
    const cartUrl = 'deletecart/';
    const url: string = this.productServiceUrl + cartUrl + carId;
    return this.httpClient.delete<Cart>(url);
  }
  getaddressDetails(addressHolder, userId): Observable<AddressModel> {
    const urladdress = this.customerServiceUrl + 'updateaddressdetails/' + userId;
    return this.httpClient.put<AddressModel>(urladdress, addressHolder);
  }
  customerAddressDelete(userId, addressId): Observable<RegModel> {
    const urlprofile = this.customerServiceUrl + 'deletecustomeraddress/' + userId + '/delete/' + addressId;
    return this.httpClient.delete<RegModel>(urlprofile);
  }
  customerAddressUpdate(userId, addressId, updateDetails): Observable<any> {
    const urlprofile = this.customerServiceUrl + 'editcustomeraddress/' + userId + '/update/' + addressId;
    return this.httpClient.put<any>(urlprofile, updateDetails);
  }
  addToCartDecrement(cart): Observable<Cart> {
    const cartUrl = 'findcartproduct/';
    const url: string = this.productServiceUrl + cartUrl;
    return this.httpClient.post<Cart>(url, cart);
  }

  addToCartTest(prod) {
    const categoryUrl = 'cart';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.post<Product>(url, prod);
  }
  deleteToCart(userid, proId) {
    const cartUrl = 'deletecart/';
    const productUrl = '/itemId/';
    const url: string = this.productServiceUrl + cartUrl + userid + productUrl + proId;
    return this.httpClient.delete<Cart>(url);
  }
  shoppingUser(userId) {
    const shoppingUrl = 'findcart/';
    const url: string = this.productServiceUrl + shoppingUrl + userId;
    return this.httpClient.get<Cart[]>(url);
  }
  shoppingCart() {
    const shoppingUrl = 'shopping/';
    const url: string = this.productServiceUrl + shoppingUrl;
    return this.httpClient.get<Product>(url);
  }

  addToCartMinus(cart) {
    const cartUrl = 'cart/';
    const productUrl = '/decproduct/';
    const url: string = this.productServiceUrl + cartUrl + cart.userId + productUrl + cart.product.productId;
    return this.httpClient.put<Product>(url, cart);
  }

  // order
  confirmOrder(order) {
    const orderUrl = 'order/';
    const url: string = this.commerceServiceUrl + orderUrl;
    return this.httpClient.put<Order>(url, order);
  }
  confirmRazorPayOrder(order) {
    const orderUrl = 'razorpayorder/';
    const url: string = this.commerceServiceUrl + orderUrl;
    return this.httpClient.put<Order>(url, order);
  }


  addRazorpayResponse(data, id, total): Observable<any> {
    const accUrl = 'addrazorpay/';
    const totalUrl = '/total/';
    const url: string = this.commerceServiceUrl + accUrl + id + totalUrl + total;
    return this.httpClient.put<PaymentDetail>(url, data);
  }
  getWishList(wish): Observable<WishList[]> {
    const pathUrl = 'getwishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.get<WishList[]>(url);
  }
  moveWishlist(wish): Observable<WishList[]> {
    const pathUrl = 'movetowishlistmvp/';
    const url: string = this.productServiceUrl + pathUrl + wish.userId;
    return this.httpClient.put<WishList[]>(url, wish);
  }
  // Marketing

  getAllDiscount(): Observable<any> {
    const pathUrl = 'getdiscount';
    const url: string = this.marketingServiceUrl + pathUrl;
    return this.httpClient.get<any>(url);
  }

  // coupon
  checkCouponOnCustomer(coupon): Observable<any> {
    const pathUrl = 'checkcouponcodebycustomer';
    const url: string = this.commerceServiceUrl + pathUrl;
    return this.httpClient.post<any>(url, coupon);
  }
  getFirstTimeSignUpCoupon(): Observable<any> {
    const pathUrl = 'getfirstsignupcouponforproduct';
    const url: string = this.marketingServiceUrl + pathUrl;
    return this.httpClient.get<any>(url);
  }
  getFlexibleCoupon(): Observable<any> {
    const pathUrl = 'getflexiblecouponforproduct';
    const url: string = this.marketingServiceUrl + pathUrl;
    return this.httpClient.get<any>(url);
  }
  checkCouponForFirstCustomer(id): Observable<any> {
    const pathUrl = 'checkcouponforfirstcustomer/';
    const url: string = this.commerceServiceUrl + pathUrl + id;
    return this.httpClient.get<any>(url);
  }
  checkCouponForPlaceOrder(id): Observable<any> {
    const pathUrl = 'checkcouponforplaceorder/';
    const url: string = this.marketingServiceUrl + pathUrl + id;
    return this.httpClient.get<any>(url);
  }
  getCouponRandomly(id): Observable<any> {
    const pathUrl = 'findcouponrandomly/';
    const url: string = this.marketingServiceUrl + pathUrl + id;
    return this.httpClient.get<any>(url);
  }
  
  // ready to wear
  getAllReadyToWear(): Observable<any> {
    const productUrl = 'getallreadytowear';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<Product>(url);
  }
  getMeasurementbyUser(): Observable<any> {
    const productUrl = 'getallmeasurementbyuser';
    const url: string = this.customerServiceUrl + productUrl;
    return this.httpClient.get<Product>(url);
  }
  getShippingFees(): Observable<any> {
    const productUrl = 'getshippingfees';
    const url: string = this.commerceServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getIncrementRate(): Observable<any> {
    const productUrl = 'getincrementrate';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  getAllSizeWiseIncrement(): Observable<any> {
    const productUrl = 'getallsizewiseincrement';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  updateQtyByShipping(data): Observable<any> {
    const path = 'updateinventorybyshipping';
    const url: string = this.productServiceUrl + path;
    return this.httpClient.post<any>(url, data);
  }
}
