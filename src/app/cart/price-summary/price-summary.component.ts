import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'app-price-summary',
  templateUrl: './price-summary.component.html',
  styleUrls: ['./price-summary.component.css']
})
export class PriceSummaryComponent implements OnInit {
@Input() totalItems: number;
@Input() subTotal: number;
@Input() totalAmount: number;
@Input() couponType: any;
@Input() couponValue: any;
@Input() firstPurchase: boolean;
@Input() totalShipement: any;
@Output() sendCoupon = new EventEmitter<any>();
@Output() removeCoupon = new EventEmitter<any>();
@ViewChild('coupon', { read: ElementRef , static: true}) couponReference: ElementRef;
couponData: {
  couponCode: string
};
  constructor(private el: ElementRef) { 
   
  }

  ngOnInit() {
  }
  checkCoupon(coupon) {
    this.couponData = {
      couponCode: coupon.toUpperCase()
    };
    this.couponReference.nativeElement.value = '';
    this.sendCoupon.emit(this.couponData);
  }
  clearCoupon(subtlt) {
    this.firstPurchase = false;
    this.removeCoupon.emit(subtlt);
  }
}
