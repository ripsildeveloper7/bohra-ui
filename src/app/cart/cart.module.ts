import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartRoutingModule } from './../cart/cart-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { AddressComponent } from './address/address.component';
import { AddressService } from './address/address.service';
import { PriceSummaryComponent } from './price-summary/price-summary.component';
import {
  MatSidenavModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule,
  MatSliderModule
} from '@angular/material';
import { PlaceOrderComponent } from './checkout/place-order/place-order.component';
import { AllAddressComponent } from './checkout/all-address/all-address.component';
import { CheckoutCartComponent } from './checkout/checkout-cart/checkout-cart.component';
import { ConfirmComponent } from './checkout/confirm/confirm.component';
import { WindowRefService } from './checkout/window-ref.service';
import { SharedModule } from './../shared/shared.module';
import {PaypalComponent} from './checkout/paypal/paypal.component';
import { PaymentSuccessComponent } from './checkout/payment-success/payment-success.component';
import { CheckoutSummeryComponent } from './checkout/checkout-summery/checkout-summery.component';
@NgModule({
  declarations: [ShoppingCartComponent, PriceSummaryComponent,
    PlaceOrderComponent, PaypalComponent, CheckoutSummeryComponent,
    AllAddressComponent, AddressComponent,
    CheckoutCartComponent, ConfirmComponent, PaymentSuccessComponent],
  imports: [
    CommonModule,
    CartRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    HttpClientModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    MatMenuModule,
    MatStepperModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatSliderModule
  ],
  providers: [AddressService, WindowRefService],
  entryComponents: [AddressComponent]
})
export class CartModule { }
