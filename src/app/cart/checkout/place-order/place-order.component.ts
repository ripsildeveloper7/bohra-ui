import { Component, OnInit , Input} from '@angular/core';
import { FormArray, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatStepper } from '@angular/material';
import { Router } from '@angular/router';
/* import { ProductService } from '../product.service';
import { Product } from '../../shared/model/product.model';
import {SingleProductOrder} from '../../shared/model/singleProductOrder.model';
import {AddressModel} from '../../account-info/address/address.model';
import {Order} from '../../shared/model/order.model'; */
import { AddressModel } from './../../../shared/model/address.model';
import { RegModel } from './../../../shared/model/registration.model';
import { CartService } from './../../cart.service';
import { AddressService } from './../../../cart/address/address.service';
import { Cart } from './../../../shared/model/cart.model';
import { Order } from './../../../shared/model/order.model';
import { WindowRefService } from './../window-ref.service';
import { PaymentDetail } from './../../../shared/model/paymentDetail.model';

import {  ViewChild, ElementRef} from '@angular/core';
import { ConverterPipe } from './../../../shared/nav/converter.pipe';
declare var paypal;

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;
  // @ViewChild('stepper', {static: true}) stepper: MatStepper;
  @ViewChild('form', {static: true}) form: ElementRef;
  @Input()  paymentModel: any;
  userId: string;
  addressModel: AddressModel[];
  regModel: RegModel;
  orderForm: FormGroup;
  shopModel: any = [];
  cartModel: Cart;
  id;
  /* orderModel: Order; */
  addressSelected: AddressModel;
  subTotal = 0;
  totalItems = 0;
  totalAmount = 0;
  couponModel;
  couponValue;
  couponType;
  packSum: number;
  paymentType: any;
  checkOutofStack: any;
  orderModel: Order;
  razorPayOrder: Order;
  rzp1;
  orderId;
  paymentDetailModel: PaymentDetail;
  typeOfPayment;
  discountStore;
  checkPoint1 = false;
  checkPoint2 = false;
  checkPoint3 = false;
  checkPoint4 = false;
  checkPoint5 = false;
  checkPoint6 = false;
  checkPoint7 = false;
  checkPoint8 = false;
  checkPoint9 = false;
  showMobileView = true;
  encRequest: string;
  accessCode: string;
  serviceData: any;
  totalShipement: any;
  holderMeasurment;
  incRateModel: any;
  complete: boolean;
  confirmEnable: boolean;
  sizInc: any;
  holder: any;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private converterPipe: ConverterPipe,
              private snackBar: MatSnackBar, private router: Router, private addressService: AddressService,
              private winRef: WindowRefService, private cartService: CartService) { 
                this.userId = sessionStorage.getItem('userId');
    if (JSON.parse(sessionStorage.getItem('login'))) {
      this.userId = sessionStorage.getItem('userId');
      this.getCustomerDetails();
      
     /*   */
    } else {
      this.shopModel = JSON.parse(sessionStorage.getItem('cart')) || [];
    }
              }

  ngOnInit( ) {
    this.createForm();
    /* this.viewSingleProduct(); */
    this.paypalApply();
    // this.selectAddress();
  }
  ngAfterViewInit() {
    this.getWindowSize();
    // this.selectAddress();
   

  }
  // ngOnChanges(){
  //   this.selectAddress();
  // }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }

  // selectAddress(){
  //   if(this.addressModel.length > 0){
  //   this.complete = true;
  //   }
  // }
  /* pay() {
    // this.cartValue contains all the order information which is sent to the server
    const sub = this.converterPipe.transform(this.totalAmount);
    const totalItem = this.shopModel.map(element => element.items);
    const orderedProducts = this.shopModel.map(element => element.cart_product);
    this.orderModel = new Order();
    this.orderModel.customerId = this.userId;
    this.orderModel.addressDetails = this.addressSelected;
    this.orderModel.total = sub;
    this.orderModel.cart = totalItem;
    this.orderModel.coupon = this.couponModel;
    this.orderModel.orderedProducts = orderedProducts;
    // You can use this package to encrypt - https://www.npmjs.com/package/node-ccavenue/
    this.cartService.createCCOrder(this.orderModel).subscribe((response: any) => {
      this.encRequest = response.encRequest;
      setTimeout(_ => this.form.nativeElement.submit());
    }, error => {
      console.log(error);
    });
  } */
  actionPlus(totalItem) {
    this.cartModel = new Cart();
    this.cartModel.userId = this.userId;
    this.cartModel.items = totalItem;
    this.cartService.addToCart(this.cartModel).subscribe(data => {
      this.shopModel = data;
      /* this.getDiscount(); */
      this.total();
    }, error => {
      console.log(error);
    });
  }
  actionMinus(totalItem) {
    this.cartModel = new Cart();
    this.cartModel.userId = this.userId;
    this.cartModel.items = totalItem;
    this.cartService.addToCartDecrement(this.cartModel).subscribe(data => {
      this.shopModel = data;
      this.total();
      /* this.getDiscount(); */
    }, error => {
      console.log(error);
    });
  }
  removeCart(item) {
    this.cartService.deleteToCart(this.userId, item).subscribe(data => {
      this.shopModel = data;
      this.total();
      /* this.getDiscount(); */
    }, err => {
      console.log(err);
    });
  }
  createForm() {
    this.orderForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      phoneNumber: [''],
      emailId: [''],
      streetAddress: [''],
      building: [''],
      landmark: [''],
      city: [''],
      state: [''],
      pincode: [''],
      qty: [''],
      productPrice: [''],
      totalPrice: ['']
    });
  }
  getCustomerDetails() {
    this.cartService.getCustomerDetails(this.userId).subscribe(data => {
      this.regModel = data;
      this.addressModel = data.addressDetails;
      this.addressSelected = this.addressModel[0];
      this.shoppingCartUser(this.userId);
    }, error => {
      console.log(error);
    });
  }
  addAddressEvent() {
    this.addressService.openAddress().subscribe(
      res => {
        if (res) {
          this.getCustomerDetails();
        }
      }
    );
    // this.addAddress();
  }
  selectedAddress(event) {
    if (event) {
      this.addressSelected = event;
      console.log('selected address', this.addressSelected);
    }
    // this.addAddress();
  }
  addAddress() {
    // this.stepper.selected.completed = true;
    // this.stepper.next();
  }
  editAddress(data) {
    this.addressService.editAddress(data).subscribe(
      res => {
        if (res) {
          this.getCustomerDetails();
        }
      }
    );
  }
  paymentMethod(type) {
    this.typeOfPayment = type;
  }
  confirmOrderData(val) {
    const sub = this.converterPipe.transform(this.totalAmount);
    console.log('type', typeof(sub));
    console.log(sub, 'converted amt');
    console.log('address', this.addressSelected);
    const totalItem = this.shopModel.map(element => element.items);
    const orderedProducts = this.shopModel.map(element => element.cart_product);
    console.log('final product check', orderedProducts);
    this.orderModel = new Order();
    this.orderModel.customerId = this.userId;
    this.orderModel.addressDetails = this.addressSelected;
    this.orderModel.total = sub;
    this.orderModel.paymentMode = 'razorpay';
  /*   this.orderModel.paymentorderId = orderData.paypalOrderId; */
    this.orderModel.cart = totalItem;
    this.orderModel.measurement = this.holderMeasurment;
    this.orderModel.coupon = this.couponModel;
    this.orderModel.shipingFees = this.totalShipement;
    this.orderModel.orderedProducts = orderedProducts;
    this.cartService.confirmRazorPayOrder(this.orderModel).subscribe(data => {
      this.orderModel = data;
      this.orderId = data._id;
      if (this.orderModel.paymentMode !== 'razorpay') {
        this.deleteCart(this.userId);
      }
     /*  this.deleteCart(this.userId); */
    /*   this.qtyUpdate(this.orderModel); */
      sessionStorage.removeItem('coupon');
      this.initPay(data.razorPayOrderId);
    }, err => {
      console.log(err);
    });
  }
  initPay(orderId) {
    const options = {
      key: 'rzp_live_6YtLBCKJ8k0Ie1',
      amount: '1000',
      order_id: orderId,
      name: 'UcchalFashion',
      handler: this.paymentResponseHander.bind(this)
    };
    this.rzp1 = new this.winRef.nativeWindow.Razorpay(options);
    this.rzp1.open();
  }
  paymentResponseHander(response) {
    this.razorPayDetails(response);
  }
  razorPayDetails(response) {
    this.razorPayOrder = new Order();
    this.paymentDetailModel = new PaymentDetail();
    this.paymentDetailModel.paymentId = response.razorpay_payment_id;
    this.paymentDetailModel.razorpayOrderId = response.razorpay_order_id;
    this.paymentDetailModel.razorpaySignature = response.razorpay_signature;
    this.cartService.addRazorpayResponse(this.paymentDetailModel, this.orderId, this.totalAmount).subscribe(data => {
      this.razorPayOrder = data;
      if (this.razorPayOrder.paymentStatus === 'Success') {
        this.router.navigate(['/account/orders']);
        this.deleteCart(this.userId);
      }
    }, err => {
      console.log(err);
    });
  }
  shoppingCartUser(userId) {
    console.log('step 1 -> shopping cart')
    this.cartService.shoppingUser(userId).subscribe(data => {
      this.shopModel = data;
      console.log('cart', this.shopModel);
      this.getIncrementRate();
      /* this.getDiscount(); */
    /*   this.total(); */
    }, err => {
      console.log(err);
    });
  }
  getIncrementRate() {
    console.log('step 2 -> inc')
    this.cartService.getIncrementRate().subscribe(data => {
      this.incRateModel = data;
      this.discountCalculation();
    }, error => {
      console.log(error);
    });
  }
  discountCalculation() {
    console.log('step 3 -> discoubt')
    this.totalShipement = 0;
    for (const cart of this.shopModel) {
      if (typeof cart.items.INTsku === 'number') {
        cart.items.INTsku = cart.items.INTsku.toString();
      }
      for (const product of cart.cart_product[0].child) {
        if (typeof product.INTsku === 'number') {
          product.INTsku =  product.INTsku.toString();
        }
        /* if (product.vp < 2000) {
          product.shippingCharege = 499.8;
        } else {
          product.shippingCharege = 0;
        } */
        const sp = product.sp * 110 / 100;
        product.sp = sp;
        product.price = sp;
        product.subPrice = sp;
        const discount = 100 - product.discount;
        const totalPrice = product.sp * (100 / discount);
        const savePrice = totalPrice - product.sp;
        product.savePrice = savePrice;
        product.totalPrice = totalPrice;
        /* this.totalShipement += product.shippingCharege; */
      }
    }
    this.getAllMeasurmentByUser();
  }
  getAllMeasurmentByUser() {
    console.log('step 4 -> measurement')
    this.cartService.getMeasurementbyUser().subscribe(data => {
      this.serviceData = data;

      this.checkService();
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  getAllReadyToWear() {
    this.cartService.getAllReadyToWear().subscribe(data => {
      this.serviceData = data;
      this.checkService();
    }, error => {
      console.log(error);
    });
  }
  /* checkService() {
    for (const cart of this.shopModel) {
      if (cart.items.tailoringService === true) {
        this.serviceData.forEach(element => {
          if (element._id === cart.items.serviceId) {
            for (const product of cart.cart_product[0].child) {
              product.serviceActive = true;
              product.serviceType = 'Ready to Wear';
              product.serviceName = element.serviceName;
              product.serviceAmount = element.price;
              product.serviceDiscount = element.discount;
              product.price = product.sp + element.price;
            }
          }
        });
      } else {
        continue;
      }
    }
    this.total();
  } */
  checkService() {
    console.log('step 4 -> service')
    this.holderMeasurment = [];
    for (const cart of this.shopModel) {
      if (cart.items.tailoringService === true) {
        if (this.serviceData.length !== 0) {
        this.serviceData.forEach(element => {
          if (element._id === cart.items.serviceId) {
            for (const product of cart.cart_product[0].child) {
              product.serviceActive = true;
              if (cart.items.isMeasurement === true) {
                product.serviceType = 'Made to Measure';
                product.serviceId = element._id;
                product.serviceName = element.serviceName;
                product.serviceAmount = element.price;
                product.serviceDiscount = element.discount;
                product.price = product.sp + element.price * (100 - element.discount) / 100;
                this.holderMeasurment.push(element);
              } else if (cart.items.isUnstitched === true) {
                product.serviceType = 'Unstitched Fabric';
                product.serviceDiscount = 0;
                product.serviceAmount = 0;
                product.serviceDiscount = 0;
                product.price = product.sp + 0;
              }
             /*  product.serviceType = 'Ready to Wear'; */
            }
          } else {
            for (const product of cart.cart_product[0].child) {
              if (cart.items.isUnstitched === true) {
                product.serviceActive = true;
                product.serviceType = 'Unstitched Fabric';
                product.serviceDiscount = 0;
                product.serviceAmount = 0;
                product.serviceDiscount = 0;
                product.price = product.sp + 0;
              }
            }
          }
        });
      } else {
          for (const product of cart.cart_product[0].child) {
            if (cart.items.isUnstitched === true) {
              product.serviceActive = true;
              product.serviceType = 'Unstitched Fabric';
              product.serviceDiscount = 0;
              product.serviceAmount = 0;
              product.serviceDiscount = 0;
              product.price = product.sp + 0;
            }
          }
      }
      } else {
        continue;
      }
    }
    /* this.discountCalculation(); */
    this.getAllSizeWiseIncrement();
  }
  getAllSizeWiseIncrement() {
    this.cartService.getAllSizeWiseIncrement().subscribe(data => {
      this.sizInc = data;
      if (this.sizInc.length === 0) {
        this.total();
      } else {
        this.applySizeWiseIncrement();
      }
    }, error => {
      console.log(error);
    });
  }
  applySizeWiseIncrement() {
    console.log('sizeInc', this.sizInc);
    for ( const shop of this.shopModel) {
     for (const product of shop.cart_product[0].child) {
       if (shop.items.INTsku === product.INTsku) {
         if (product.variationType === 'Size') {
           for (const size of this.sizInc) {
             if (size.subCategoryId) {
               if (size.subCategoryId === product.subCategoryId) {
                 if (Number(size.size) <= Number(product.sizeVariant)) {
                  product.sizeIncApply = true;
                  product.sizeIncType = 'Additional charges for plus size';
                  product.sizeWisePrice = (product.subPrice * size.incPercentage) / 100;
                  product.price = product.price + (product.subPrice * size.incPercentage) / 100;
                  product.serviceAmount = product.serviceAmount + (product.subPrice * size.incPercentage) / 100;
                 }
               }
             } else {
              if (size.superCategoryId === product.superCategoryId) {
                if (Number(size.size) <= Number(product.sizeVariant)) {
                  product.sizeIncApply = true;
                  product.sizeIncType = 'Additional charges for plus size';
                  product.sizeWisePrice = (product.subPrice * size.incPercentage) / 100;
                  product.price = product.price + (product.subPrice * size.incPercentage) / 100;
                  product.serviceAmount = product.serviceAmount + (product.subPrice * size.incPercentage) / 100;
                }
              }
             }
           }
         }
       }
     }
    }
    this.total();
  }
  deleteData(addressId) {
    this.cartService.customerAddressDelete(this.userId, addressId).subscribe(data => {
      this.regModel = data;
      this.addressModel = data.addressDetails;
      this.addressSelected = this.addressModel[0];
    }, error => {
      console.log(error);
    });
  }
  deleteCart(userId) {
    console.log('payment order', this.razorPayOrder);
    this.cartService.deleteAllCart(userId).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
    }, error => {
      console.log(error);
    });
  }
  qtyUpdate(order) {
      this.cartService.updateQtyByShipping(order).subscribe(data => {
        this.shopModel = data;
      }, error => {
        console.log(error);
      });
  }
  total() {
    console.log('step 5 -> total')
    this.subTotal = 0;
    this.totalItems = 0;
    const totalProduct: any = this.shopModel.map(item => item.cart_product[0]);
    const totalSet = this.shopModel.map(item => item.items);
    this.totalItems += totalSet.length;
    totalSet.map(item => {
      const priceSingle = totalProduct.find(test => test._id === item.productId && test.applied !== true);
      priceSingle.applied = true;
      const priceSizeVariant = priceSingle.child;
      const priceSize = priceSizeVariant.find(check => check.INTsku === item.INTsku);
      this.subTotal += item.qty * priceSize.price;
    });
    totalProduct.forEach(element => {
      element.applied = false;
    });
    this.totalAmount = this.subTotal;
  this.confirmEnable = true;
  console.log('confirm enable status', this.confirmEnable);
    console.log('total inititaed in parent', this.totalAmount);
    sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
    this.getShippingFees();
    this.checkQty();
  }
  getShippingFees() {
    this.cartService.getShippingFees().subscribe(data => {
      const temp = data;
      if (temp.length === 0) {
        this.checkCoupon();
      } else {
        this.holder = data[0];
        console.log(data,'fees');
        this.shippingChargeApply();
      }
    }, error => {
      console.log(error);
    });
  }
  checkQty() {
    this.shopModel.forEach((element) => {
      element.quantityCheck = 0;
      if (element.cart_product.length > 0) {
        this.eachSizeCheck(element, element.cart_product[0], element.items.INTsku);
      }
    });
  }

  eachSizeCheck(element, product, sku) {
    product.child.forEach((variantElement, index) => {
      if (sku === variantElement.INTsku) {
        element.quantityCheck += variantElement.quantity;
      }
    });
  }
  shippingChargeApply() {
    if (this.totalAmount > this.holder.minimumPrice) {
      this.totalShipement = 0;
    } else {
      this.totalShipement = this.holder.fees;
    }
    this.totalAmount = this.totalAmount + this.totalShipement;
    this.checkCoupon();
  }
  checkCoupon() {
    if (sessionStorage.getItem('coupon')) {
      const couponID = sessionStorage.getItem('coupon');
      this.cartService.checkCouponForPlaceOrder(couponID).subscribe(data => {
        console.log(data);
        this.couponModel = data;
        if (data.length !== 0) {
          this.calculationOfCoupon(data);
        }
      }, error => {
        console.log(error);
      });
    } else {
      console.log('no');
  }
  }
  calculationOfCoupon(coupon) {
    console.log('coupon', coupon);
    this.totalAmount = this.subTotal;
    coupon.forEach(element => {
      if (element.amountType === 'Flat') {
        this.couponType = 'Flat';
        this.couponValue = element.typeValue;
        this.totalAmount = this.totalAmount - element.typeValue;

      } else if (element.amountType === 'Percentage') {
        this.couponType = 'Percentage';
        this.couponValue = element.typeValue;
        this.totalAmount = this.totalAmount - Math.round(this.totalAmount / 100 * element.typeValue);
      }
    });
    console.log(this.totalAmount, this.couponType, this.couponValue);
  }
  getDiscount() {
    this.cartService.getAllDiscount().subscribe(data => {
      this.discountStore = data;
      /* this.discountTotal(); */
      this.total();
    }, error => {
      console.log(error);
    });
  }
  /* discountTotal() {
    this.subTotal = 0;
    this.totalItems = 0;
    this.totalItems = this.shopModel.map(ele => ele.items).length;
    console.log('discount', this.discountStore);
    for (let l = 0; l <= this.shopModel.length - 1; l++) {
      for (let m = 0; m <= this.shopModel[l].cart_product.length - 1; m++) {
        for (let n = 0; n <= this.shopModel[l].cart_product[m].child.length - 1; n++) {
          if (this.shopModel[l].cart_product[m].child[n].sku === this.shopModel[l].items.sku ) {
                  if (this.shopModel[l].cart_product[m].discount === undefined || this.shopModel[l].cart_product[m].discount === 0) {
                this.shopModel[l].cart_product[m].child[n].discount = 0;
                this.checkPoint1 = true;
                this.shopModel[l].cart_product[m].displayClass = 'discountNone';
              }
                  for (let i = 0; i <= this.discountStore.length - 1; i++) {
                    for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
                      for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {
                  if (this.shopModel[l].cart_product[m]._id === this.discountStore[i].conditions[j].value[k]) {
                    if (this.discountStore[i].amountType === 'Percentage') {
                      this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                      this.checkPoint2 = true;
                      this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                      console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
                } else {
                  this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                  this.checkPoint3 = true;
                  this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                  console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
                }
              }
            }
          }
            }
                  if (this.checkPoint1 === true || this.checkPoint2 === true || this.checkPoint3 === true) {
                  console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty);
                  this.subTotal += this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty;
                  this.checkPoint1 = false;
                  this.checkPoint2 = false;
                  this.checkPoint3 = false;
              }
          }
        }
      }
    }
    console.log('subTotal', this.subTotal);
    console.log('totalItem', this.totalItems);
    console.log( 'updated' , this.shopModel);
  } */
  discountTotal() {
    this.subTotal = 0;
    this.totalItems = 0;
    this.totalItems = this.shopModel.map(ele => ele.items).length;
    console.log('discount', this.discountStore);
    for (let l = 0; l <= this.shopModel.length - 1; l++) {
      for (let m = 0; m <= this.shopModel[l].cart_product.length - 1; m++) {
        for (let n = 0; n <= this.shopModel[l].cart_product[m].child.length - 1; n++) {
          if (this.shopModel[l].cart_product[m].child[n].sku === this.shopModel[l].items.sku ) {
                  if (this.shopModel[l].cart_product[m].discount === undefined || this.shopModel[l].cart_product[m].discount === 0) {
                this.shopModel[l].cart_product[m].child[n].discount = 0;
                this.checkPoint1 = true;
                this.shopModel[l].cart_product[m].displayClass = 'discountNone';
                this.shopModel[l].cart_product[m].child[n].displayClass = 'discountNone';
              }
                  for (let i = 0; i <= this.discountStore.length - 1; i++) {
                    for (let j = 0; j <= this.discountStore[i].conditions.length - 1; j++) {
                      for (let k = 0; k <= this.discountStore[i].conditions[j].value.length - 1; k++) {
/* -------------------------------------------------------------------------------------------------------- */
                  if (this.discountStore[i].conditions[j].field === 'Product Name') {
                    if (this.shopModel[l].cart_product[m]._id === this.discountStore[i].conditions[j].value[k]) {
                    if (this.shopModel[l].cart_product[m].child[n].displayClass === 'discountStyle') {
                      if (this.discountStore[i].amountType === 'Flat') {
                        const temp = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                        if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                          this.shopModel[l].cart_product[m].child[n].discount = temp;
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.checkPoint4 = true;
                          console.log('check point 8', this.shopModel[l].cart_product[m].child[n].discount);
                        } else {
                          continue;
                        }
                      } else if (this.discountStore[i].amountType === 'Percentage') {
                        const temp = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                        if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                          this.shopModel[l].cart_product[m].child[n].discount = temp;
                          this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                          this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                          this.checkPoint5 = true;
                          console.log('check point 6', this.shopModel[l].cart_product[m].child[n].discount);
                        } else {
                          continue;
                        }
                      }
                    } else {
                      if (this.discountStore[i].amountType === 'Percentage') {
                        this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                        this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                        this.checkPoint2 = true;
                        this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                        console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
                  } else {
                    this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                    this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                    this.checkPoint3 = true;
                    this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                    console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
                  }
                    }
              } else {
                continue;
              }
            } else if (this.discountStore[i].conditions[j].field === 'Product Category') {
              if (this.shopModel[l].cart_product[m].superCategoryId === this.discountStore[i].conditions[j].value[k]) {
                if (this.shopModel[l].cart_product[m].child[n].displayClass === 'discountStyle') {
                  if (this.discountStore[i].amountType === 'Flat') {
                    const temp = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                    if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                      this.shopModel[l].cart_product[m].child[n].discount = temp;
                      this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                      this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                      this.checkPoint8 = true;
                      console.log('check point 4', this.shopModel[l].cart_product[m].child[n].discount);
                    } else {
                      continue;
                    }
                  } else if (this.discountStore[i].amountType === 'Percentage') {
                    const temp = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                    if (this.shopModel[l].cart_product[m].child[n].discount > temp) {
                      this.shopModel[l].cart_product[m].child[n].discount = temp;
                      this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                      this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                      this.checkPoint9 = true;
                      console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount);
                    } else {
                      continue;
                    }
                  }
                } else {
                  if (this.discountStore[i].amountType === 'Percentage') {
                    this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                    this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                    this.checkPoint6 = true;
                    this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - Math.round(this.shopModel[l].cart_product[m].child[n].price / 100 * this.discountStore[i].typeValue);
                    console.log('check point 1', this.shopModel[l].cart_product[m].child[n].discount);
              } else {
                this.shopModel[l].cart_product[m].displayClass = 'discountStyle';
                this.shopModel[l].cart_product[m].child[n].displayClass = 'discountStyle';
                this.checkPoint7 = true;
                this.shopModel[l].cart_product[m].child[n].discount = this.shopModel[l].cart_product[m].child[n].price - this.discountStore[i].typeValue;
                console.log('check point 2', this.shopModel[l].cart_product[m].child[n].discount);
              }
                }
              }
            }
              /* ----------------------------------------------------------------------------------------------- */
            }
          }
            }
                  if (this.checkPoint1 === true || this.checkPoint2 === true || this.checkPoint3 === true ||
                      this.checkPoint6 === true || this.checkPoint7 === true) {
                  console.log('check point 3', this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty);
                  this.subTotal += this.shopModel[l].cart_product[m].child[n].discount === 0 ? this.shopModel[l].cart_product[m].child[n].price * this.shopModel[l].items.qty : this.shopModel[l].cart_product[m].child[n].discount * this.shopModel[l].items.qty;
                  this.checkPoint1 = false;
                  this.checkPoint2 = false;
                  this.checkPoint3 = false;
              }
          }
        }
      }
    }
    console.log('subTotal', this.subTotal);
    console.log('totalItem', this.totalItems);
    console.log( 'updated' , this.shopModel);
  }

  // paypal testing

  paypalApply() {
    console.log('paypal apply method invoke');
    const sub = this.converterPipe.transform(this.totalAmount);
    console.log('sub value', sub);
    console.log('total value in confirm', this.totalAmount);
    paypal.Buttons({
       createOrder: (data, actions) => {
         return actions.order.create({
           purchase_units: [{
             description: 'Sample',
             amount: {
               currency_code: 'USD',
               value: sub
             }
           }]
         }).then(
         console.log(actions, 'actions'));
       },
       onApprove: (data, approveactions) => {
        // Capture the funds from the transaction
        return approveactions.order.capture().then(function(details) {
          // Show a success message to your buyer
          console.log('paypal details', details);
          // live
    window.location = <any>'https://www.ucchalfashion.com/cart/orderid/'+ details.id  ;
    // local
   /*  window.location = <any>'http://localhost:4500/cart/orderid/'+ details.id  ; */
        });
      }
     }).render(this.paypalElement.nativeElement);
  }
}
