import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-checkout-summery',
  templateUrl: './checkout-summery.component.html',
  styleUrls: ['./checkout-summery.component.css']
})
export class CheckoutSummeryComponent implements OnInit {
  @Input() totalItems: number;
  @Input() subTotal: number;
  @Input() totalAmount: number;
  @Input() couponType: any;
  @Input() couponValue: any;
  @Input() totalShipement: any;
/*   @Output() sendCoupon = new EventEmitter<any>();
  @Output() removeCoupon = new EventEmitter<any>();
  @ViewChild('coupon', { read: ElementRef , static: true}) couponReference: ElementRef; */
  couponData: {
    couponCode: string
  };
    constructor(private el: ElementRef) { }
  
    ngOnInit() {
    }
    /* checkCoupon(coupon) {
      this.couponData = {
        couponCode: coupon.toUpperCase()
      };
      this.couponReference.nativeElement.value = '';
      this.sendCoupon.emit(this.couponData);
    }
    clearCoupon(subtlt) {
      this.removeCoupon.emit(subtlt);
    } */
  }
  