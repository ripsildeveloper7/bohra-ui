import { Component, ViewChild, ElementRef, AfterViewInit, HostListener, Inject } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AppSetting } from './config/appSetting';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedService } from './shared/shared.service';
import { ConverterPipe } from './shared/nav/converter.pipe';

export let price;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'etailx';
  priceModel: any;
  constructor(private route: Router, private sharedService: SharedService, private convertPipe: ConverterPipe) {
    this.route.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (window as any).ga('set', 'page', event.urlAfterRedirects);
        (window as any).ga('send', 'pageview');
        (window as any).fbq('track', 'PageView');
      }
    });
   
  }
  getPrice() {
    this.sharedService.getPriceRate().subscribe(data => {
      this.priceModel = data;
      let holder;
      for (const model of this.priceModel) {
        if (model.currencyCode === 'USD') {
          holder = model.amount;
        } else {
          if (model.currencyCode === 'USD') {
            holder = model.amount;
          }
        }
      }
      price = holder;
      this.convertPipe.setRate(this.priceModel);
      console.log('USD PRICE', this.priceModel);
      }, error => {
        console.log(error);
      });
  }


}
