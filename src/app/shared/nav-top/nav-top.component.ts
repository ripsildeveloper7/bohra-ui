import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-nav-top',
  templateUrl: './nav-top.component.html',
  styleUrls: ['./nav-top.component.css']
})
export class NavTopComponent implements OnInit {
  footerDetails: any;

  constructor(public sharedSerivce: SharedService) { }

  ngOnInit() {
    this.allFooter();
  }

  logOut() {

    sessionStorage.removeItem ( 'login');
    sessionStorage.removeItem('userId');
  }
  allFooter() {
    this.sharedSerivce.getFooterDetails().subscribe(data => {
      this.footerDetails = data[0];
      console.log(this.footerDetails);
      /* console.log(data); */
    }, error => {
      console.log(error);
    });
  }
}
