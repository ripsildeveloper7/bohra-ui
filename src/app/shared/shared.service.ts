import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AppSetting } from '../config/appSetting';
import { FooterComponent } from './footer/footer.component';
import { Footer } from './footer/footer.model';
import { Header } from './nav/header.model';
import { SuperCategory } from './../shared/model/superCategory.model';
import { States } from './states';
import { RegModel } from '../account-info/registration/registration.model';

import { SignIn } from '../account-info/signin/signIn.model';
import { Cart } from './model/cart.model';
import { Rates } from './nav/rates.model';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  rates: Rates;
  productServiceUrl: string = AppSetting.productServiceUrl;
  customerServiceUrl: string = AppSetting.customerServiceUrl;
  contentServiceUrl: string = AppSetting.contentServiceUrl;
  
  private state_url: string = '../../assets/data/states-and-districts.json';
  invokeEvent: Subject<any> = new Subject(); 
  invokeEventC = this.invokeEvent.asObservable();
  public temp: any;
  constructor(private httpClient: HttpClient) {
   }
 /*  getPriceRate() {
    const productUrl = 'getpricerate';
    const url: string = this.productServiceUrl + productUrl;
    this.httpClient.get<any>(url).subscribe(data => {
      this.temp = data[0].priceRate;
      return this.temp;
    });
  } */
  getPriceRate(): Observable<any> {
    const productUrl = 'getpricerate';
    const url: string = this.productServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }
  callMethod() {
    return this.invokeEvent.next(true);
}
  getState(): Observable<States[]>{
    return this.httpClient.get<States[]>(this.state_url);
  }
  getSuperCategory(): Observable<any> {
    const categoryUrl = 'viewcategorywithattribute';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<SuperCategory>(url);
  }

  getFooterDetails(): Observable<any> {

    const categoryUrl = 'getfooterdetail';
    const url: string = this.contentServiceUrl + categoryUrl;
    return this.httpClient.get<Footer>(url);

  }

  /* getHeaderDetails(): Observable<any> {
    const categoryUrl = 'headerDetails';
    const url: string = this.productServiceUrl + categoryUrl;
    return this.httpClient.get<Footer>(url);
  } */
  getHeaderImage(): Observable<any> {
    const categoryUrl = 'getheaderimage';
    const url: string = this.contentServiceUrl + categoryUrl;
    return this.httpClient.get<Header>(url);
  }
  addToWishlist() {
    let sum = 0;
    if (JSON.parse(sessionStorage.getItem('login'))) {
        sum = +sessionStorage.getItem('wislistLength');
        return sum;
    } else {
      return sum;
    }
  }
  addToQty() {
    let sum = 0;
    if (JSON.parse(sessionStorage.getItem('login'))) {
        sum = +sessionStorage.getItem('cartqty');
        return sum;
    } else {
      const cart = JSON.parse(sessionStorage.getItem('cart')) || [];
    /*   cart.map(item => {
        sum += item.length;
      }); */
      return cart.length;
    }
  }
  addToWhishQty() {
    let sum = 0;
    if (JSON.parse(sessionStorage.getItem('login'))) {
      sum = +sessionStorage.getItem('wislistLength');
      return sum;
    } else {
      return sum;
    }
  }
  getLogin() {
    return JSON.parse(sessionStorage.getItem('login'));
  }

  getEmail() {
    return sessionStorage.getItem('emailId');
  }
  sessionLogout() {
    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('emailId');
    sessionStorage.setItem('login', 'false');
    sessionStorage.removeItem('cartqty');
  }
  findName() {
    return sessionStorage.getItem('userEmailId');
  }
  showMyAccount() {
    return sessionStorage.getItem('login');
  }
  socialMediaLogin(holder): Observable<any> {
    const urlway = this.customerServiceUrl + 'addsocialmediacustomerforlogin';
    return this.httpClient.post<RegModel>(urlway, holder);
  }
  signIn(data: SignIn): Observable<any> {
    const signInurl = 'customerlogin';
    const url: string = this.customerServiceUrl + signInurl;
    return this.httpClient.post<SignIn>(url, data);
  }
  shoppingUser(userId) {
    const shoppingUrl = 'findcart/';
    const url: string = this.productServiceUrl + shoppingUrl + userId;
    return this.httpClient.get<Cart>(url);
  }
  addToCart(cart): Observable<Cart> {
    const cartUrl = 'cart';
    const url: string = this.productServiceUrl + cartUrl;
    return this.httpClient.post<Cart>(url, cart);
  }
  getregForm(holder): Observable<RegModel> {
    const urlway = this.customerServiceUrl + 'createcustomer';
    return this.httpClient.post<RegModel>(urlway, holder);
  }
  getNewArrivals(): Observable<any> {
    const productUrl = 'getpublishedfifthrow';
    const url: string = this.contentServiceUrl + productUrl;
    return this.httpClient.get<any>(url);
  }

  addSubscriber(data): Observable<any> {
    const customerUrl = 'addsubscribe';
    const url: string = this.customerServiceUrl + customerUrl;
    return this. httpClient.post<any>(url,{emailId:data});
  }
  // getPlusSize(): Observable<any> {
  //   const productUrl = 'getplussize';
  //   const url: string = this.productServiceUrl + productUrl;
  //   return this.httpClient.get<any>(url);
  // }

  multipleProductDiscount(discount, product) {
    for (let l = 0; l <= product.length - 1; l++) {
      product[l].discountStyle = 'discountNone';        // set all discountStyle field 'discountNone'
      for (let i = 0; i <= discount.length - 1; i++) {
        for (let j = 0; j <= discount[i].conditions.length - 1; j++) {
          for (let k = 0; k <= discount[i].conditions[j].value.length - 1; k++) {

            if (discount[i].conditions[j].field === 'Product Name') {     // check discount field type
              if (product[l]._id === discount[i].conditions[j].value[k]) {

                if (product[l].discountStyle === 'discountStyle') {     // check offer already applied or not
                  if (discount[i].amountType === 'Flat') {              // check amount type
                    const temp = product[l].oldPrice - discount[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {      // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - discount[i].typeValue;
                      product[l].discount = 'Flat ' + discount[i].typeValue;
                      product[l].discountStyle = 'discountStyle';
                    } else {        // new price is smallerthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {                  // amount type percentage
                    const temp = product[l].price - Math.round((product[l].price / 100 * discount[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {          // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                      product[l].discount = discount[i].typeValue + '%';
                      product[l].discountStyle = 'discountStyle';
                    } else {                // new price is smallerthan previous price
                      continue;
                    }
                  }

                  /* check amount greaterthan or lessthan ----- end ---------*/

                  /* previous offer not applied product-------------- start ------------- */

                } else {
                  product[l].oldPrice = product[l].price;
                  if (discount[i].amountType === 'Flat') {
                    product[l].price = product[l].oldPrice - discount[i].typeValue;
                    product[l].discount = 'Flat ' + discount[i].typeValue;
                    product[l].discountStyle = 'discountStyle';
                  } else {
                    product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                    product[l].discount = discount[i].typeValue + '%';
                    product[l].discountStyle = 'discountStyle';
                  }
                }

                /* previous offer not applied product-------------- end ------------- */

              }
            } else if (discount[i].conditions[j].field === 'Product Category') {     // check discount field type
              if (product[l].superCategoryId === discount[i].conditions[j].value[k]) {
                if (product[l].discountStyle === 'discountStyle') {  // check offer already applied or not
                  if (discount[i].amountType === 'Flat') {     // check amount type
                    const temp = product[l].oldPrice - discount[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {      // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - discount[i].typeValue;
                      product[l].discount = 'Flat ' + discount[i].typeValue;
                      product[l].discountStyle = 'discountStyle';
                    } else {        // new price is lesserthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {       // amount type percentage
                    const temp = product[l].price - Math.round((product[l].price / 100 * discount[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {          // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                      product[l].discount = discount[i].typeValue + '%';
                      product[l].discountStyle = 'discountStyle';
                    } else {  // new price is smallerthan previous price
                      continue;
                    }
                  }
                  /* previous offer not applied product-------------- start ------------- */

                } else {
                  product[l].oldPrice = product[l].price;
                  if (discount[i].amountType === 'Flat') {
                    product[l].price = product[l].oldPrice - discount[i].typeValue;
                    product[l].discount = 'Flat ' + discount[i].typeValue;
                    product[l].discountStyle = 'discountStyle';
                  } else {
                    product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                    product[l].discount = discount[i].typeValue + '%';
                    product[l].discountStyle = 'discountStyle';
                  }
                }
                /* previous offer not applied product-------------- end ------------- */
              }
            }
          }
        }
      }
    }
    return product;
  }
  multiplePromotionDiscount(discount, product) {
    for (let l = 0; l <= product.length - 1; l++) {
      product[l].discountStyle = 'discountNone';        // set all discountStyle field 'discountNone'
      for (let i = 0; i <= discount.length - 1; i++) {
        for (let j = 0; j <= discount[i].conditions.length - 1; j++) {
          for (let k = 0; k <= discount[i].conditions[j].value.length - 1; k++) {

            if (discount[i].conditions[j].field === 'Product Name') {     // check discount field type
              if (product[l].productId === discount[i].conditions[j].value[k]) {

                if (product[l].discountStyle === 'discountStyle') {     // check offer already applied or not
                  if (discount[i].amountType === 'Flat') {              // check amount type
                    const temp = product[l].oldPrice - discount[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {      // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - discount[i].typeValue;
                      product[l].discount = 'Flat ' + discount[i].typeValue;
                      product[l].discountStyle = 'discountStyle';
                    } else {        // new price is smallerthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {                  // amount type percentage
                    const temp = product[l].price - Math.round((product[l].price / 100 * discount[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {          // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                      product[l].discount = discount[i].typeValue + '%';
                      product[l].discountStyle = 'discountStyle';
                    } else {                // new price is smallerthan previous price
                      continue;
                    }
                  }

                  /* check amount greaterthan or lessthan ----- end ---------*/

                  /* previous offer not applied product-------------- start ------------- */

                } else {
                  product[l].oldPrice = product[l].price;
                  if (discount[i].amountType === 'Flat') {
                    product[l].price = product[l].oldPrice - discount[i].typeValue;
                    product[l].discount = 'Flat ' + discount[i].typeValue;
                    product[l].discountStyle = 'discountStyle';
                  } else {
                    product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                    product[l].discount = discount[i].typeValue + '%';
                    product[l].discountStyle = 'discountStyle';
                  }
                }

                /* previous offer not applied product-------------- end ------------- */

              }
            } else if (discount[i].conditions[j].field === 'Product Category') {     // check discount field type
              if (product[l].superCategoryId === discount[i].conditions[j].value[k]) {
                if (product[l].discountStyle === 'discountStyle') {  // check offer already applied or not
                  if (discount[i].amountType === 'Flat') {     // check amount type
                    const temp = product[l].oldPrice - discount[i].typeValue;

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {      // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - discount[i].typeValue;
                      product[l].discount = 'Flat ' + discount[i].typeValue;
                      product[l].discountStyle = 'discountStyle';
                    } else {        // new price is lesserthan previous price
                      continue;
                    }

                    /* check amount greaterthan or lessthan ----- end ---------*/

                  } else {       // amount type percentage
                    const temp = product[l].price - Math.round((product[l].price / 100 * discount[i].typeValue));

                    /* check amount greaterthan or lessthan ----- start ---------*/

                    if (product[l].price > temp) {          // new price is lesserthan previous price
                      product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                      product[l].discount = discount[i].typeValue + '%';
                      product[l].discountStyle = 'discountStyle';
                    } else {  // new price is smallerthan previous price
                      continue;
                    }
                  }
                  /* previous offer not applied product-------------- start ------------- */

                } else {
                  product[l].oldPrice = product[l].price;
                  if (discount[i].amountType === 'Flat') {
                    product[l].price = product[l].oldPrice - discount[i].typeValue;
                    product[l].discount = 'Flat ' + discount[i].typeValue;
                    product[l].discountStyle = 'discountStyle';
                  } else {
                    product[l].price = product[l].oldPrice - Math.round((product[l].oldPrice / 100 * discount[i].typeValue));
                    product[l].discount = discount[i].typeValue + '%';
                    product[l].discountStyle = 'discountStyle';
                  }
                }
                /* previous offer not applied product-------------- end ------------- */
              }
            }
          }
        }
      }
    }
    return product;
  }
}
