import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavComponent } from './nav/nav.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedRoutingModule } from './shared-routing.module';
import { FooterComponent } from './footer/footer.component';
import { NavTopComponent } from './nav-top/nav-top.component';
import { CategoryMenuComponent } from './category-menu/category-menu.component';
import { SharedService } from './shared.service';
import { TitleCasePipe } from '../shared/nav/title-case.pipe';


import {
  MatSidenavModule,
  MatBadgeModule,
  MatListModule,
  MatTooltipModule,
  MatOptionModule,
  MatSelectModule,
  MatMenuModule,
  MatSnackBarModule,
  MatGridListModule,
  MatToolbarModule,
  MatIconModule,
  MatButtonModule,
  MatRadioModule,
  MatCheckboxModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatExpansionModule,
  MatRippleModule,
  MatDialogModule,
  MatChipsModule,
  MatInputModule,
  MatFormFieldModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatTableModule,
  MatSortModule,
  MatTabsModule,
  MatSliderModule
} from '@angular/material';
import { SlideNavDirective } from './nav/slide-nav.directive';
import { MyCurrencyPipe } from './nav/currency.pipe';
import { ConverterPipe } from './nav/converter.pipe';
import { SwipeDirective } from './nav/swipe.directive';
import { SigninDailogComponent } from './signin-dailog/signin-dailog.component';
import { SwipeLeftDirective } from './nav/swipe-left.directive';
import { LoadResolverService } from './load-resolver.service';
@NgModule({
  declarations: [NavComponent, FooterComponent, CategoryMenuComponent,
     NavTopComponent, SlideNavDirective, TitleCasePipe,
    MyCurrencyPipe, ConverterPipe, SwipeDirective, SigninDailogComponent, SwipeLeftDirective],
  imports: [
    CommonModule,
    SharedRoutingModule,
    FormsModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatListModule,
    MatTooltipModule,
    MatOptionModule,
    MatSelectModule,
    MatMenuModule,
    MatSnackBarModule,
    MatGridListModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatRadioModule,
    MatCheckboxModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatRippleModule,
    MatDialogModule,
    MatChipsModule,
    MatInputModule,
    MatFormFieldModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatTabsModule,
    MatSliderModule,
    ReactiveFormsModule
  ],
  entryComponents: [SigninDailogComponent],
  providers: [SharedService, ConverterPipe, LoadResolverService ],
  exports: [NavComponent, FooterComponent, CategoryMenuComponent, NavTopComponent,
    CommonModule, FormsModule, MatSelectModule, MyCurrencyPipe, TitleCasePipe, ConverterPipe, SwipeDirective]
})
export class SharedModule { }
