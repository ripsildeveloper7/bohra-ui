import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegModel } from '../../account-info/registration/registration.model';
import { SignIn } from '../../account-info/signin/signIn.model';
import { Cart } from '../model/cart.model';
import { Product } from '../model/product.model';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';

declare const gapi: any;
declare const FB: any;
@Component({
  selector: 'app-signin-dailog',
  templateUrl: './signin-dailog.component.html',
  styleUrls: ['./signin-dailog.component.css']
})
export class SigninDailogComponent implements OnInit {
  signInForm: FormGroup;
  regForm: FormGroup;
  isSignUp = false;
  tabItems = [{ item: 'Login' }, { item: 'Registration' }];
  selectedItemTab = this.tabItems[0].item;
  shopModel: any;
  signInModel: SignIn;
  pwdError: boolean;
  submitted = false;
  showPassword = false;
  cart: Cart;
  productModel: Product;
  userId: string;
  cartLocal: any;
  showMobileView = false;
  googleStatus = 'googleCustomer';
  facebookStatus = 'facebookCustomer';
  facebookData;
  isFacebook = false;
  holder;
  text = 'registredCustomer';

  constructor(public dialogRef: MatDialogRef<SigninDailogComponent>, private location: Location,
              private sharedService: SharedService, private router: Router, private fb: FormBuilder) {
        // facebook

        (window as any).fbAsyncInit = function () {
          FB.init({
            appId: '614348709091296',
            cookie: true,
            xfbml: true,
            version: 'v4.0'
          });
          FB.AppEvents.logPageView();
        };
        (function (d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) { return; }
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
  }

  ngOnInit() {
    this.createForm();
    this.createRegForm();
  }
  createForm() {
    this.userId = sessionStorage.getItem('userId');
    this.signInForm = this.fb.group({
      emailId: ['', Validators.required],
      password: ['', Validators.required],
      mobileNumber: ['']
    });
  }
  createRegForm() {
    this.regForm = this.fb.group({
      emailId: ['', Validators.email],
      password: ['', [Validators.required, Validators.minLength(6)]],
      mobileNumber: ['']

    });
  }
  ngAfterViewInit() {
    /* this.getWindowSize(); */
    this.googleInit();
    this.getWindowSize();
  }

  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  onSubmit() {
    this.submitted = true;
    this.signInModel = new SignIn();
    this.signInModel.emailId = this.signInForm.controls.emailId.value;
    this.signInModel.password = this.signInForm.controls.password.value;
    this.sharedService.signIn(this.signInModel).subscribe(data => {
      console.log(data);
      if (data.length === 0) {
        this.pwdError = true;
        sessionStorage.setItem('login', 'false');
        sessionStorage.removeItem('userId');
      } else {
        /* this.setCookie(data[0]._id); */
        console.log(data);
        sessionStorage.setItem('login', 'true');
        sessionStorage.setItem('userId', data[0].userId);
        sessionStorage.setItem('emailId', data[0].emailId);
        /* this.router.navigate(['home/welcome']); */
        this.logInUserData();
      }
    });
  }
  logInUserData() {
    this.userId = sessionStorage.getItem('userId');
    this.cartLocal = JSON.parse(sessionStorage.getItem('cart'));
    if (!this.cartLocal) {
      /* this.router.navigate(['home/welcome']); */
      this.shoppingCartUser(this.userId);
      /* this.location.back(); */
      this.close();
    } else {
      const localItem = this.cartLocal.map(item => item.items);
      this.addToCartServer(this.userId, localItem);
    }
  }
  shoppingCartUser(userId) {
    this.sharedService.shoppingUser(userId).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      this.close();
    }, err => {
      console.log(err);
    });
  }
  addToCartServer(userId, items) {
    this.cart = new Cart();
    this.cart.userId = userId;
    this.cart.items = items;
    this.sharedService.addToCart(this.cart).subscribe(data => {
      this.shopModel = data;
      sessionStorage.setItem('cartqty', JSON.stringify(this.shopModel.length));
      sessionStorage.removeItem('cart');
      /* this.router.navigate(['home/welcome']); */
      /* this.location.back(); */
      this.close();
    }, error => {
      console.log(error);
    });
  }
  close() {
    this.dialogRef.close(true);
  }
  signup() {
    this.isSignUp = true;
  }
  login() {
    this.isSignUp = false;
  }
/// Google Login - start

public auth2: any;
public googleInit() {
  gapi.load('auth2', () => {
    this.auth2 = gapi.auth2.init({
      client_id: '3512610627-t9nl0vkb47dtau7fj4d8uoo14bhquuq5.apps.googleusercontent.com',
      cookiepolicy: 'single_host_origin',
      scope: 'profile email'
    });
    this.attachSignin(document.getElementById('googleBtn'));
  });

}
public attachSignin(element) {
  this.auth2.attachClickHandler(element, {},
    (googleUser) => {
      const profile = googleUser.getBasicProfile();
      this.addGoogleLogin(profile);
      console.log(profile);
    }, (error) => {
      alert(JSON.stringify(error, undefined, 2));
    });
}
addGoogleLogin(profile) {
  const holder = new RegModel();
  holder.firstName = profile.getGivenName();
  holder.lastName = profile.getFamilyName();
  holder.emailId = profile.getEmail();
  holder.createdBy = this.googleStatus;
  this.sharedService.socialMediaLogin(holder).subscribe(data => {
    sessionStorage.setItem('login', 'true');
    sessionStorage.setItem('lastLoginBy', data.lastLoginBy);
    sessionStorage.setItem('userId', data._id);
    this.close();
    /* this.router.navigate(['home/welcome']); */
  });
}
/// Google login - End

/// Facebook Login - Start

facebookLoginSubmit() {
  const promise = new Promise((resolve, reject) => {
    FB.getLoginStatus(function (response) {
      if (response.status === 'connected') {
        /* this.isFacebook = false; */
        console.log('already loggedin');
        FB.logout(function (response) {
          console.log('logout successful');
        });
        FB.login(function (response) {
          /* this.isFacebook = true; */
          if (response.authResponse) {
            FB.api('/me?fields=name,id,email,first_name,last_name', function (response) {
              resolve(response);
              /* console.log('inside1', this.facebookData); */
            });
          }
        }, { scope: 'email', return_scope: true });
      } else {
        FB.login(function (response) {
          if (response.authResponse) {
            FB.api('/me?fields=name,id,email,first_name,last_name', function (response) {
              resolve(response);
              /* console.log('inside2', this.facebookData); */
            });
          }
        }, { scope: 'email', return_scope: true });
      }
    });
  });
  promise.then((res) => {
    this.facebookLogin(res);
  });
  promise.catch((err) => {
    console.log(err);
  });
}

facebookLogin(facebook) {
  console.log('login', facebook);
  const temp = new RegModel();
  temp.emailId = facebook.email;
  temp.firstName = facebook.first_name;
  temp.lastName = facebook.last_name;
  temp.createdBy = this.facebookStatus;
  this.sharedService.socialMediaLogin(temp).subscribe(data => {
    console.log('checkPoint 3', data);
    sessionStorage.setItem('login', 'true');
    sessionStorage.setItem('lastLoginBy', data.lastLoginBy);
    sessionStorage.setItem('userId', data._id);
    this.close();
    /* this.router.navigate(['home/welcome']); */
  }, error => {
    console.log(error);
  });
}

/// Facebook Login - End

// Register User

onRegister() {
  this.holder = new RegModel();
  this.holder.emailId = this.regForm.controls.emailId.value;
  this.holder.mobileNumber = this.regForm.controls.mobileNumber.value;
  this.holder.password = this.regForm.controls.password.value;
  this.holder.createdBy = this.text;
  this.sharedService.getregForm(this.holder).subscribe(data => {
    if (data.result) {
      this.holder = data;
    } else {
      sessionStorage.setItem('login', 'true');
      sessionStorage.setItem('userId', data.userId);
      sessionStorage.setItem('lastLoginBy', data.lastLoginBy);
      this.router.navigate(['account/profile']);
      this.close();
    }
  }, error => {
    console.log(error);
  });
  /* console.log(this.regForm); */
}
checkPassword() {
  this.showPassword = !this.showPassword;
}
}
