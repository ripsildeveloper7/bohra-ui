import { Component, OnInit } from '@angular/core';
import {SharedService} from '../shared.service';
import {Footer} from './footer.model';
import {Header} from '../nav/header.model';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  footerDetails: Footer;
  header: Header[];
  imageData: any;
  showMobileView = false;
  superCategory: any;
  constructor(private sharedService: SharedService) { 
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
   }

  ngOnInit() {
    this.allFooter();
    this.getLogo();
    this.getSuperCategory();
}


getLogo() {
  this.sharedService.getHeaderImage().subscribe(data => {
    /* console.log(data); */
    this.imageData = data[0];
  }, error => {
    console.log(error);
  });
}
allFooter() {
  this.sharedService.getFooterDetails().subscribe(data => {
    this.footerDetails = data[0];
    console.log(data);
  }, error => {
    console.log(error);
  });
}
ngAfterViewInit(){
  this.getWindowSize();
}
getWindowSize() {
  if (window.screen.width > 900) {
    this.showMobileView = false;
  } else {
    this.showMobileView = true;
  }
}
getSuperCategory() {
  this.sharedService.getSuperCategory().subscribe(data => {
    this.superCategory = data;
    this.allFooter();
  });
}

submit(val){
this.sharedService.addSubscriber(val).subscribe(data =>{
console.log(data);
})
}
}
