import { Details } from './details.model'
export class HowToMeasure {
    superCategoryId: string;
      mainCategoryId: string;
      subCategoryId: string;
      superCategoryName: string;
      mainCategoryName: string;
      subCategoryName: string;
      image: string;
      name: string;
      detail: [Details];
     
  }