
export class Size {
    _id: string;
    sizeName: string;
    sku: string;
    productId: string;
    sizeQuantity: number;
    oldSizePrice: number;
    sizePrice: number;
    pricePrefix: boolean;
    subtractStack: number;
    discountStyle: string;
}
