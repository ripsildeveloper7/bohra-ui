export class KameezMeasurement {
    userId: string;
    typeName: string;
    typeDescription: string;
    price: number;
    discount: number;
    // Kameez Measurement
    aroundBust: number;
    aroundAboveWaist: number;
    aroundHip: number;
    kameezLength: number;
    frontNeckStyle: string;
    frontNeckDepth: number;
    backNeckStyle: string;
    backNeckDepth: number;
    sleeveStyle: string;
    sleeveLenth: number;
    aroundArm: number;
    kameezClosingSide: string;
    kameezClosingWith: string;
    // Bottom Measurement
    bottomStyle: string;
    fitOption: string;
    aroundWaist: number;
    aroundThigh: number;
    aroundKnee: number;
    aroundCalf: number;
    aroundBottom: number;
    bottomLength: number;
    waistClosingSide: string;
    waistClosingWith: string;

    specialInstruction: string;
    measurementName: string;
    addedDate: string;
    serviceId: string;
    measurementId: string;
}
