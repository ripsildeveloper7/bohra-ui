import { MainCategory } from './mainCategory.model';
import {FieldAttributeValue} from './field-attribute-value.model'
export class SuperCategory {
    _id: string;
    categoryName: string;
    categoryDescription: string;
    sortOrder: number;
    status: string;
    checkCategoryName: boolean;
    mainCategory: [MainCategory];
    count: number;
    superCatData: boolean;
    mainCatData: boolean;
    subCatData: boolean;
    brandCatData: boolean;
    collectionCatData: boolean;
    brandId: string;
    collectionId: string;
    promotionCatData: boolean;
    attribute: [{fieldName: String, fieldType: String, fieldSetting, fieldEnable: boolean,  fieldValue: FieldAttributeValue}];
}
