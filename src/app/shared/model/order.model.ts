
import {Product} from './product.model';
import {Cart} from './cart.model';
import {AddressModel} from './address.model';
import { Coupon } from './coupon.model';
import { CustomerMeasurement } from './customerMeasurement.mode';
export class Order {
    _id: string;
    customerId: string;
    mobileNumber: string;
    orderId: string;
    items: [{productId: number, pack: number,
       ratioQty: number}];
    total: number;
    /* addressDetails: [{
       name: string;
       mobileNumber: number;
       streetAddress: string;
       building: string;
       landmark: string;
       city: string;
       state: string;
       pincode: string;
    }]; */
    addressDetails: AddressModel;
    paymentStatus: string;
    orderStatus: string;
    orderDate: Date;
    orderUpdatedDate: Date;
    razorPayOrderId: string;
    orderedProducts: [ Product,
    ];
    cart: [Cart];
    paypalOrderId: string;
    paymentMode: string;
    shipmentStatus: string;
    productId: string;
    coupon: [Coupon];
    serviceId: string;
    shipingFees: number;
    measurement: [CustomerMeasurement];
 }
