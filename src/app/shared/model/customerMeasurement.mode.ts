export class CustomerMeasurement {
    userId: string;
    typeName: string;
    aroundBust: number;
    aroundAboveWaist: number;
    blouseLength: number;
    frontNeckStyle: string;
    frontNeckDepth: number;
    backNeckStyle: string;
    backNeckDepth: number;
    sleeveStyle: string;
    sleeveLenth: number;
    aroundArm: number;
    closingSide: string;
    closingWith: string;
    lining: string;
    specialInstruction: string;
    measurementName: string;
    addedDate: string;
    serviceId: string;
    typeDescription: string;
    price: number;
    discount: number;
    productId: string;
}
