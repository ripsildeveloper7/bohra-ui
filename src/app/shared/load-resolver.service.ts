import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material'
import { Observable, of, Subject } from 'rxjs';
import { SigninDailogComponent } from './../shared/signin-dailog/signin-dailog.component';
@Injectable({
  providedIn: 'root'
})
export class LoadResolverService implements Resolve<any> {
  dialogRef: MatDialogRef<SigninDailogComponent>;
  constructor(private dialog: MatDialog) { }

  resolve(){
    console.log("hello");
  }
  
  openLogin(): Observable<boolean> {
    this.dialogRef = this.dialog.open(SigninDailogComponent, {
    disableClose: true, backdropClass: 'light-backdrop',
    width: '720px'
  });
return this.dialogRef.afterClosed();
}

  
}
