import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { SharedService } from './../shared.service';
import { Header } from './header.model';
import { Country } from './country.model';
import { Product } from './../model/product.model';
import { SuperCategory } from './../model/superCategory.model';
import { Router } from '@angular/router';
import { trigger, transition, state, style, animate } from '@angular/animations';
import { ConverterPipe } from './converter.pipe';
import { Rates } from './rates.model';
import { ThrowStmt } from '@angular/compiler';
import { SigninDailogComponent } from '../signin-dailog/signin-dailog.component';
import {Location } from '@angular/common';
import {
  MatDialog,
  MatDialogConfig
} from "@angular/material";

declare const FB: any;
declare const gapi: any;
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  animations: [
    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ])
  ],
  providers: [ConverterPipe]
})
export class NavComponent implements OnInit, AfterViewInit {
  navbarShow = false;
  dropShow: boolean;
  header: Header[];
  dropdownShow = false;
  mainCategory;
  mainCatCount;
  selectedDropDown: string;
  selected: any;
  colorText = '#f93800';
  logoImage: string;
  imageData: any;
  superCategory: SuperCategory[];
  product: Product[];
  footerDetails: any;
  onLogin = false;
  showNav = false;
  showMobileView = false;
  eRef: any;
  navbarhide = false;
  showSearch = false;
  email: any;
  rates = new Rates().rates;
  buttons: Array<string>;
  unit: string;
  currencyVal: any;
  title = 'currency-converter';
  currentVal: number;
  value = 500;
  total = 10000;
  topPosToStartShowing = 100;
  rateArray;
  priceModel: void;
  plusModel: any;
  // @HostListener('window:scroll')
  // checkScroll() {


  //   const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
  //   if(scrollPosition == 0){
  //     document.getElementById('m').style.top = null;
  //   } else {
  //   document.getElementById('m').style.top = '0';
    
  //   }

  //   if (scrollPosition >= this.topPosToStartShowing) {
    
  // }
  // }
  constructor(private dialog: MatDialog, public sharedService: SharedService,
              private converterPipe: ConverterPipe, private router: Router) {
                // this.getPlusSize();
    this.currentVal = 1;
    this.buttons = Object.keys(new Rates().rates);
    this.currencyVal = {};
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
   
    // facebook

    (window as any).fbAsyncInit = function () {
      FB.init({
        appId: '614348709091296',
        cookie: true,
        xfbml: true,
        version: 'v4.0'
      });

      FB.AppEvents.logPageView();

    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  ngOnInit() {
    this.getHeaderImage();
    /* this.allHeader(); */
    this.getSuperCategory();
    this.getWindowSize();
    this.sharedService.invokeEvent.subscribe(
      () => {
        this.toggleNavbar();
      });
     
  }
  
  ngAfterViewInit() {
    this.getWindowSize();
  }
  convert(d, unit) {
    // Get rates Object from rates.ts
    // Get all the keys i.e the currency names
    this.rateArray = Object.keys(this.rates);
    // Search the index of the selected currency name in array
    const test = this.rateArray.indexOf(unit);
    // Get the value of the selected currency i.e rate of selected currency from the object
    this.selected = this.rates[Object.keys(this.rates)[test]];
    this.converterPipe.transform(this.selected, unit);
    /* this.currentRegion = unit; */
  }
  getKeys() {
    return Object.keys(this.currencyVal);
  }
  getWindowSize() {
    if (window.screen.width > 900) {
      this.showMobileView = false;
    } else {
      this.showMobileView = true;
    }
  }
  /* allHeader() {
    this.sharedService.getHeaderDetails().subscribe(data => {
      this.header = data;
      this.header.map(elememt =>        {
          this.logoImage = elememt.logoImageName;
        }
      );
    }, error => {
      console.log(error);
    });
  } */


  toggleNavbar() {
    this.navbarShow = !this.navbarShow;
    // console.log("hi");
    return this.navbarShow;
  }
  closeDropdown() {
    this.selectedDropDown = '';
    this.selected = '';
  }

  onEnter(key, box) {
    const num: any = 1;
    if (sessionStorage.getItem("searchCount")) {
      let count: any = parseInt(sessionStorage.getItem("searchCount"));
      count += num;
      sessionStorage.setItem("searchCount", count);
    } else {
      sessionStorage.setItem("searchCount", num);
    }
    const id = sessionStorage.getItem("searchCount");
    this.showSearch = false;
    this.router.navigate(["product/searchProduct/",key]
    //  {
    //    queryParams: { search: key }
    // }
    );
  }


  getHeaderImage() {
    this.sharedService.getHeaderImage().subscribe(data => {
      console.log(data);
      this.imageData = data[0];
    }, error => {
      console.log(error);
    });
  }
  toggleDropdownLeave() {
    this.selectedDropDown = '';
    this.selected = '';
  }
  toggleDropdownClick() {
    this.dropdownShow = !this.dropdownShow;
    this.selectedDropDown = '';
    this.selected = '';
  }
  toggleLeave() {
    this.dropdownShow = !this.dropdownShow;
  }
  shoppingView() {
    this.router.navigate(['/cart/shopping']);
  }
  getSuperCategory() {
    this.sharedService.getSuperCategory().subscribe(data => {
      this.superCategory = data;
      console.log(data);
      this.superCategory.map( d => {
        this.mainCatCount = d;
        console.log(d);
      })
      this.allFooter();
    });
  }


  toggleDropdown(cat) {
      this.superCategory.forEach(element => {
        if (element._id === cat._id) {
          this.dropdownShow = !this.dropdownShow;
          this.selectedDropDown = element._id;
        }
      });
  }

  getMainCategory(id){
    if(this.mainCatCount.length == 0){
      this.router.navigate(['/product/supercategory/',id]);
    } else {
      this.router.navigate(['/product/supercategory/'])
    }
  }

  logOut() {
    if (sessionStorage.getItem('lastLoginBy')) {
      const temp = sessionStorage.getItem('lastLoginBy');
      if (temp === 'googleCustomer') {
        this.googleSignOut();
      } else if (temp === 'facebookCustomer') {
        this.facebookLogout();
      } else {
        this.sharedService.sessionLogout();
        this.router.navigate(['home/welcome']);
        this.email = '';
      }
    } else {
      this.sharedService.sessionLogout();
      this.router.navigate(['home/welcome']);
      this.email = '';
    }

  }
   googleSignOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
    this.sharedService.sessionLogout();
    this.router.navigate(['home/welcome']);
    this.email = '';
  }
  facebookLogout() {
    const promise = new Promise((resolve, reject) => {
      FB.getLoginStatus(function(response) {
        if (response && response.status === 'connected') {
          FB.logout(function(response) {
            resolve(response);
          });
        } else {
          FB.logout(function(response) {
            resolve(response);
          });
        }
      });
    });
    promise.then((res) => {
      console.log(res);
      this.sharedService.sessionLogout();
      this.router.navigate(['home/welcome']);
      this.email = '';
    });
    promise.catch((err) => {
      console.log(err);
    });
  }
  getLogin() {
    this.dropShow = false;
  }
  allFooter() {
    this.sharedService.getFooterDetails().subscribe(data => {
      this.footerDetails = data[0];
      console.log('footer details', this.footerDetails);
      /* console.log(data); */
    }, error => {
      console.log(error);
    });
  }
  getClick() {
    this.router.navigate(['home/welcome']);
  }
  openNav() {
    this.showNav = !this.showNav;
  }
  closeNav() {
    this.showNav = !this.showNav;
  }
  /* dropDownShow() {
    this.dropShow = !this.dropShow;
  } */
  onClick() {
    this.showSearch = !this.showSearch;
  }

  touch() {
    // window.addEventListener('touchend',(e) => {
    // this.toggleNavbar();
    alert(this.navbarShow);

  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(SigninDailogComponent, {
      panelClass: 'c1',
    });
  }
  getNewArrival() {
    this.sharedService.getNewArrivals().subscribe(data => {
      const tempData = data[0].productDetails.map(e => e.productId);
      const length = tempData.length;
      this.router.navigate(['/product/newarrival/', length], {queryParams: {'array': tempData, 'name': data[0].title, 'desc': data[0].description}});
  });
  }

  defaultTouch = { x: 0, y: 0, time: 0 };

  @HostListener('document.touchstart', ['$event'])
  //@HostListener('touchmove', ['$event'])
  @HostListener('touchend', ['$event'])
  @HostListener('touchcancel', ['$event'])
  handleTouch(event) {
      let touch = event.touches[0] || event.changedTouches[0];
  
      // check the events
      if (event.type === 'touchstart') {
          this.defaultTouch.x = touch.pageX;
          this.defaultTouch.y = touch.pageY;
          this.defaultTouch.time = event.timeStamp;
      } else if (event.type === 'touchend') {
          let deltaX = touch.pageX - this.defaultTouch.x;
          let deltaY = touch.pageY - this.defaultTouch.y;
          let deltaTime = event.timeStamp - this.defaultTouch.time;
  
          // simulte a swipe -> less than 500 ms and more than 60 px
          if (deltaTime < 500) {
              // touch movement lasted less than 500 ms
              if (Math.abs(deltaX) > 60) {
                  // delta x is at least 60 pixels
                  if (deltaX > 0) {
                      this.doSwipeRight(event);
                  } else {
                      this.doSwipeLeft(event);
                  }
              }
  
              if (Math.abs(deltaY) > 60) {
                  // delta y is at least 60 pixels
                  if (deltaY > 0) {
                      this.doSwipeDown(event);
                  } else {
                      this.doSwipeUp(event);
                  }
              }
          }
      }
  }
  
  doSwipeLeft(event) {
      console.log('swipe left', event);
      // this.onClickRight();
  }
  
  doSwipeRight(event) {
      console.log('swipe right', event);
      // this.nav.toggleNavbar();
     this.toggleNavbar();
      // this.onClickLeft();
  }
  
  doSwipeUp(event) {
      console.log('swipe up', event);
  }
  
  doSwipeDown(event) {
      console.log('swipe down', event);
  }
  
  getCategory(cat) {
    console.log(cat);
    if (cat.mainCategory.length === 0) {
      this.router.navigate(['product/supercategory/', cat._id]);
    } else {
      this.router.navigate(['product/categorylandingpage/', cat._id]);
    }
  }
  // getPlusSize() {
  //   this.sharedService.getPlusSize().subscribe(data => {
  //     console.log('plus', data);
  //     this.plusModel = data;
  //   }, error => {
  //     console.log(error);
  //   });
  // }
  selectPlusSize(plus) {
    console.log(plus);
    const holder = {
      Size: plus.startingSize
    };
    if (plus.subCategoryName === undefined) {
      this.router.navigateByUrl('product/supercategory/' + plus.superCategoryId, {skipLocationChange: false}).then(() => {
        this.router.navigate(['product/supercategory/', plus.superCategoryId], {queryParams: {Size: plus.startingSize}});
    }); 
     /*  this.router.navigate(['product/supercategory/', plus.superCategoryId], {queryParams: {Size: plus.startingSize}}); */
    } else {
      /* this.router.navigate(['product/supercategory/' + plus.superCategoryId + '/maincategory/' + plus.mainCategoryId + '/subcategory/' + plus.subCategoryId], {queryParams: {Size: plus.startingSize}}) */
      this.router.navigateByUrl('product/supercategory/' + plus.superCategoryId + '/maincategory/' + plus.mainCategoryId + '/subcategory/' + plus.subCategoryId, {skipLocationChange: false}).then(() => {
        this.router.navigate(['product/supercategory/' + plus.superCategoryId + '/maincategory/' + plus.mainCategoryId + '/subcategory/' + plus.subCategoryId], {queryParams: {Size: plus.startingSize}});
    }); 
     
    }
  }
}
