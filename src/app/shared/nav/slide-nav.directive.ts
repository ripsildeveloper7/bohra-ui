import { Directive, ElementRef, Input, AfterViewInit, Renderer2  } from '@angular/core';
import { HostListener } from '@angular/core';
import { HostBinding } from '@angular/core';

@Directive({
  selector: '[appSlideNav]'
})
export class SlideNavDirective {

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  getDevice() {
    if (window.screen.width > 990) {

    } else {

    }
  }
  @HostListener('click') onclick() {
    /* const part = this.elementRef.nativeElement.querySelector('.sidenav');
    this.renderer.addClass() */
  }
  @HostListener('mouseover') onMouseOverLeft() {
    const part = this.elementRef.nativeElement.querySelector('.boxShape');
    this.renderer.addClass(part, 'show');
  }
  @HostListener('mouseout') onMouseOutLeft() {
    const part = this.elementRef.nativeElement.querySelector('.boxShape');
    this.renderer.removeClass(part, 'show');
  }
}
