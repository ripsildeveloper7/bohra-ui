import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleCase'
})
export class TitleCasePipe implements PipeTransform {

  transform(input: string): string {
      var s = input;
      s = s.replace(/([A-Z])/g, ' $1').trim();
    return s;
    } 
    
}
