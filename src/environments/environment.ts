// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


// export const environment = {
//   production: true,
//   contentServiceUrl: 'https://065h6q6de3.execute-api.ap-south-1.amazonaws.com/qa/',
//   instagramUrl : 'https://api.instagram.com/v1/users/self/media/recent/?access_token=8636464595.f5aef9d.b112bf6c1e6a41a482ec7e7a77882469',
// productServiceUrl: 'http://localhost:3072/',
//     // productServiceUrl: 'https://7hs6v5qhs4.execute-api.ap-south-1.amazonaws.com/qa/', 
//   productImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/product/',
//   /* sizeGuideImageUrl: 'https://product-microservice-image.s3.ap-south-1.amazonaws.com/images/size/', */
//   sizeGuideImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/size/',
//   categoryImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/category/',
//   cmsServiceUrl: 'https://3rhujs56d2.execute-api.ap-south-1.amazonaws.com/qa/',
//  commerceOrderServiceUrl: 'http://localhost:3112/',
//  /*  commerceOrderServiceUrl: 'https://ddxia2qpii.execute-api.ap-south-1.amazonaws.com/qa/',  */
//   customerServiceUrl: 'https://7vkipfu9da.execute-api.ap-south-1.amazonaws.com/qa/', 
//   // customerServiceUrl: 'http://localhost:3111/',
//   subCategoryImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/',
//   brandImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/brand/',
//   marketingServiceUrl: 'https://9p6levewy7.execute-api.ap-south-1.amazonaws.com/qa/',
// };

// export const environment = {
//   production: false,
//   contentServiceUrl: 'https://065h6q6de3.execute-api.ap-south-1.amazonaws.com/qa/',
//   measurementImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/measurement/',
//   productServiceUrl: 'http://localhost:3072/',
//   // productServiceUrl: 'https://7hs6v5qhs4.execute-api.ap-south-1.amazonaws.com/qa/', 
//   productImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/product/',
//   instagramUrl : 'https://api.instagram.com/v1/users/self/media/recent/?access_token=8636464595.f5aef9d.b112bf6c1e6a41a482ec7e7a77882469',
//   categoryImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/category/',
//   sizeGuideImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/size/',
//   cmsServiceUrl: 'https://3rhujs56d2.execute-api.ap-south-1.amazonaws.com/qa/',
//   /*  commerceOrderServiceUrl: 'http://localhost:3112/', */
//   /*customerServiceUrl: 'http://localhost:3111/', */
//  commerceOrderServiceUrl: 'https://ddxia2qpii.execute-api.ap-south-1.amazonaws.com/qa/',
//   customerServiceUrl: 'https://7vkipfu9da.execute-api.ap-south-1.amazonaws.com/qa/',
//   subCategoryImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/subcategory/',
//   brandImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/brand/',
//   marketingServiceUrl: 'https://9p6levewy7.execute-api.ap-south-1.amazonaws.com/qa/',
//   categoryBannerImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/categorybanner/',
//   howToMeasureImageUrl: 'https://ucchalfashion-product-images.s3.ap-south-1.amazonaws.com/images/howtomeasure/',
// };


// testing api

export const environment = {
  production: false,
  contentServiceUrl: 'https://ohof6aswlc.execute-api.ap-south-1.amazonaws.com/qa/',
  // contentServiceUrl:'http://localhost:3110/',
  measurementImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/measurement/',
  // productServiceUrl: 'http://localhost:3072/',
  productServiceUrl: 'https://3gr03xpvlc.execute-api.ap-south-1.amazonaws.com/qa/', 
  productImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/product/',
  instagramUrl : 'https://api.instagram.com/v1/users/self/media/recent/?access_token=8636464595.f5aef9d.b112bf6c1e6a41a482ec7e7a77882469',
  categoryImageUrl:  'http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/category/',
  sizeGuideImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/size/',
  cmsServiceUrl: 'https://ohof6aswlc.execute-api.ap-south-1.amazonaws.com/qa/',
  //  commerceOrderServiceUrl: 'http://localhost:3112/',
  customerServiceUrl: 'http://localhost:3111/',
 commerceOrderServiceUrl: 'https://qwv6tk7tc6.execute-api.ap-south-1.amazonaws.com/qa/',
  // customerServiceUrl: 'https://mhxr06om92.execute-api.ap-south-1.amazonaws.com/qa/',
  subCategoryImageUrl: 'http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/subcategory/',
  brandImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/brand/',
  marketingServiceUrl: 'https://pgrvf8whti.execute-api.ap-south-1.amazonaws.com/qa/',
  categoryBannerImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/categorybanner/',
  howToMeasureImageUrl: ' http://bohra-product-images.s3-website.ap-south-1.amazonaws.com/images/howtomeasure/',
};
